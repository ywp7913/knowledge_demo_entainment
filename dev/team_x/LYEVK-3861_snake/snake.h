/*
 * Copyright (c) 2021 KaiHong Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#ifndef __OLED_SNAKE_H
#define __OLED_SNAKE_H		
 
#define Key_UP 		P00
#define Key_DOWN	P01
#define Key_Right	P02
#define Key_Left	P03 
#define Key_UP_number 		0
#define Key_DOWN_number 	1
#define Key_Right_number 	2
#define Key_Left_number 	3
//按键宏定义
 
 /**
 * @brief   蛇最大的右边界像素点
 */
 #define GAME_MAX_WIDTH   127
 /**
 * @brief   蛇最大的底部边界像素点
 */
 #define GAME_MAX_BOTTON   63

/**
 * @brief  蛇头移动的宏定义
 *
 * @param x   屏幕左右移动参数，-1左移，1 右移, 0 不变
 * @param y   屏幕上下移动参数，-1上移，1 下移，0 不变
 */
 #define SNAKE_MOVE(x, y)\
    Snake.X[Snake.Long-1]=Snake.X[Snake.Long-2]+ x;\
    Snake.Y[Snake.Long-1]=Snake.Y[Snake.Long-2]+ y
//void OLED_snake();

typedef enum {
    LEVEL_EASY = 1,
    LEVEL_NOMARL,
    LEVEL_HARD    
} gameLevel;

typedef enum {
    DIREC_STRAIGHT = -1,
    DIREC_RIGHT,  //右
    DIREC_TOP,    //上
    DIREC_LEFT,   //左
    DIREC_BOTTOM, //底
    DIREC_MAX
} snakeDirection;

/*1-2 直行  3-4 左转 5-6 右转*/
static int8 snakeDirectonInfo[4][6] = {
    {1, 0, 0, -1, 0, 1},  //DIREC_RIGHT
    {0, -1, -1, 0, 1, 0}, //DIREC_TOP
    {-1, 0, 0, 1, 0, -1}, //DIREC_LEFT
    {0, 1, 1, 0, -1, 0}   //DIREC_BOTTOM
};

typedef struct {
    uint8 X;
    uint8 Y;
    uint8 Yes;   // 1- 需要放置食物 0- 食物已产生
} FoodType; //食物结构体

#define SNAKE_MAX_LONG 20 //最大长度

typedef struct {
    int8 X[SNAKE_MAX_LONG];
    int8 Y[SNAKE_MAX_LONG];
    uint8 Long;           //蛇的长度
    gameLevel Level;          // 1-简单 2- 正常 3- 困难
    snakeDirection Direction; //蛇的前进方向 默认向右
} snakeType;                      //蛇结构体


#endif