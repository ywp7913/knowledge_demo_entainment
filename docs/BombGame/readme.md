# [OpenHarmony 分布式传炸弹小游戏](../../FA/BombGame)

## 一、简介

#### 1.样例效果

[本Demo](../../FA/BombGame)是基于OpenHarmony3.1 Beta，使用ETS语言编写的应用。该样例使用ETS编写，在邀请用户进行设备认证后，用户根据操作提示通过分布式流转实现随机传递炸弹的效果。

邀请用户（Hi3516d）

![show](media/3516_1.gif)

开始游戏（Hi3516d）

![show](media/3516_2.gif)

开始游戏（HH-SCDAYU200）

![show](media/3568_start_game.gif)

#### 2.涉及OpenHarmony技术特性

- ETS UI
- 分布式调度

#### 3.支持OpenHarmony版本

OpenHarmony 3.0 LTS、OpenHarmony 3.1 Beta。

#### 4.支持开发板

- 润和HiSpark Taurus AI Camera(Hi3516d)开发板套件（OpenHarmony 3.0 LTS、OpenHarmony 3.1 Beta）
- 润和大禹系列HH-SCDAYU200开发板套件（OpenHarmony 3.1 Beta，该开发板无3.0 LTS版本）

## 二、快速上手

#### 1.标准设备环境准备

润和HiSpark Taurus AI Camera(Hi3516d)开发板套件：

- [获取OpenHarmony源码](https://www.openharmony.cn/pages/0001000202/#%E5%AE%89%E8%A3%85%E5%BF%85%E8%A6%81%E7%9A%84%E5%BA%93%E5%92%8C%E5%B7%A5%E5%85%B7)，OpenHarmony版本须3.0LTS或3.1 Beta；
- [安装开发板环境](https://www.openharmony.cn/pages/0001000400/#hi3516%E5%B7%A5%E5%85%B7%E8%A6%81%E6%B1%82)
- [开发板烧录](https://www.openharmony.cn/pages/0001000401/#%E4%BD%BF%E7%94%A8%E7%BD%91%E5%8F%A3%E7%83%A7%E5%BD%95)

润和大禹系列HH-SCDAYU200开发套件：

- [润和RK3568开发板标准设备上手-HelloWorld](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/rk3568_helloworld)

#### 2.应用编译环境准备

- 下载DevEco Studio 3.0.0.601版本 [下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio#download_beta)；
- 配置SDK，参考 [配置OpenHarmony-SDK](https://www.openharmony.cn/pages/00090001/#%E5%89%8D%E6%8F%90%E6%9D%A1%E4%BB%B6)
- DevEco Studio 点击File -> Open 导入本下面的代码工程BombGame

#### 3.项目下载和导入

 项目地址：https://gitee.com/openharmony-sig/knowledge_demo_entainment/tree/master/FA/BombGame

1）git下载

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_entainment.git --depth=1
```

2）项目导入

打开DevEco Studio,点击File->Open->下载路径/FA/BombGame

#### 4.安装应用

- [配置应用签名信息](https://www.openharmony.cn/pages/00090003/#%E7%94%9F%E6%88%90%E5%AF%86%E9%92%A5%E5%92%8C%E8%AF%81%E4%B9%A6%E8%AF%B7%E6%B1%82%E6%96%87%E4%BB%B6)

- 安装应用

  打开**OpenHarmony SDK路径 \toolchains** 文件夹下，执行如下hdc_std命令，其中**path**为hap包所在绝对路径。

  ```text
  hdc_std install -r path\entry-debug-standard-ark-signed.hap
  ```

**PS**分布式流转流转时，需要多个开发板，连接同一个wifi或使用网线连接

环境准备，源码下载，编译，烧录设备，应用部署的完整步骤请参考[这里](https://blog.csdn.net/sd2131512/article/details/121403543)

## 三、关键代码解读

#### 1.目录结构

```
.
├─entry\src\main
│     │  config.json // 应用配置
│     ├─ets
│     │  └─MainAbility
│     │      │  app.ets //ets应用程序主入口
│     │      └─pages
│     │              CommonLog.ets // 日志类
│     │              game.ets // 游戏首页
│     │              RemoteDeviceManager.ets // 设备管理类
│     └─resources // 静态资源目录
│         ├─base
│         │  ├─element
│         │  ├─graphic
│         │  ├─layout
│         │  ├─media // 存放媒体资源
│         │  └─profile
│         └─rawfile
```

#### 2.日志查看方法

```
hdc_std shell
hilog | grep BombGame 
```

#### 3.关键代码

- UI界面，设备流转：game.ets
- 设备管理：RemoteDeviceManager.ets

## 四、如何从零开发传炸弹

[从零开发传炸弹小游戏](quick_develop.md)

## 五、参考链接

- [OpenHarmony基于TS扩展的声明式开发范式](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/arkui-ts/Readme-CN.md)
- [OpenHarmony应用接口](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/apis/Readme-CN.md)
- [OpenHarmonyETS开发FAQ](https://gitee.com/Cruise2019/team_x/blob/master/homework/ets_quick_start/ETS%E5%BC%80%E5%8F%91FAQ.md)
- [润和RK3568开发板标准设备上手-HelloWorld](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/rk3568_helloworld)
