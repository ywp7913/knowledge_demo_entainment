# 意见分歧解决器应用代码介绍

### 简介

意见分歧解决器是装在OpenHarmony系统中的应用，在两台设备间通过分布式数据共享实现数据间的通信，两个人的意见发生了分歧，可以通过这个应用在两台设备间进行石头剪刀布的对战。

### 样例效果

![样例效果](resources/1.gif)

### 代码结构

本demo包括entry模块

![代码结构](resources/1.png)

### 安装部署

##### 1.代码编译运行步骤

1）下载此项目，[链接](https://gitee.com/openharmony-sig/knowledge_demo_entainment/tree/master/FA/DistrubutedGuessGame )。

2）开发环境搭建，开发工具：DevEco Studio 3.0 Beta1 。

3）导入OpenHarmony工程：OpenHarmony应用开发，只能通过导入Sample工程的方式来创建一个新工程，具体可参考[导入Sample工程创建一个新工程](https://gitee.com/openharmony/docs/blob/update_master_0323/zh-cn/application-dev/quick-start/import-sample-to-create-project.md)

4）OpenHarmony应用运行在真机设备上，需要对应用进行签名，请参考[OpenHarmony应用签名](https://gitee.com/openharmony/docs/blob/update_master_0323/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md)

### 约束限制

1. 提前准好已实名认证的开发者联盟账号
2. 两台设备需要提前做好组网环境，需要在同一局域网环境下，通过点击“测试分布式任务”进入配置页面进行配置，配置完成后退出配置页面，通过点击“意见分歧解决器”，进入对战页面。
