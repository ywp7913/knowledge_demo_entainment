﻿# [OpenHarmony上跑起ArkUI小游戏](../../FA/SnakeGame)

## 一、简介

#### 1.样例效果

我们基于JS开发了一款如下视频所示贪吃蛇小游戏应用。

![模拟器效果](resources/%E6%8C%89%E9%94%AE%E6%93%8D%E4%BD%9C.gif)

打开应用，按下↓方向键，即可控制小蛇开始游戏。实际演示如下：

![实际演示](resources/%E6%B8%B8%E6%88%8F%E6%BC%94%E7%A4%BA.gif)


#### 2.涉及OpenHarmony技术特性

- JS UI

####  3.支持OpenHarmony版本

OpenHarmony 3.0 LTS

#### 4、支持的开发板

- 九联科技Unionpi Tiger(A311D)开发板

## 二、快速上手

#### 1、标准设备环境准备

- [九联科技Unionpi Tiger(A311D)开发板，源码编译烧录参考](https://gitee.com/openharmony-sig/device_unionpi)

#### 2、应用编译环境准备

- 下载DevEco Studio [下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio#download_beta)；

- 配置SDK，参考 [配置OpenHarmony-SDK](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md);

- DevEco Studio 点击File -> Open 导入本下面的代码工程SnakeGame;

#### 3、[项目下载和导入](../../FA/SnakeGame)

1) git下载

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_entainment.git
```

2) 项目导入

打开DevEco Studio,点击File->Open->下载路径/FA/SnakeGame

#### 4、配置签名

[配置应用签名信息](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md)

#### 5、安装应用

将hap包放入U盘，将U盘插入开发板的 USB 接口，电脑与开发板的 Micro USB 接口连接，进入U盘的文件夹内，输入如下指令即可完成安装。

```
bm install -p snakegame.hap
```

## 三、代码解读

完整的项目结构目录如下：

```
├─entry
│  └─src
│      └─main
│          │  config.json // 应用配置
│          │
│          ├─js
│          │  └─MainAbility
│          │      ├─common // 存放图片视频等资源
│          │      │
│          │      ├─i18n // 语言包
│          │	  │
│          │      └─pages
│          │		└─index
│          │              	index.css  // 页面样式
│          │              	index.hml  // 游戏首页
│          │              	index.js  // 游戏逻辑
│          │     app.js // 应用生命周期
│          │
│          └─resources // 静态资源目录
│              ├─base
│              │  ├─element
│              │  │
│              │  └─media // 存放媒体资源
│              └─rawfile
```

步骤如下：编写声明式UI界面、样式修改和编写游戏逻辑

### 1. 编写声明式UI界面

##### 1.1 新增工程

在 DevEco Studio 中点击 File -> New Project -> Empty Ability->Next，Language 选择 JS 语言，最后点击 Finish 即创建成功。 

![创建项目](resources/%E5%88%9B%E5%BB%BA%E9%A1%B9%E7%9B%AE.png)


##### 1.2 编写游戏页面

![游戏页面](resources/%E6%B8%B8%E6%88%8F%E9%A1%B5%E9%9D%A2.png)

效果图如上，可以分为三部分

- 贪吃蛇移动区域展示

  采用<canvas>组件描绘出贪吃蛇的移动区域

  ```
  <canvas ref="canvasref" style="width: 720px; height: 200px; background-color: black;margin-left: 10px;margin-right: 10px;"></canvas>
  ```

- 游戏按键

  通过放置四张上下左右按键图片来控制贪吃蛇的移动， 想要的效果是方向键如同键盘方向的布局，所以只需对下面三个按键进行处理。可以用一个div标签把它们包裹起来，再定义一个新属性 

  ```
  <div class="btn">
  <!--上按键-->
      <image src="/common/up.png" class="backBtnup" onclick="onStartGame(1)"></image>
  
  <!--下面三个按键用同一样式，所以用同一个div包围-->
      <div class="directsecond">
      <!--左按键-->
          <image src="/common/left.png" class="backBtnleft" onclick="onStartGame(2)"></image>
      <!--下按键-->
          <image src="/common/down.png" class="backBtncenter" onclick="onStartGame(3)"></image>
      <!--右按键-->
          <image src="/common/right.png" class="backBtnright" onclick="onStartGame(4)"></image>
      </div>
  </div>
  ```

- 游戏分数

  ```
  <!--用if判断，如果游戏结束，则显示该模块-->
      <text if="{{gameOver}}" class="scoretitle">
          <span>Game Over!!!</span>
      </text>
  <!--用if判断，如果游戏没有结束，则显示该模块。显示得分-->
      <text if="{{!gameOver}}" class="scoretitle">
          <span>Score: {{score}}</span>
      </text>
  ```

### 2. 编写游戏逻辑

##### 2.1 绑定点击事件

按钮的触发是通过点击屏幕，所以要有点击事件，鼠标点击事件是有对应的方法，通过方法传不同的参数来区别不同的方向

##### 2.2 食物的生成

随机生成，判断食物生成的位置如果出现在蛇身上，则重新生成。

##### 2.3 蛇身的初始化 （由于案例比较简单，所以没有设定随机生成初始位置）

给定长度并设定一个空数组，通过for循环，把x和y的坐标push进数组，作为蛇身每格的位置。

##### 2.4 蛇运动

移动是靠每帧重绘位置，吃到食物就头部立刻加长，没吃到食物就去掉尾部，把头部方向指向的下一个位置记录到数组头部，等下次刷新帧

##### 2.5 判定游戏结束

碰壁、相对方向移动、形成环路

## 四、参考链接

- [OpenHarmony基于JS扩展的类Web开发范式](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/arkui-js/Readme-CN.md)

- [OpenHarmony应用接口](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/apis/Readme-CN.md)

