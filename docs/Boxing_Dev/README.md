## 一、简介：

受环境和器材的限制，家庭互动游戏，越来越受到广大家庭欢迎。本样例为大家呈现的是一款拳击互动游戏。依托OpenAtom OpenHarmony（以下简称“OpenHarmony”）3.2Beta1操作系统，样例分为应用端和设备端两部分，本文主要介绍设备端的实现，后续会分享应用端的开发。

设备端：采用[小熊派BearPi-HM Nano（Hi3861）开发板](https://gitee.com/bearpi/bearpi-hm_nano/tree/master)，处理加速度计传感器数据。

应用端：采用 [润和DAYU200（RK3568）开发板](https://gitee.com/hihope_iot/docs/blob/master/HiHope_DAYU200/docs/README.md)，主要处理显示及音效。

## 二、原理：

相比正常状态下，挥拳动作会引起手臂较大的加速度变化。根据这个特征，我们使用BearPi-HM_Nano开发板的扩展模块E53_SC2，它内部集成了MPU6050传感器，能够读取加速度的大下。通过挥拳实验，记录数据，得到挥拳时的加速度阈值，从而能准确实时捕捉到挥拳动作。使用无线通信，再配合润和DAYU200开发板应用端的使用，做到设备端与应用端同步互动效果。

## 三、加速度传感器说明：

首先给大家介绍一下加速度计，加速度计传感器的使用，主要涉及两点：1、重力加速度g的理解；2、如何把MPU6050寄存器数据转化为有单位的数据？

1、本样例使用的加速度传感器是MPU6050，它有±2g, ±4g, ±8g 和±16g四个量程可以选择。一个g是指一个重力加速度，代表9.8米/秒²大小。举个例子：假如设备从高处掉落，其加速计测量到的加速度将为0g，因为传感器没有受到力的挤压，处在失重状态；假如设备水平放在桌面上，则加速计测量出的加速度为1g(9.8米/秒²)，我们可以理解为受到1g的压力；

2、MPU6050采用16位的ADC采样。什么意思？举个例子：如果量程选择（通过寄存器选择）是±2g，16位的ADC采样，表示的含义是用65536（即2的16次方）种情况去表达-2到+2g的情况；如下datasheet截图显示，AFS_SEL=0，表示±2g量程，当数据寄存器的数据为16384，对应表示受到1g的力。例如：数据寄存器读取到的值为X，则对应受到的力的大小为Y，Y=X/16384，单位是g。

![image-20220715170516232](./res/Acc_range.png)

## 三、代码解析：

设备端代码主要分为两个线程：1、传感器数据处理线程；2、TCP通信线程；它们之间通过事件的方式同步。

1 传感器数据处理线程主要函数说明：

```c

//E53_SC2模块MPU6050传感器数据处理主要流程
static void ExampleTask(void)
{
    uint8_t ret;
   
    int X = 0, Y = 0, Z = 0;

    ret = E53SC2Init();//MPU6050传感器初始化及配置，配置为+—8g量程
    if (ret != 0) 
    {
        printf("E53_SC2 Init failed!\r\n");
        return;
    }
    while (1) {

        ret = E53SC2ReadData(&data);//MPU6050传感器寄存器数据读取
        if (ret != 0) {
            printf("E53_SC2 Read Data!\r\n");
            return;
        }
        AccDataHandle(&data);//MPU6050传感器数据处理，转化为单位为g的数据

        if(myCaldata.Accel[ACCEL_X_AXIS] < 0)//把数据统一处理为正数
        {
            myCaldata.Accel[ACCEL_X_AXIS] = myCaldata.Accel[ACCEL_X_AXIS] * -1.0; 
        }

        if(myCaldata.Accel[ACCEL_Y_AXIS] < 0)
        {
            myCaldata.Accel[ACCEL_Y_AXIS] = myCaldata.Accel[ACCEL_Y_AXIS] * -1.0; 
        }

        if(myCaldata.Accel[ACCEL_Z_AXIS] < 0)
        {
            myCaldata.Accel[ACCEL_Z_AXIS] = myCaldata.Accel[ACCEL_Z_AXIS] * -1.0; 
        }

        //判断是否触发拳击动作的最小加速度值，触发这发起事件
        if( myCaldata.Accel[ACCEL_X_AXIS] > Boxing_ACC || myCaldata.Accel[ACCEL_Y_AXIS] > Boxing_ACC || myCaldata.Accel[ACCEL_Z_AXIS] > Boxing_ACC)
        {
            printf("MPU set flg\r\n");
            osEventFlagsSet(g_eventFlagsId, FLAGS_MSK1);//触发事件
        }

        usleep(10000);//10ms
    }
}

//MPU6050传感器数据处理，转化为单位为g的数据
int AccDataHandle(E53SC2Data *dat)
{
    if(dat->Accel[ACCEL_X_AXIS] < 32764)
    {
        //量程为+-8g，所以分辨率为4096
        myCaldata.Accel[ACCEL_X_AXIS] = dat->Accel[ACCEL_X_AXIS]/4096.0;
    }
    else//数据为负的情况
    {
        myCaldata.Accel[ACCEL_X_AXIS] = 1-(dat->Accel[ACCEL_X_AXIS]-49512)/4096.0;
    }

    if(dat->Accel[ACCEL_Y_AXIS] < 32764)
    {
        myCaldata.Accel[ACCEL_Y_AXIS] = dat->Accel[ACCEL_Y_AXIS]/4096.0;
    }
    else
    {
        myCaldata.Accel[ACCEL_Y_AXIS] = 1-(dat->Accel[ACCEL_Y_AXIS]-49512)/4096.0;
    }

    if(dat->Accel[ACCEL_Z_AXIS] < 32764)
    {
        myCaldata.Accel[ACCEL_Z_AXIS] = dat->Accel[ACCEL_Z_AXIS]/4096.0;
    }
    else
    {
        myCaldata.Accel[ACCEL_Z_AXIS] = 1-(dat->Accel[ACCEL_Z_AXIS]-49512)/4096.0;
    }

return 0;
}
```

2 TCP通信线程主要函数说明：

本样例的网络通信，服务端是润和DAYU200（RK3568）开发板，客户端则是小熊派BearPi-HM Nano（Hi3861）开发板。它们之间采用TCP机制通信。

通信线程一直等待传感器数据处理线程触发拳击事件，触发后通信线程则会发送信息给服务端：

```c
static void TCPClientTask(void)
{
    // 在sock_fd 进行监听，在 new_fd 接收新的链接
    int sock_fd;
    uint32_t flags;

    // 服务器的地址信息
    struct sockaddr_in send_addr;
    socklen_t addr_length = sizeof(send_addr);
    char recvBuf[512];

    // 连接Wifi
    WifiConnect(CONFIG_WIFI_SSID, CONFIG_WIFI_PWD); 

    // 创建socket
    if ((sock_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("create socket failed!\r\n");
        exit(1);
    }

    // 初始化预连接的服务端地址
    send_addr.sin_family = AF_INET;
    send_addr.sin_port = htons(CONFIG_SERVER_PORT);
    send_addr.sin_addr.s_addr = inet_addr(CONFIG_SERVER_IP);
    addr_length = sizeof(send_addr);

    // 连接
    connect(sock_fd,(struct sockaddr *)&send_addr, addr_length);
    printf("TCPClient connect success\r\n");

    while (1) {
        bzero(recvBuf, sizeof(recvBuf));
        // 等待事件是否触发
        flags = osEventFlagsWait(g_eventFlagsId, FLAGS_MSK1, osFlagsWaitAny, osWaitForever);

        printf("TCP get flag\r\n");
        sprintf(sendbuf,"right\r\n");
        send(sock_fd, sendbuf, strlen(sendbuf), 0);//tcp发出触发信息

        // 线程休眠一段时间
        usleep(100000);//100ms
    }

    // 关闭这个 socket
    closesocket(sock_fd);
}
```

## 四、代码构建、编译及烧录：

1 源码下载，[地址参考](https://gitee.com/openharmony/docs/blob/master/zh-cn/release-notes/OpenHarmony-v3.2-beta1.md)文章结尾处链接；

2 在源码根目录下的vendor目录下，新建team_x文件夹；

3 把boxing文件夹，拷贝到team_x目录下，如下图所示：

![image-20220718162741401](./res/mulu.png)

3 在源码目录下，输入hb set，然后选择当前文件路径，即输入.(点)，然后通过方向键选取team_x下的boxing，如下图；

![image-20220718163427564](./res/hbset.png)

4 输入hb build -f,开始编译，编译成功后，会在根目录下的out目录生成Hi3861_wifiiot_app_allinone.bin，如下图：

![编译成功](./res/buildOK.PNG)

![bin文件路径](./res/outbin1.PNG)

5 用HiBurn工具烧录程序，[烧录参考链接](https://gitee.com/bearpi/bearpi-hm_nano/tree/master/applications/BearPi/BearPi-HM_Nano/docs/quick-start)在文章结尾处；

烧录成功后，可以本地验证是否成功：

1 电脑端使用网络调试助手软件，建立TCP服务端，电脑端建立服务端需要注意以下几点；

​    （1）电脑与BearPi-HM Nano开发板连入同一个wifi热点，如图电脑连入热点YYYYYY；

​    （2）BearPi-HM Nano开发板程序设置的IP，电脑的IP，网络调试助手服务端的IP，三者保持一致，如下图；

​    （3）点击网络调试助手的连接按钮，即先启动服务端。

![ip显示](./res/IP.PNG)

2 BearPi-HM Nano开发板串口连入电脑，设置波特率为115200；

3 复位BearPi-HM Nano开发板，复位后，串口会打印WiFi连接成功、TCP连接成功等信息，如下图（右侧）；

4 尝试出拳，挥动开发板，网络助手的TCP服务端能接收到同步信息，如下图（左侧）。

![运行效果](./res/OK.PNG)

## 五、总结：

本文主要讲述了拳击互动游戏中，关于设备端的开发，使用小熊派BearPi-HM Nano（Hi3861）开发板硬件，在小熊派相关基础例程上做了二次开发。本设备端开发，使用了OpenHarmony的线程，事件，GPIO，IIC，TCP通信等相关基础知识，结合加速度计传感器的使用，实现同步交互的功能，集趣味与学习一体。

本样例是[OpenHarmony知识体系工作组](https://gitee.com/openharmony-sig/knowledge/blob/master/docs/co-construct_demos/README_zh.md)（相关链接在文章末尾）为广大开发者分享的样例，知识体系工作组结合日常生活环境，给开发者规划了各种场景的Demo样例，如智能家居场景、影音娱乐场景、运动健康场景等；OpenHarmony知识体系工作组，欢迎广大开发者一同参与OpenHarmony的开发，更加完善样例，相互学习，相互促进。

## 六、参考连接：

本样例代码下载链接：

https://gitee.com/openharmony-sig/knowledge_demo_entainment/tree/master/dev/team_x/boxing

OpenHarmony 3.2 Beta1源码下载：

https://gitee.com/openharmony/docs/blob/master/zh-cn/release-notes/OpenHarmony-v3.2-beta1.md

BearPi-HM Nano（Hi3861）开发板代码烧录参考链接：

https://gitee.com/bearpi/bearpi-hm_nano/tree/master/applications/BearPi/BearPi-HM_Nano/docs/quick-start

OpenHarmony知识体系共建开发仓：

https://gitee.com/openharmony-sig/knowledge/blob/master/docs/co-construct_demos/README_zh.md

润和RK3568开发板标准设备上手-HelloWorld:

https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/rk3568_helloworld

润和DAYU200（RK3568）开发板介绍：

https://gitee.com/hihope_iot/docs/blob/master/HiHope_DAYU200/docs/README.md

小熊派BearPi-HM Nano开发板学习路径：

https://gitee.com/bearpi/bearpi-hm_nano/tree/master

小熊派陀螺仪样例说明：

https://gitee.com/bearpi/bearpi-hm_nano/blob/master/applications/BearPi/BearPi-HM_Nano/sample/C4_e53_sc2_axis/README.md
