经典扫雷小游戏（eTS+API9）
---
### 介绍
1992年4月6日，"扫雷"小游戏首次搭载在Windows3.1，至今正好30周年，如今被贴上了"暴露年龄"标签😂😂，本节实现"扫雷"小游戏并运行在DAYU200开发板上。
### 准备环境
开发板：DAYU200
系统版本：OpenHarmony v3.2 Beta1
Api版本：OpenHarmony SDK API9
开发工具：DevEco Studio 3.0 Beta4

### 准备工程
#### 工程下载
```shell
git clone https://gitee.com/openharmony-sig/knowledge_demo_entainment.git
```
#### 工程导入
打开DevEco Studio,点击File->Open->下载路径/FA/MineSweeping
#### 配置签名信息
点击File > Project Structure > Project > Signing Configs界面勾选“Automatically generate signing”，等待自动签名完成即可，点击“OK”。
#### 点击工具栏运行查看效果

![](images/start_game.png)

![](images/play_game.png)

![](images/end_game.png)

#### 说明

更改`index`页面`boardRowsNum`、`boardColsNum`、`maxMineNum`的值实现不同难度。当然你也可以对其进行扩充，比如设定简单、中级、高级三种模式，或者你也可以在实现复杂的功能，如插旗，棋盘格不明确标识等。