# OpenHarmony 基于LYEVK-3861开发板 IOT套件的音乐播放

​    `在LYEVK-38613861开发板的学习过程中，发现缺少基于OpenHarmony 2.0的相关文档，同时缺少基于2.0系统 IoT接口的应用示例。在学习PWM相关接口的过程中，了解到PWM接口驱动蜂鸣器可以实现类似音乐播放的效果，个人觉得是个不错的思路，就有了本次的学习开发之旅。`

## 快速上手（推荐使用DevEco Device Tool工具下载，编译，烧录）

### 准备硬件环境

+ LYEVK-3861A套件

### 准备开发环境

开发环境安装配置参照文档：[DevEco Device Tool 环境搭建](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/blob/master/docs/%E5%8D%97%E5%90%91IDE%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/README.md)

### 准备工程

本用例采DevEco Device Tool工具进行开发，当配置完开发环境后，我们可以在IDE上进行工程的配置下载。

- 打开DevEco Device Tool，连接远程linux服务器：[DevEco Device Tool 环境搭建](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/blob/master/docs/%E5%8D%97%E5%90%91IDE%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/README.md)

- 点击左下角DevEco插件图标，然后点击左边框出现的主页，弹出主页界面，主页中选择新建项目，如下图：

&nbsp;![main_config](resource/1新建工程.PNG)

- 配置下载工程

&nbsp;![config && download](resource/2配置工程.PNG)

  如上图所示，填写对应样例工程的名称，选择对应的样例组件和样例工程存放路径后，点击创建即可进行样例工程的下载。下载界面如下：

&nbsp;![download](resource/3获取源码.PNG)

当左下角显示正在下载OpenHarmony镜像时，耐心等待下载完成即可。

&nbsp;![download](resource/4下载源码.PNG)

### 准备工具链

- 在Projects中，点击Settings按钮，进入music配置界面。

- 在toolchain页签中，DevEco Device Tool会自动检测依赖的编译工具链是否完备，如果提示部分工具缺失，可点击SetUp按钮，自动安装所需工具链。

- 如果出现安装pip组件失败，可参考[修改Python源的方法](https://gitee.com/link?target=http%3A%2F%2Fdevice.harmonyos.com%2Fcn%2Fdocs%2Fdocumentation%2Fguide%2Fide-set-python-source-0000001227639986)进行修改，完成尝试重新安装。

- 工具链自动安装完成后如下图所示。

  &nbsp;![工具链配置成功](resource/5配置工具链.PNG)

### 编译

样例代码下载完成后，DevEco Device Tool会重新要求连接远程服务器，输入密码连接后会进入对应的代码编辑界面，此时点击左下角DevEco插件图标，选择PROJECT TASKS可以查看到对应的样例工程，点击build选项进行编译，并可在终端查看对应的编译结果。

&nbsp;![build ok](resource/6编译成功.PNG)

固件生成在对应工程目录的out/hispark_pegasus/wifiiot_hispark_pegasus/目录下。

![固件路径](resource/7固件路径.PNG)

### 烧录/安装

编译完成后可以通过DevEco Device Tool进行烧录，在烧录前需要做一些烧录的配置:

#### 配置准备

在配置烧录前需要先查看DevEco Device Tool是否可以正常识别串口。

- 点击左边栏"REMOTE DEVELOPMENT"，找到 并点击” Local PC “ 选项。

- 查看 Local PC右边图标

  如若图标为![方型图标](resource/zhengfang_icon.png), 则代表DevEco Device Tool已连接本地，可以正常识别串口。

  如若图标为![箭头图标](resource/sanjiao_icon.png),则代表DevEco Device Tool未连接本地，不能识别串口，此时需要点击该绿色图标进行连接，连接成功后图标会变为![方型图标](resource/zhengfang_icon.png)。

&nbsp;![check local pc](resource/dev-8.png)

- 点击主页，在主页选择对应工程，点击配置工程进入到配置页面

&nbsp;![config project](resource/8打开工程.PNG)

#### 配置串口

配置页面选择的板级配置页面，在该页面查找到烧录选项，配置烧录选项中的upload_port和upload_protocol，upload_port选择开发板对应的串口号，upload_protocol默认选择hiburn-serial，最后点击右上角的保存按钮进行保存。

&nbsp;![config serail port](resource/9配置烧录串口.PNG)

#### 烧录

当配置完串口以及固件后，直接点击左边栏工程管理中的upload即可，此时下方终端会出现对应烧录的信息，当终端出现BootromDownloadBoot字样，按下开发板的 “RESET” 键复位开发板即可。

&nbsp;![burn](resource/dev-11.png)

## 代码分析

### 2.1 相关基础知识介绍

##### PWM输出方波的IOT接口

鸿蒙系统IoT硬件子系统提供了一些外设相关的接口，目录位于：

`base/iot_hardware/peripheral/interfaces/kits`

PWM相关接口，接口头文件为iot_pwm.h，其中开始输出方波的接口为：

`unsigned int IoTPwmStart(unsigned int port, unsigned short duty, unsigned int freq);`

接口参数介绍：

freq：IoTPwmStart接口中freq参数是分频倍数，PWM实际输出的方波频率等于 PWM时钟源频率 除以 分频倍数，即
`f = Fcs / freq`
其中，Fcs是PWM时钟源频率；
duty:IoTPwmStart接口的duty参数可以控制输出方波的占空比，占空比是指PWM输出的方波波形的高电平时间占整个方波周期的比例，具体占空比值取值为1到99，例如想要输出占空比 50%的方波信号，那么duty填的值就要是50。

##### 音符-频率对应关系

![音频对应关系](./resource/音频对应关系.png)

这个表中有一个规律——音高升高一个八度，频率升高一倍。

`hi_u32 hi_pwm_set_clock(hi_pwm_clk_source clk_type);`

160M时钟源条件下，输出方波的最低频率是：160M/65535=2441.44...，这个频率略高，在上面的表格中没有找到音名。通过调用hi_pwm_set_clock接口，可以修改时钟源，将时钟源设置为晶体时钟且时钟频率为40MHz，40M/65535= 610.3...，这样就能够输出E5及以上的所有音符。

### 2.2  曲谱转换

由于个人比较喜欢《蜜雪冰城》，我选择了《蜜雪冰城主题曲》的曲谱作为素材，简谱如下：

![image-20210805182918687](./resource/image-20210805182918687.png)

#### 简谱转换

每个音符都需要有节拍，在外面的代码里体现为停顿时间，不同音符的不同停顿时间，可以实现简单的音乐起伏。

常见的节拍简谱对应：

![image-20210805190849674](./resource/image-20210805190849674.png)

通过简谱和以上表格的对应，就可以将我们现有的简谱，转换成为可以被程序识别的”程序谱子“。

### 2.3  编写代码

#### 接口初始化

修改device/hisilicon/hispark_pegasus/sdk_liteos/build/config/usr_config.mk配置文件，打开PWM编译支持，如已打开，可略过：

```
# CONFIG_UART_DMA_SUPPORT is not set
CONFIG_PWM_SUPPORT=y
# CONFIG_PWM_HOLD_AFTER_REBOOT is not set
```

修改device/hisilicon/hispark_pegasus/sdk_liteos/app/wifiiot_app/init/app_io_init.c的如下代码

```c
#ifdef CONFIG_PWM_SUPPORT 
    /* PWM 0/2/3/4/5 配置同理 */
    //hi_io_set_func(HI_IO_NAME_GPIO_8, HI_IO_FUNC_GPIO_8_PWM1_OUT);
    //GPIO引脚复用
    hi_io_set_func(HI_IO_NAME_GPIO_8, HI_IO_FUNC_GPIO_8_GPIO);  //button
    hi_io_set_pull(HI_IO_NAME_GPIO_8, HI_IO_PULL_UP);
    hi_io_set_func(HI_IO_NAME_GPIO_9,HI_IO_FUNC_GPIO_9_PWM0_OUT);// PWM
#endif
```

备注：2.0把GPIO的引脚复用从应用层移到了板级，对IO的功能做了更加细分的处理，之前一直以为2.0移除了相关的接口实现，最后才发现2.0做了功能上的优化。

#### 实例代码

##### 按键驱动

![button](./resource/image-20210806100515075-1628511751140.png)

说明：按键主要是为了开启音乐的播放。

##### 程序”曲谱

《蜜雪冰城主题曲》的“程序”曲谱，定义如下：

##### ![image-20210806101347804](./resource/image-20210806101347804.png)

以上的曲谱，看起来比较直观，也比较容易理解，其中的一些宏定义定义如下：

![曲谱定义](./resource/image-20210806101221886.png)

有了以上的枚举，可以自己直接谱曲，甚至不用拘泥于现有的曲子，当然放出来的具体效果如何，就因人而异了。

##### 音符频率

以下是IoTPwmStart接口的入口参数freq的定义，和前述枚举是一一对应的:

![image-20210806102414462](./resource/image-20210806102414462.png)

##### 音乐处理

```c
/* 音乐处理*/
static void *BeeperMusicTask(const char *arg)
{
    (void)arg;
    int status = 0;
    printf("BeeperMusicTask start!\r\n");

    hi_pwm_set_clock(PWM_CLK_XTAL); // 设置时钟源为晶体时钟(40MHz，默认时钟源160MHz)

    while (1)
    {
        usleep(M_INTERVAL_TIME_US);
        /*第一次点击按键播放，播放完以后按键才能继续生效*/
        if (music == 1)
        {
            for (size_t i = 0; i < sizeof(g_interval) / sizeof(g_interval[0]); i++)
            {  
                uint32 tune = g_interval[i].tuneNotes; // 音符
                uint16 freqDivisor = g_tuneFreqs[tune];
                uint32 tuneInterval = g_interval[i].interval * (TICKS_DELAY); // 音符时间
                IoTPwmStart(IOT_PWM_PORT0, PWM_DUTY, freqDivisor);
                usleep(tuneInterval);
                IoTPwmStop(IOT_PWM_PORT0);
                music = 0;
            }
        }
    }

    return NULL;
}
```

##### 功能展示

​    以上都完成之后，就可以编译、烧录，并测试最后的完成效果。

​    说明：<br>
    1、本程序需要Hi3861开发板配合交通灯板实现完整的演示

​    2、烧录完成之后，RESET重启开发板之后，音乐不会播放；

​    3、重启开发板之后，点击交通灯板开发播放音乐；

播放过程中，按键失去效果，待播放完成之后，再次点击按键，音乐继续播放。

[视频演示链接](https://www.bilibili.com/video/BV1fh411q7N1)
