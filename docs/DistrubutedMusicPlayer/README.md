# [OpenHarmony 分布式音乐播放器](../../FA/DistrubutedMusicPlayer)

## 一、简介
#### 1.样例效果
[本样例](../../FA/DistrubutedMusicPlayer)是基于OpenHarmony 3.2 Beta3，使用ArkTs语言编写，使用Stage模型的应用。该样例可以播放多首wav格式的歌曲，并可使用分布式调度特性，进行跨设备流转。

![show](media/music.jpg)

动态效果：

![3568](media/music_3568.gif)

#### 2.涉及OpenHarmony技术特性
- arkTS-UI
- 音频
- 分布式调度

####  3.支持OpenHarmony版本
OpenHarmony 3.2 Beta3

#### 4.支持开发板
- 润和大禹系列HH-SCDAYU200开发板套件

## 二、快速上手
#### 1.标准设备环境准备

润和大禹系列HH-SCDAYU200开发套件：

- [润和RK3568开发板标准系统快速上手](https://growing.openharmony.cn/mainPlay/learnPathMaps?id=27)

#### 2.应用编译环境准备
- 下载DevEco Studio  [下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio#download)；

- 配置SDK，参考 [配置OpenHarmony-SDK](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md)。  因使用分布式特性，推荐使用[FullSDK](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.2-Beta3/zh-cn/application-dev/quick-start/full-sdk-switch-guide.md)(非必须)

- 因3.2后分布式能力限制只能系统应用使用，需要提升apl等级：

  找到所使用API版本对应toolchains>版本号>lib>UnsgnedReleasedProfileTemplate.json，更改  "apl": "normal"为  "apl": "system_core"

- DevEco Studio 点击File -> Open 导入本下面的代码工程DistrubutedMusicPlayer

#### 3.项目下载和导入

项目地址：https://gitee.com/openharmony-sig/knowledge_demo_entainment/tree/master/FA/DistrubutedMusicPlayer

1）git下载

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_entainment.git --depth=1
```

2）项目导入

打开DevEco Studio,点击File->Open->下载路径/FA/DistrubutedMusicPlayer


#### 4.安装应用
- **配置自动签名**
  DevecoStudio里，点击“File”-->"Project Structure"-->"Signing Configs",勾选“Automatically generate signature”

- **安装应用**

  点击"Run entry"从IDE直接启动应用。

  或者打开**OpenHarmony SDK路径 \toolchains** 文件夹下，执行如下hdc_std命令，其中**path**为hap包所在绝对路径。
  
  ```text
  hdc_std install -r path\entry-debug-standard-ark-signed.hap
  ```

**PS** 分布式流转时，需要多个开发板，连接同一个wifi或使用网线连接


##  三、关键代码解读
#### 1.目录结构
```
├── AppScope
├── entry\src\main\ets
│   ├── Application 
│   	├──MyAbilityStage.ets
│   ├── Common 
│   	├──components
│   		├──DeviceListDialog.ets //设备列表自定义弹窗组件
│   		├──MusicListDialog.ets //音乐列表自定义弹窗组件
│   		├──MusicItem.ets //首页音乐项组件
│   	├──model
│   		├──DeviceInfo.ts // 设备实体类
│   		├──Music.ts // 音乐实体类
│   	├──CommonLog.ts // 日志管理类
│   	├──PlayerManager.ts //音乐播放管理类
│   	├──RemoteDeviceManager.ts //设备管理类，用于发现设备，认证设备
│   ├── MainAbility  
│   	├──MainAbility.ts 
│   ├──pages
│   	├──index.ets //播放页
│   	├──main.ets //音乐播放器首页
├── entry\src\main\resources
```

#### 2.日志查看方法
```
hdc_std shell
hilog | grep MyOpenHarmonyPlayer  
```

#### 3.关键代码
- UI界面：index.ets，main.ets
- 音乐播放：PlayerManager.ts
- 设备管理：RemoteDeviceManager.ts

##  四、参考链接
- [润和RK3568开发板标准系统快速上手](https://growing.openharmony.cn/mainPlay/learnPathMaps?id=27)
- [官网学习路径：OpenHarmony标准系统应用开发](https://growing.openharmony.cn/mainPlay/learnPathMaps?id=46)
- [OpenHarmony js 分布式播放器Sample](https://gitee.com/openharmony/applications_app_samples/tree/master/ability/JsDistributedMusicPlayer)

