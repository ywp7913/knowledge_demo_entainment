# [用JS写一个OpenHarmony拼图小游戏](../../FA/Jigsaw)

## 一、简介

#### 1.样例效果

[本Demo](../../FA/Jigsaw)是基于OpenHarmony3.0 LTS，使用JS语言编写的拼图小游戏。

![游戏效果展示](resources/%E6%95%88%E6%9E%9C%E5%B1%95%E7%A4%BA.gif)

#### 2.涉及OpenHarmony技术特性

- JS UI

####  3.支持OpenHarmony版本

OpenHarmony 3.0 LTS

#### 4、支持的开发板

- 九联科技Unionpi Tiger(A311D)开发板

## 二、快速上手

#### 1、标准设备环境准备

- [九联科技Unionpi Tiger(A311D)开发板，源码编译烧录参考](https://gitee.com/openharmony-sig/device_unionpi)

#### 2、应用编译环境准备

- 下载DevEco Studio [下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio#download_beta)；

- 配置SDK，参考 [配置OpenHarmony-SDK](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md);

- DevEco Studio 点击File -> Open 导入本下面的代码工程FlightGame;

#### 3、项目下载和导入

1) git下载

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_entainment.git
```

2) 项目导入

打开DevEco Studio,点击File->Open->下载路径/FA/Jigsaw

#### 4、配置签名

[配置应用签名信息](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md)

#### 5、安装应用

将hap包放入U盘，将U盘插入开发板的 USB 接口，电脑与开发板的 Micro USB 接口连接，进入U盘的文件夹内，输入如下指令即可完成安装。

```
bm install -p jigsaw.hap
```

## 三、关键代码解读

#### 1、目录结构

```
.
├─entry\src\main
│     │  config.json // 应用配置
│     ├─js
│     │  └─MainAbility
│     │      │  app.js
│     │      ├─common
│     │      │       images.js // 图片列表
│     │      ├─i18n
│     │      └─pages
│     │              ├─index // 首页
│     │              │       index.css
│     │              │       index.hml
│     │              │       index.js
│     │              └─jigsaw // 游戏页面
│     │                      jigsaw.css
│     │                      jigsaw.hml
│     │                      jigsaw.js
│     └─resources // 静态资源目录
│         ├─base
│         │  ├─element
│         │  └─media // 存放媒体资源
│         └─rawfile
```


#### 2、关键代码解读

[拼图游戏代码解读](https://gitee.com/openharmony-sig/knowledge_demo_entainment/blob/master/docs/Jigsaw/code_explain.md)

## 四、参考链接

- [OpenHarmony基于JS扩展的类Web开发范式](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/arkui-js/Readme-CN.md)

- [OpenHarmony应用接口](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/apis/Readme-CN.md)