## Pegasus智能家居套件样例开发--PWM

### 样例简介

使用润和软件Pegasus Wi-Fi IoT智能家居套件中的蜂鸣器播放两只老虎音乐。

####  工程版本

- 系统版本/API版本：OpenHarmony 3.0 release
- IDE版本：DevEco Device Tool Release 3.0.0.401

### 快速上手

#### 准备硬件环境

-  预装windows系统的PC机


-  Hi3861V100开发板套件


#### 准备开发环境

开发环境安装配置参照文档：[DevEco Device Tool 环境搭建](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/blob/master/docs/%E5%8D%97%E5%90%91IDE%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/README.md)

#### 准备工程

本用例采DevEco Device Tool工具进行开发，当配置完开发环境后，我们可以在IDE上进行工程的配置下载。

- 打开DevEco Device Tool，连接远程linux服务器：[DevEco Device Tool 环境搭建](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/blob/master/docs/%E5%8D%97%E5%90%91IDE%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/README.md)

- 点击左下角DevEco插件图标，然后点击左边框出现的主页，弹出主页界面，主页中选择新建项目，如下图：

&nbsp;![main_config](./resource/1新建工程.PNG)

- 配置下载工程

&nbsp;![config_download](./resource/2配置工程.PNG)

  如上图所示，填写对应样例工程的名称，选择对应的样例组件和样例工程存放路径后，点击创建即可进行样例工程的下载。下载界面如下：

&nbsp;![download](./resource/3获取源码.PNG)

  当左下角显示正在下载OpenHarmony镜像时，耐心等待下载完成即可。

  &nbsp;![main_config](./resource/4下载源码.PNG)

#### 准备工具链

- 在Projects中，点击Settings按钮，进入配置工程界面。

- 在toolchain页签中，DevEco Device Tool会自动检测依赖的编译工具链是否完备，如果提示部分工具缺失，可点击SetUp按钮，自动安装所需工具链。

- 如果出现安装pip组件失败，可参考[修改Python源的方法](https://gitee.com/link?target=http%3A%2F%2Fdevice.harmonyos.com%2Fcn%2Fdocs%2Fdocumentation%2Fguide%2Fide-set-python-source-0000001227639986)进行修改，完成尝试重新安装。

工具链自动安装完成后如下图所示。

&nbsp;![config_toolchain2](./resource/5配置工具链.PNG)

样例代码下载完成后，DevEco Device Tool会重新要求连接远程服务器，输入密码连接后会进入对应的代码编辑界面，此时点击左下角DevEco插件图标，选择PROJECT TASKS可以查看到对应的样例工程，点击build选项进行编译，并可在终端查看对应的编译结果。

&nbsp;![build_ok](./resource/6编译成功.PNG)

固件生成在对应工程目录的out/hispark_pegasus/wifiiot_hispark_pegasus/目录下。

&nbsp;![firm](./resource/7固件路径.PNG)

#### 烧录/安装

编译完成后可以通过DevEco Device Tool进行烧录，在烧录前需要做一些烧录的配置:

##### 配置准备

在配置烧录前需要先查看DevEco Device Tool是否可以正常识别串口。

- 点击左边栏"REMOTE DEVELOPMENT"，找到 并点击” Local PC “ 选项。

- 查看 Local PC右边图标

  如若图标为![zhengfang_iocn](./resource/zhengfang_iocn.png),则代表DevEco Device Tool已连接本地，可以正常识别串口。

  如若图标为![sanjiao_icon](./resource/sanjiao_icon.png),则代表DevEco Device Tool未连接本地，不能识别串口，此时需要点击该绿色图标进行连接，连接成功后图标会变为![zhengfang_iocn](./resource/zhengfang_iocn.png)。
  

&nbsp;![check_local_pc](./resource/check_local_pc.png)

- 点击主页，在主页选择对应工程，点击配置工程进入到配置页面。

##### 查询串口

在DevEco Device Tool中，点击QUICK ACCESS > DevEco Home > Device，查看并记录对应的串口号。

&nbsp;![config_serail](./resource/config_serail.png)

如果对应的串口异常，请根据[Hi3861V100开发板串口驱动安装](https://device.harmonyos.com/cn/docs/documentation/guide/hi3861-drivers-0000001058153433)安装USB转串口的驱动程序。

##### 配置串口

在QUICK ACCESS > DevEco Home > Projects中，点击**Settings**打开工程配置界面。

&nbsp;![config_serail1](./resource/8打开工程.PNG)

在“hi3861”页签，设置烧录选项，包括upload_port、upload_protocol和upload_partitions。

- upload_port：选择已查询的串口号。
- upload_protocol：选择烧录协议，选择“hiburn-serial”。
- upload_partitions：选择待烧录的文件，默认选择hi3861_app。

&nbsp;![config_serail2](./resource/9配置烧录串口.PNG)

##### 烧录

+ 所有的配置都修改完成后，在工程配置页签的顶部，点击**Save**进行保存。点击**Open**打开工程文件，然后在“PROJECT TASKS”中，点击hi3861下的**Upload**按钮，启动烧录。

&nbsp;![burn](./resource/burn.png)

+ 启动烧录后，显示如下提示信息时，请在15秒内，按下开发板上的RST按钮重启开发板。

&nbsp;![continueburn](./resource/continueburn.png)

+ 重新上电后，界面提示如下信息时，表示烧录成功。

&nbsp;![burnsucess](./resource/burnsucess.png)

更多烧录细节可以参考[Hi3861V100开发板烧录](https://device.harmonyos.com/cn/docs/documentation/guide/ide-hi3861-upload-0000001051668683)。

- 烧录文件后，按下reset按键，程序开始运行，蜂鸣器会播放《两只老虎》

### PWM API

| API名称                                                      | 说明              |
| ------------------------------------------------------------ | ----------------- |
| unsigned int IoTPwmInit(WifiIotPwmPort port);                   | PWM模块初始化     |
| unsigned int IoTPwmStart(WifiIotPwmPort port, unsigned short duty, unsigned short freq); | 开始输出PWM信号   |
| unsigned int IoTPwmStop(WifiIotPwmPort port);                   | 停止输出PWM信号   |
| unsigned int IoTPwmDeinit(WifiIotPwmPort port);                 | 解除PWM模块初始化 |
| unsigned int PwmSetClock(WifiIotPwmClkSource clkSource);     | 设置PWM模块时钟源 |

### 交通灯板蜂鸣器与主控芯片（Pegasus）引脚的对应关系

- **蜂鸣器：**GPIO9/PWM0/输出PWM波控制蜂鸣器发出声音

  [详细说明](https://harmonyos.51cto.com/posts/1511)

### 参考资料

+  [系统基础环境搭建](https://gitee.com/openharmony/docs/blob/OpenHarmony_1.0.1_release/zh-cn/device-dev/quick-start/%E6%90%AD%E5%BB%BA%E7%B3%BB%E7%BB%9F%E5%9F%BA%E7%A1%80%E7%8E%AF%E5%A2%83.md)
+  [DevEco Device Tool 环境搭建](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/blob/master/docs/%E5%8D%97%E5%90%91IDE%E7%8E%AF%E5%A2%83%E6%90%AD%E5%BB%BA/README.md)
+  [知识体系](https://gitee.com/openharmony-sig/knowledge)
+  [Hi3861V100开发板烧录](https://device.harmonyos.com/cn/docs/documentation/guide/ide-hi3861-upload-0000001051668683)
+  [3861开发板介绍](https://gitee.com/openharmony/docs/blob/OpenHarmony_1.0.1_release/zh-cn/device-dev/quick-start/Hi3861%E5%BC%80%E5%8F%91%E6%9D%BF%E4%BB%8B%E7%BB%8D.md)
