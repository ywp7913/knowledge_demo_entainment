# [WarChess](../../FA/WarChess)

## 一、简介

#### 1.样例效果
本Demo是基于OpenHarmony3.0 LTS，使用js语言编写的应用。该样例使用js编写，用户通过不同的触屏方式来实现对战棋游戏的游玩效果，支持运行在OpenHarmony3.0+版本标准系统上。

[注意]():该demo引用了部分来源于RPGMaker的图片素材。

![运行展示](resources/demo.gif)

#### 2.涉及OpenHarmony技术特性
- JS UI

#### 3.支持OpenHarmony版本
OpenHarmony 3.0 LTS

## 二、快速上手

- [源码下载点击此处](../../FA/WarChess)

#### 1、标准设备环境准备

- [九联科技Unionpi Tiger(A311D)开发板，源码编译烧录参考](https://gitee.com/openharmony-sig/device_unionpi)

#### 2、应用编译环境准备

- 下载DevEco Studio [下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio#download_beta)；

- 配置SDK，参考 [配置OpenHarmony-SDK](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md);

- DevEco Studio 点击File -> Open 导入本下面的代码工程WarChess;

#### 3、项目下载和导入

1) git下载

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_entainment.git
```

2) 项目导入

打开DevEco Studio,点击File->Open->下载路径/FA/WarChess

#### 4、配置签名

[配置应用签名信息](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md)

#### 5、安装应用

将hap包放入U盘，将U盘插入开发板的 USB 接口，电脑与开发板的 Micro USB 接口连接，进入U盘的文件夹内，输入如下指令即可完成安装。

```
bm install -p warchess.hap
```

## 三、关键代码解读

#### 1.目录结构
```
.
├─entry\src\main
│     │  config.json // 应用配置
│     ├─js
│     │  └─MainAbility
│     │      ├─common
│     │      │    ├─  js //定义一些对象和方法
│     │      │    │    ├─Attack_Time.js
│     │      │    │    ├─Backstate_Map.js
│     │      │    │    └─Chess.js
│     │      │    └─picture // 存放要用的贴图
│     │      ├─i8n
│     │      ├─pages
│     │      │    ├─index
│     │      │    │    ├─index.css
│     │      │    │    ├─index.hml
│     │      │    │    └─index.js // 游戏首页
│     │      │    └─second
│     │      │         ├─second.css
│     │      │         ├─second.hml
│     │      │         └─second.js
│     │      └─app.js
│     └─resources // 静态资源目录
│         ├─base
│         │  ├─element
│         │  ├─media
│         └─rawfile
```

#### 2.关键代码
- UI界面：second.hml
- 游戏逻辑：second.js
- 寻路算法: Backstate_Map.js

## 四、如何开发战棋小游戏

[开发战棋小游戏](https://gitee.com/openharmony-sig/knowledge_demo_entainment/blob/master/docs/WarChess/quick_develop.md)

## 五、参考链接

- [基于JS扩展的类Web开发范式](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/arkui-js/Readme-CN.md)

- [OpenHarmony应用接口](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/apis/Readme-CN.md)

