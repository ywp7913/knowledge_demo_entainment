# [拳击健康游戏应用开发样例](../../FA/Boxing)

### 样例简介

[拳击健康游戏应用](../../FA/Boxing)是基于OpenHarmony 3.2 Beta标准系统上开发的eTS应用，本应用运行于RK3568，游戏开始会随着音乐播放会拳击方库进行随机速度下落，样例利用NAPI组件获取“游戏手柄”（BearPi-HM_Nano开发板）获取游玩者当前挥拳的状态，并进行判断得分，在得分后也能加载显示出不同的击中动画效果

#### 运行效果

![效果图](media/effect.png)

更多效果请点击[查看视频](https://www.bilibili.com/video/BV1NB4y157aE/?vd_source=3bf1d6353a191d63e41ec9d739b0db60)

#### 样例原理

本demo通过NAPI组件进行TCP通信，来获取“手柄”的状态，然后通过计算拳击方块运动时间来计算当前是否处于目标击中区域，从而来判断得分和相应完美/普通击中的动画播放。

![样例原理](media/principle.png)

#### 工程版本

+ 系统版本/API版本：OpenHarmony SDK API 9
+ IDE版本：DevEco Studio 3.0 Beta3

### 快速上手

#### 准备硬件环境

- [搭建标准系统环境](https://gitee.com/openharmony/docs/blob/update_master_0323/zh-cn/device-dev/quick-start/quickstart-standard-env-setup.md)
- 润和DAYU200开发套件

#### 准备开发环境

- 安装最新版[DevEco Studio](https://gitee.com/link?target=https%3A%2F%2Fdeveloper.harmonyos.com%2Fcn%2Fdevelop%2Fdeveco-studio%23download_beta_openharmony)。
- 请参考[配置OpenHarmony SDK](https://gitee.com/openharmony/docs/blob/update_master_0323/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md)，完成**DevEco Studio**的安装和开发环境配置。

#### 准备工程

##### 工程下载

 项目地址：https://gitee.com/openharmony-sig/knowledge_demo_entainment/tree/master/FA/Boxing

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_entainment.git --depth=1
```

应用端源码:[Boxing](../../FA/Boxing)

设备端文档:[Boxing_Dev](../../docs/Boxing_Dev/README.md)

设备端源码:[BoxingGame_NAPI](../../dev/team_x/BoxingGame_NAPI)

##### 工程导入

- DevEco Studio导入本工程;

  打开DevEco Studio,点击File->Open->下载路径/FA/Boxing

#### 编译

- 点击**File > Project Structure** > **Project > Signing Configs**界面勾选“**Automatically generate signing**”，等待自动签名完成即可，点击“**OK**”。如下图所示：![运行](./media/signed.png)

- 点击Build->Build Hap/APPs 编译，编译成功生成entry-default-signed.hap

![编译](./media/build.png)

#### 烧录/安装

- 识别到设备后点击，或使用默认快捷键Shift+F10（macOS为Control+R)运行应用。

![img](./media/install.png)

- [安装应用](https://gitee.com/openharmony/docs/blob/update_master_0323/zh-cn/application-dev/quick-start/installing-openharmony-app.md) 如果IDE没有识别到设备就需要通过命令安装，如下

  打开**OpenHarmony SDK路径 \toolchains** 文件夹下，执行如下hdc_std命令，其中**path**为hap包所在绝对路径。

  ```
  hdc_std install -r path\entry-default-signed.hap//安装的hap包需为xxx-signed.hap，即安装携带签名信息的hap包。
  ```

### 参考资料

- [OpenHarmony 基于TS扩展的声明式开发范式](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.2-Beta1/zh-cn/application-dev/reference/arkui-ts/Readme-CN.md)
- [OpenHarmony应用接口](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.2-Beta1/zh-cn/application-dev/reference/apis/Readme-CN.md)
- [拳击游戏设备端文档](../../docs/Boxing_Dev/README.md)
