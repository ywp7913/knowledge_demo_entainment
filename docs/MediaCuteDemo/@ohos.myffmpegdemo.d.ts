/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

declare namespace myffmpegdemo {
  function add(x: number, y: number): number;
  function videoCute(path1: string, start_time: number, end_time: number, path2: string, callback: AsyncCallback<number>): void;
  function videoCute(path1: string, start_time: number, end_time: number, path2: string): Promise<number>;
  function videoToAacH264(in_file: string, out_video: string, out_audio: string, callback: AsyncCallback<number>): void;
  function videoToAacH264(in_file: string, out_video: string, out_audio: string): Promise<number>;
}

export default myffmpegdemo;


