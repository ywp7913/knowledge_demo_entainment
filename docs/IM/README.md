# OpenHarmony 即时通讯（IM）开发样例

## 样例简介

本样例是基于即时通讯（Instant messaging，简称IM）服务实现的OpenAtom OpenHarmony（简称“OpenHarmony”）应用，允许两人或多人使用互联网即时地传递文字、图片、文件、语音、emoji等讯息，可应用于各类聊天场景，为人们带来更加及时高效的通讯体验。

此外即时通讯平台具备较高的定制化特点，适用于多种行业，客户可以根据自己的需求来定制，实现即时通讯的内部私有化。

#### 运行效果

[本Demo](../../FA/IM)是基于OpenHarmony 3.1 release系统，使用JS语言编写的应用。

聊天界面 

<img src="resource/im.bmp" alt="im" style="zoom:50%;" />

文件发送

<img src="resource/file.png" alt="im" style="zoom:100%;" />

语音发送

<img src="resource/voice.png" alt="im" style="zoom:100%;" />

emoji发送

<img src="resource/emoji.png" alt="im" style="zoom:100%;" />

#### 样例原理

<img src="resource/communication.png" alt="im" style="zoom:50%;" />

想要实现多个设备之间的无障碍即时通讯，需要多台终端设备、终端应用和服务器配合一起使用。首先应该将终端应用安装到终端设备上，用户通过应用向服务器申请注册账号。随后，用户可以通过账号进行查找，添加其他好友，并向好友发送文字、图片、文件、语音、emoji等讯息。用户发送的讯息会先送达服务器，由服务器判断其好友的状态（离线/在线），然后选择发送或者暂时缓存消息等操作。最后，好友的终端应用接收到消息。

**实现即时通讯的设备需求**：安装应用的终端设备、网络环境和云端服务器。

**前提条件**：用户将应用安装在终端设备上，并且拥有注册账号，且需要通讯的用户也成功注册了账号并且添加了好友。

**通讯原理**：用户在安装了应用的终端设备上编辑信息（文字、图片、文件、语音、emoji等），通过网络将消息发送至云端服务器。当对方用户在线时，云端服务器将把消息推送给对方用户，对方用户安装了应用的终端设备也将接收到信息。当对方用户不在线时，信息将被暂时缓存在云端服务器。

#### 工程版本

- 系统版本/API版本：OpenHarmony 3.1 release、API8；
- IDE版本：DevEco Studio 3.1 Canary1, 版本号：3.1.0.101；
- 支持开发板：润和大禹系列HH-SCDAYU200开发板套件；

## 快速上手

#### 准备硬件环境

润和大禹系列HH-SCDAYU200开发套件：

- [润和RK3568开发板标准设备上手-HelloWorld](https://gitee.com/openharmony-sig/knowledge_demo_temp/tree/master/docs/rk3568_helloworld)

#### 准备开发环境

- 下载DevEco Studio 3.1 Canary1， 版本号是3.1.0.101，版本 [下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio/archive/)；
- DevEco Studio 点击File -> Open 导入下载代码工程的IM；

#### 准备工程

 项目地址：https://gitee.com/openharmony-sig/knowledge_demo_entainment/tree/master/FA/IM

1）git下载

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_entainment.git --depth=1
```

2）项目导入

打开DevEco Studio,点击File->Open->下载路径/FA/IM

#### 安装应用

- [配置应用签名信息](https://docs.openharmony.cn/pages/v3.2Beta/zh-cn/application-dev/security/hapsigntool-overview.md/)

- 安装应用

  打开**OpenHarmony SDK路径 \toolchains** 文件夹下，执行如下hdc_std命令，其中**path**为hap包所在绝对路径。

  ```text
  hdc_std install -r path\entry-default-signed.hap
  ```

## 参考链接

- [ OpenHarmony兼容JS的类Web开发范式](https://gitee.com/openharmony/docs/tree/master/zh-cn/application-dev/reference/arkui-js)
- [OpenHarmony应用接口](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/reference/apis/Readme-CN.md)
- [RK3568开发板上丝滑体验OpenHarmony标准系统](https://gitee.com/Cruise2019/team_x/tree/master/homework/rk3568_quick_start)

