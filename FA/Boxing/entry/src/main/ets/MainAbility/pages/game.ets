/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import CommonLog from '../common/CommonLog'
import lottie from '@ohos/lottieETS'
import router from '@ohos.router';
import media from '@ohos.multimedia.media';
import fileIO from '@ohos.fileio'
// @ts-ignore
import BoxingGameNAPI from '@ohos.boxinggame_napi'
const TAG = 'Game'

@Entry
@Component
struct Game {
    @State leftY1: string = '-50%'
    @State leftY2: string = '-50%'
    @State leftY3: string = '-50%'
    @State leftY4: string = '-50%'
    @State rightY1: string = '-50%'
    @State rightY2: string = '-50%'
    @State rightY3: string = '-50%'
    @State rightY4: string = '-50%'
    @State text: number = 0
    private boxingCount = 4
    private targetOffsetY = '25%'
    private animateLeftTimestamp:number = 0
    private animateRightTimestamp:number = 0
    private leftDuration:number = 4000
    private rightDuration:number = 6000
    private intervalNumber:number = 0

    // Lottie
    private controllerLeft: CanvasRenderingContext2D = new CanvasRenderingContext2D()
    private controllerRight: CanvasRenderingContext2D = new CanvasRenderingContext2D()
    private animateName: string = "animate"
    private animateJustPath: string = "common/lottie/just.json"
    private animatePerfectPath: string = "common/lottie/perfect.json"
    private leftAnimateLock:boolean = false
    private rightAnimateLock:boolean = false
    private lottieDuration:number = 1000

    build() {
        Stack() {
            // 背景图
            Image($r('app.media.ic_bg'))
            Column() {
                this.BackgroundArea()
            }.offset({ x: '-30%' })
            Column() {
                this.BackgroundArea()
            }.offset({ x: '30%' })

            // 动画部分
            // 左侧
            this.LeftBoxing(this.leftY1)
            this.LeftBoxing(this.leftY2)
            this.LeftBoxing(this.leftY3)
            this.LeftBoxing(this.leftY4)
            // 右侧
            this.RightBoxing(this.rightY1)
            this.RightBoxing(this.rightY2)
            this.RightBoxing(this.rightY3)
            this.RightBoxing(this.rightY4)

            // 挥拳区域
            Column() {
                this.TargetArea(this.controllerLeft,'animate_left')
            }.offset({ x: '-30%' })

            Column() {
                this.TargetArea(this.controllerRight,'animate_right')
            }.offset({ x: '30%' })
            Text(this.text + '').fontSize(64).fontColor('#92FFEC')
        }
        .height('100%').width('100%').borderColor(Color.Red).backgroundColor(Color.Black)
    }

    @Builder BackgroundArea(){
        Stack(){
            Column() {}.width(90).height('100%').backgroundColor('#4C4E63').opacity(1)

            Column() {
            }.width(70).height('100%').backgroundColor('#636980').opacity(1)

            Column() {
            }.width(2).height('100%').backgroundColor(Color.White)
            Image($r('app.media.img_boxing_area')).width(192).height(212).offset({ y: this.targetOffsetY })
        }.height('100%').width(220)
    }

    @Builder TargetArea(controller:CanvasRenderingContext2D,lottieName:string) {
        Stack() {
            Canvas(controller)
                .aspectRatio(1)
                .width(300)
                .offset({ y:  this.targetOffsetY })
                .onAppear(() => {
//                    lottie.loadAnimation({
//                        container: controller,
//                        renderer: 'canvas',
//                        loop: true,
//                        autoplay: false,
//                        name: lottieName,
//                        path: this.animateJustPath,
//                    })
//                    this.setLottie(controller,lottieName,this.animateJustPath)
                })
            Animator('__lottie_ets') // declare Animator('__lottie_ets') when use lottie
        }.height('100%').width(220)
    }

    @Builder LeftBoxing(offsetY: string) {
        Image($r('app.media.icon_boxing_left'))
            .width(144)
            .height(110)
            .offset({ x: "-30%", y: offsetY })
            .touchable(true)
            .onClick((e) => {
                console.log("###" + JSON.stringify(e))
            })
    }
    @Builder RightBoxing(offsetY: string) {
        Image($r('app.media.icon_boxing_right'))
            .width(144)
            .height(110)
            .offset({ x: "30%", y: offsetY })
            .touchable(true)
            .onClick((e) => {

                console.log("###" + JSON.stringify(e))
            })
    }

    onPageShow() {
        setTimeout(()=>{
            this.setScoreListen()
            this.startAnimate()
            this.playMusic()
        },0)
    }

    onPageHide(){
        // 销毁定时器
        if (this.intervalNumber) {
            clearInterval(this.intervalNumber)
        }
    }

    async startAnimate() {
        CommonLog.info(TAG,'startAnimate')
        this.rightAnimate()
        this.leftAnimate()
    }

    setLottie(controller:CanvasRenderingContext2D,lottieName:string,animatePath:string){
        lottie.loadAnimation({
            container: controller,
            renderer: 'canvas',
            loop: false,
            autoplay: false,
            name: lottieName,
            path: animatePath,
        })
        lottie.setSpeed(1,lottieName)
    }

    async leftAnimate(){
        CommonLog.info(TAG,'start leftAnimate end')
        // 设置图标下滑动画
        // 动画持续时间
        let rightDuration = this.getRandomDuration()
        this.leftDuration = rightDuration
        // 延迟时长
        let leftDelay = rightDuration / (this.boxingCount - 1)
        // 设置开始时间
        let now = new Date
        this.animateLeftTimestamp = now.getTime()
        // 左侧动画
        animateTo({ duration: rightDuration, curve: Curve.Linear,delay:0 * leftDelay ,iterations: 1 }, () => {
            this.leftY1 = "50%"
        })
        animateTo({ duration: rightDuration, curve: Curve.Linear,delay:1 * leftDelay, iterations: 1 }, () => {
            this.leftY2 = "50%"
        })
        animateTo({ duration: rightDuration, curve: Curve.Linear,delay:2 * leftDelay, iterations: 1 }, () => {
            this.leftY3 = "50%"
        })
        animateTo({ duration: rightDuration, curve: Curve.Linear,delay:3 * leftDelay, iterations: 1 }, () => {
            this.leftY4 = "50%"
        })
        let totalTime = rightDuration + 3 * leftDelay
        await this.sleep(totalTime)
        this.resetAnimate(true)
        CommonLog.info(TAG,'leftAnimate end')
        this.leftAnimate()
    }

    async rightAnimate(){
        CommonLog.info(TAG,'start rightAnimate end')
        // 设置图标下滑动画
        // 动画持续时间
        let rightDuration = this.getRandomDuration()
        this.rightDuration = rightDuration
        // 延迟时长
        let rightDelay = rightDuration /(this.boxingCount - 1)
        // 设置开始时间
        let now = new Date
        this.animateRightTimestamp = now.getTime()
        // 左侧动画
        animateTo({ duration: rightDuration, curve: Curve.Linear,delay:0 * rightDelay,iterations: 1 }, () => {
            this.rightY1 = "50%"
        })
        animateTo({ duration: rightDuration, curve: Curve.Linear,delay:1 * rightDelay, iterations: 1 }, () => {
            this.rightY2 = "50%"
        })
        animateTo({ duration: rightDuration, curve: Curve.Linear,delay:2 * rightDelay, iterations: 1 }, () => {
            this.rightY3 = "50%"
        })
        animateTo({ duration: rightDuration, curve: Curve.Linear,delay:3 * rightDelay, iterations: 1 }, () => {
            this.rightY4 = "50%"
        })
        let totalTime = rightDuration + 3 * rightDelay
        await this.sleep(totalTime)
        this.resetAnimate(false)
        CommonLog.info(TAG,'rightAnimate end')
        this.rightAnimate()
    }

    playMusic(){
// @ts-ignore
        let musicPath = router.getParams().musicPath
        fileIO.open(musicPath)
            .then(fd=>{
                CommonLog.info(TAG,'musicPath fd:' +fd);
                let audioPlayer = media.createAudioPlayer();
                audioPlayer.src = 'fd://' + fd
                audioPlayer.on('dataLoad', () => { //设置'dataLoad'事件回调，src属性设置成功后，触发此回调
                    CommonLog.info(TAG,' audio set source success');
                    audioPlayer.play(); //调用play方法开始播放，触发'play'事件回调
                });

                audioPlayer.on('play', () => { //设置'play'事件回调
                    console.info('ttt audio play success');
                });

                audioPlayer.on('finish', () => { //设置'finish'事件回调，播放完成触发
                    let duration = audioPlayer.duration
                    console.info('tttt audio play finish');
                    audioPlayer.release(); //audioPlayer资源被销毁
                    audioPlayer = undefined;
                    router.push({
                        url:'pages/finish',
                        params:{
                            boxingCount:this.text,
                            gameTime:duration / 1000
                        }
                    })
                });
            }).catch(err=>{
                CommonLog.error(TAG,' err ' +err);
        })
    }

    setScoreListen(){
        this.intervalNumber = setInterval(async()=>{
            let res = await BoxingGameNAPI.recvMsg();
            CommonLog.info(TAG,JSON.stringify(res))
            if(res?.message.length > 0){
                if(res.message.includes('left') && !this.leftAnimateLock){
                    this.judgeLeft()
                }else if(res.message.includes('right') && !this.rightAnimateLock){
                    this.judgeRight()
                }
            }
        },1000)
    }

    judgeLeft(){
        let nowTime = new Date().getTime()
        // 首次抵达目标顶部时间
        let firstTime = this.animateLeftTimestamp + (this.percentToPoint(this.targetOffsetY)+this.percentToPoint('50%') - this.percentToPoint('10%')) * this.leftDuration
        // 结束时间
        let endTime = this.animateLeftTimestamp + this.leftDuration * 2
        CommonLog.info(TAG,'judgeLeft')
        CommonLog.info(TAG,`nowTime:${nowTime},firstTime:${firstTime}`)
        if(nowTime > firstTime - 200 && nowTime < endTime){
            // 得分时间界限
            let leftDelay =  this.leftDuration /(this.boxingCount -1 )
            let handleTime = (nowTime - firstTime) % leftDelay
            let judgeTime = this.leftDuration /6
            CommonLog.info(TAG,`leftDelay:${leftDelay},handleTime:${handleTime},judgeTime:${judgeTime}`)
            // 完美击中
            if (judgeTime/4 < handleTime && handleTime  < (judgeTime *(3/4))) {
                lottie.destroy('animate_left')
                this.setLottie(this.controllerLeft,'animate_left',this.animatePerfectPath)
                this.text += 2
                lottie.play('animate_left')
                // 等动画执行完成后才能进入下一次挥拳判定
                this.leftAnimateLock = true
                setTimeout(()=>{
                    lottie.stop()
                    lottie.destroy('animate_left')
                    this.leftAnimateLock = false
                },this.lottieDuration)
                CommonLog.info(TAG,`left perfect`)
            }else if(handleTime < judgeTime){
                // 击中
                lottie.destroy('animate_left')
                this.setLottie(this.controllerLeft,'animate_left',this.animateJustPath)
                this.text += 1
                lottie.play('animate_left')
                this.leftAnimateLock = true
                setTimeout(()=>{
                    lottie.stop()
                    lottie.destroy('animate_left')
                    this.leftAnimateLock = false
                },this.lottieDuration)
                CommonLog.info(TAG,`left just`)
            }else{
                CommonLog.info(TAG,`Did not score`)
            }
        }else{
            CommonLog.info(TAG,`Did not arrive`)
        }
        CommonLog.info(TAG,`-------`)
    }

    judgeRight(){
        let nowTime = new Date().getTime()
        // 首次抵达目标顶部时间
        let firstTime = this.animateRightTimestamp + (this.percentToPoint(this.targetOffsetY)+this.percentToPoint('50%') - this.percentToPoint('10%')) * this.rightDuration
        // 结束时间
        let endTime = this.animateLeftTimestamp + this.leftDuration * 2
        CommonLog.info(TAG,'judgeRight')
        CommonLog.info(TAG,`nowTime:${nowTime},firstTime:${firstTime}`)
        if(nowTime > firstTime - 200 && nowTime < endTime){
            // 得分时间界限
            let rightDelay =  this.rightDuration /(this.boxingCount - 1)
            let handleTime = (nowTime - firstTime) % rightDelay
            let judgeTime = this.rightDuration /6
            CommonLog.info(TAG,`rightDelay:${rightDelay},handleTime:${handleTime},judgeTime:${judgeTime}`)
            // 完美击中
            if (judgeTime/4 < handleTime && handleTime  < (judgeTime *(3/4))) {
                lottie.destroy('animate_right')
                this.setLottie(this.controllerRight,'animate_right',this.animatePerfectPath)
                this.text += 2
                lottie.play('animate_right')
                // 动画执行前无法二次触发
                this.rightAnimateLock = true
                setTimeout(()=>{
                    lottie.stop()
                    lottie.destroy('animate_right')
                    this.rightAnimateLock = false
                },this.lottieDuration)
                CommonLog.info(TAG,`right perfect`)
            }else if(handleTime < judgeTime){
                lottie.destroy('animate_right')
                this.setLottie(this.controllerRight,'animate_right',this.animateJustPath)
                this.text += 1
                lottie.play('animate_right')
                // 动画执行前无法二次触发
                this.rightAnimateLock = true
                setTimeout(()=>{
                    lottie.stop()
                    lottie.destroy('animate_right')
                    this.rightAnimateLock = false
                },this.lottieDuration)
                CommonLog.info(TAG,`right just`)
            }else{
                // 未到达
                CommonLog.info(TAG,`Did not arrive`)
            }
        }else{
            CommonLog.info(TAG,`Did not score`)
        }
        CommonLog.info(TAG,`-------`)
    }

    resetAnimate(isLeft:boolean){
        if (isLeft) {
            this.leftY1 = "-50%"
            this.leftY2 = "-50%"
            this.leftY3 = "-50%"
            this.leftY4 = "-50%"
        }else{
            this.rightY1 = "-50%"
            this.rightY2 = "-50%"
            this.rightY3 = "-50%"
            this.rightY4 = "-50%"
        }
    }

    percentToPoint(percent){
        let str = percent.replace("%","")
        return str/100
    }

    sleep(timeout) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve('');
            }, timeout)
        })
    }

    getRandomDuration(){
        let durationArr = [4000,5000,6000]
        let random = this.getRandomInt(0,durationArr.length)
        // 动画持续时间
        return durationArr[random]
    }

    getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }
}

