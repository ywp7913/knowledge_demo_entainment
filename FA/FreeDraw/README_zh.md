# 涂鸦样例

## 简介

涂鸦是装在标准设备上的应用，适用于OpenHarmony3.1_release版本，使用ArkUI提供的Canvas组件实现自由的涂鸦功能，目前适配在深开鸿的KHDVK-3566B设备上运行。

## 样例效果
![](resources/1.jpg)

![](resources/2.jpg)

![](resources/3.jpg)

&ensp;&ensp;&ensp;&ensp;本样例首页会显示涂鸦的一些图片，最后一张是可以让用户随意涂写的，在选择具体的图片以后，点击该图片进入涂鸦页面，涂鸦页面可以对画笔的颜色，粗细进行设置，代码中也可以在目前支持的大小和颜色的基础上进行修改增加，如果涂鸦过程中有错误，可以通过橡皮擦进行擦除，也可以点击清除按钮，清空涂鸦的内容，重新进行涂鸦操作。

##代码目录
![](resources/4.png)


## 安装部署

#### 1.代码编译运行步骤

1）下载此项目，[链接](https://gitee.com/openharmony-sig/knowledge_demo_entainment/tree/master/FA/FreeDraw)。

2）开发环境搭建，开发工具：DevEco Studio 3.0 Beta3 。

3）导入OpenHarmony工程：DevEco Studio 点击File -> Open 导入本样例的代码工程FreeDraw。

4）OpenHarmony应用运行在真机设备上，需要进行签名，[签名方法](https://docs.openharmony.cn/pages/v3.1/zh-cn/application-dev/security/hapsigntool-guidelines.md/)。
