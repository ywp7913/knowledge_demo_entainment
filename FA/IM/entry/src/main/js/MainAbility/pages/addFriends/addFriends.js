/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@system.router';
import http from '@ohos.net.http';
import prompt from '@system.prompt';
const sessionList = [
    {
        id: 1,
        imgUrl: '/common/images/addFriendPic/leidajiahaoyou.png',
        text: '雷达加朋友',
        intro: '添加身边的朋友',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 2,
        imgUrl: '/common/images/addFriendPic/jianqun.png',
        text: '面对面建群',
        intro: '与身边的朋友进入同一个群聊',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 3,
        imgUrl: '/common/images/addFriendPic/saoma.png',
        text: '扫一扫',
        intro: '扫描二维码名片',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 4,
        imgUrl: '/common/images/addFriendPic/lianxiren.png',
        text: '手机联系人',
        intro: '添加或邀请通讯录的朋友',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 5,
        imgUrl: '/common/images/addFriendPic/gongzhonghao.png',
        text: '公众号',
        intro: '获取更多咨询与服务',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    }
        ]
export default {
    data:{
        sessionList: sessionList,
        searchid:'',
        uid:'',
        returnValue:'',
        httpRequest:{},
        result:{
            code:"",
            msg:"",
            data:""
        },
    },
    onCreate(){
        this.uid = this.$app.$def.uid;
    },
    onInit() {
        this.httpRequest = http.createHttp();
        console.info('addFriend====>http.createHttp ');
    },
    onDestroy() {
        this.httpRequest.destroy();
    },
    backToSession(){
       // router.back();
        router.push({
            uri:'pages/index/index'
        })
    },
    change(e){
        this.searchid = e.value;
    },
    search(){
            prompt.showToast({
                message: '点击按钮成功！',
                duration: 2000,
            });
        console.info('addFriend====>searchid: ' + this.searchid);
        console.info('addFriend====>uid: ' + this.$app.$def.uid);
       // let httpRequest= http.createHttp();
       // console.info('addFriend====>http.createHttp ');
       /* let promise = httpRequest.request("http://192.168.1.110:8090/user/addFriend", {
            method: 'POST',
//            参数已协调
            extraData:JSON.stringify({"uid":this.$app.$def.uid,"touid":this.searchid}),
            connectTimeout: 60000,
            readTimeout: 60000,
            header: {
                'Content-Type': 'application/json'
            }
        });
        console.info('addFriend===>:httpRequest');
        promise.then((value) => {
            prompt.showToast({
                message: '发送成功！',
                duration: 2000,
            });
            console.info('Result111:' + value.result);
            //            this.result = value.result;
            this.result = JSON.parse(value.result.toString())
            this.returnValue = this.result.code;
            console.info('this.result.code:' + this.result.code);
            if(this.returnValue == '2019'){
                prompt.showToast({
                    message: '该用户不存在！',
                    duration: 2000,
                });
            }else if(this.returnValue == '2020'){
                prompt.showToast({
                    message: '该好友已存在！',
                    duration: 2000,
                });
            }else if(this.returnValue == '200'){
                prompt.showToast({
                    message: '好友申请发送成功！',
                    duration: 2000,
                });
                //          参数已协调
                console.info("this.result.code:TCP begin");
                var obj = {uid:this.$app.$def.uid,touid:[this.searchid],role:'ho_user',type:'addFriend',content:' ',stype:0}
                this.$app.$def.mySocket.send(JSON.stringify(obj))
            }
        }).catch((err) => {
            console.error(`addFriend====>errCode:${err.code}, errMessage1:${err.data}`);
        });*/
            this.httpRequest.request(
                "http://" + this.$app.$def.ip +"/user/addFriend",
                {
                    method: 'POST',
                    extraData:JSON.stringify({"uid":this.$app.$def.uid,"touid":this.searchid}),
                    connectTimeout: 30000,
                    readTimeout: 60000,
                    header: {
                        'Content-Type': 'application/json'
                    }
                },(err, data) => {
                console.info('addFriend====>(err, data)');
                if (!err) {
                    console.info('(!err)');
                    this.result = JSON.parse(data.result.toString())
                    this.returnValue = this.result.code;
                    console.info('addFriend====>returnValue: ' + this.result.code);
                    if(this.returnValue == '2019'){
                        prompt.showToast({
                            message: '该用户不存在！',
                            duration: 2000,
                        });
                    }else if(this.returnValue == '2020'){
                        prompt.showToast({
                            message: '该好友已存在！',
                            duration: 2000,
                        });
                    }else if(this.returnValue == '200'){
                        prompt.showToast({
                            message: '好友申请发送成功！',
                            duration: 2000,
                        });
                    }

                    //httpRequest.destroy();
                    console.info("addFriend====> TCP begin");
                    var obj = {uid:this.$app.$def.uid,touid:[this.searchid],role:'ho_user',type:'addFriend',content:' ',stype:0}
                    this.$app.$def.mySocket.send(JSON.stringify(obj));
                } else {
                    console.info("addFriend====> httpRequest.destroy()");
                    this.httpRequest.destroy();
                }
            });
    }
}