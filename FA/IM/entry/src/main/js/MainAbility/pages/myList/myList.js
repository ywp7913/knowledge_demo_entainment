/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@system.router';
import featureAbility from '@ohos.ability.featureAbility';
import display from '@ohos.display';
import http from '@ohos.net.http';

const serviceList = [
    {
        id: 1,
        imgUrl: '/common/images/personPic/service.png',
        text: '服务',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 2,
        imgUrl: '/common/images/personPic/collection.png',
        text: '收藏',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 3,
        imgUrl: '/common/images/personPic/emoji.png',
        text: '表情',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 4,
        imgUrl: '/common/images/personPic/setup.png',
        text: '设置',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
]
var personData = {
    'name': '我叫学习',
    'personNum': '萌聊号:',
    'imgUrl': '',
    'find_right_qr': '/common/images/personPic/qrcode.png',
    'find_right_icon': '/common/images/findPic/find-right-icon.png',
}

export default {
    props:['imgUrl'],
    data: {
        person: personData,
        serviceList: serviceList,
        uid: '',
        first: true,
        second: true,
        three: true
    },
    onInit() {
        console.log('myList====> display.width:' + display.width + "display.height：" + display.height);
        this.grantPermission();
        console.info("myList=====onInit")
        this.getProfileImg(this.$app.$def.uid); //预览时注释掉这一行
    },
//    onCreate() {
//        this.uid = this.$app.$def.uid
//    },
    onShow() {
        console.info("myList=====>onShow()");
        // 修改昵称后的刷新
        this.uid = this.$app.$def.uid
        this.person.name = this.$app.$def.nickname
        console.info("myList=====>onShow()=====>this.person.name: "+this.person.name);
    },
    /*获取部分权限*/
    grantPermission() {
        console.info('myList=====>grantPermission')
        let context = featureAbility.getContext()
        context.requestPermissionsFromUser(['ohos.permission.DISTRIBUTED_DATASYNC'], 666, function (result) {
            console.info(`myList=====>grantPermission,requestPermissionsFromUser`)
        })
    },
    closeSecond(e) {
        this.second = false;
    },
    closeThree(e) {
        this.three = false;
    },
    // 点击事件
    itemClick(chat) {
        router.push({
            uri: "pages/dialogPage/dialogPage",
            params: {
                'title': this.person.name,
                'type': this.person.personNum,
                'imgUrl': this.person.imgUrl,
            }
        });
    },
    chat() {
        router.push({
            uri: "pages/sessionList/sessionList",
        });
    },
    message() {
        router.push({
            uri: "pages/mailList/mailList",
        });
    },
    find() {
        router.push({
            uri: "pages/findList/findList",
        });
    },
    setting(){
        router.push({
            uri: "pages/myList/setting/setting",
        });
    },
    switch() {
        router.push({
            uri: "pages/myList/personPage/personPage",
            params: {
                'title': this.person.name,
                'type': this.person.personNum,
                'imgUrl': this.person.imgUrl,
            }
        });
    },
    getProfileImg(uid) {
        let httpRequest = http.createHttp();
        console.info("myList=====create http request to get profileImg Url");
        httpRequest.request(
            "http://" + this.$app.$def.ip + "/file/getProfileImg?uid=" + uid,
            {
                connectTimeout: 30000,
                readTimeout: 60000,
                header: {
                    'Content-Type': 'application/json'
                }
            }, (err, data) => {
            if (!err) {
                console.info("myList=====Profile Img Result : " + data.result.toString());
                let value = JSON.parse(data.result.toString());
                this.person.imgUrl = "http://" + value.data;
            } else {
                console.info("myList====> httpRequest err" + JSON.stringify(err));
                httpRequest.destroy();
            }
        }
        );
    },
    toQrCode(){
        router.push({
            uri: "pages/myList/qrcode/qr",
            params: {
                sayOrGroup: 'say',
                sayOrGroup_ID: this.$app.$def.uid,
            },
        });
    }
}



