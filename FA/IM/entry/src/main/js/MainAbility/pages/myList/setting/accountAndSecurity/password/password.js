/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@system.router';
import http from '@ohos.net.http';
import prompt from '@system.prompt';

export default {
    data: {
        uid:'',  // 当前用户UID
        oldPassword: "",
        newPassword: "",
        newPasswordConfirm: "",
        re:{
            code:"",
            msg:"",
            data:""
        },
    },
    onInit() {
        // Previewer不能输入，此处设定默认值以便测试
        this.uid =  this.$app.$def.uid;
    },
    onShow() {
    },
    backToSession(){
        router.back();
    },
    oldPasswordChange(e){
        this.oldPassword = e.value;
    },
    newPasswordChange(e){
        this.newPassword = e.value;
    },
    newPasswordConfirmChange(e){
        this.newPasswordConfirm = e.value;
    },
    changePassword(){
        console.info('changePassword====>oldPassword.result: ' + this.oldPassword);
        if(this.oldPassword == "" || this.newPassword == "" || this.newPasswordConfirm == ""){
            prompt.showDialog(({message:"请输入完整"}));
        } else if(this.newPassword != this.newPasswordConfirm){
            prompt.showDialog(({message:"两次密码输入不相同！"}));
        } else{
            // 曹天恒：发送HTTP请求部分，以下功能未经测试，Previewer不支持
            let httpRequest= http.createHttp();
            let promise = httpRequest.request("http://" + this.$app.$def.ip +"/user/changePassword", {
                method: 'POST',
                extraData:JSON.stringify({"uid":this.uid,"oldPassword":this.oldPassword,"newPassword":this.newPassword}),
                connectTimeout: 60000,
                readTimeout: 60000,
                header: {
                    'Content-Type': 'application/json'
                }
            });
            router.back();
            promise.then((value) => {
                prompt.showToast({
                    message: '发送成功！',
                    duration: 2000,
                });
                console.info('login====>value.result: ' + value.result);
                let re = JSON.parse(value.result.toString())
                if(re.code == 2023){
                    prompt.showDialog({message: '原密码输入错误！'});
                } else if(re.code == 2024){
                    prompt.showDialog({message: '重设密码成功！'});
                }
            }).catch((err) => {
                console.error(`login====> errCode:${err.code}, errMessage1:${err.data}`);
            });
        }

    },

}