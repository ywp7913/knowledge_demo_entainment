/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@system.router';
// 导入app模块
import app from '@system.app'

const Gtalk = [
    {
        id: 1,
        imgUrl: 'common/images/personPic/1.png',
        text: '账号与安全',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 2,
        imgUrl: '/common/images/personPic/2.png',
        text: '新消息通知',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 3,
        imgUrl: '/common/images/personPic/3.png',
        text: '通用',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 4,
        imgUrl: '/common/images/personPic/4.png',
        text: '个人信息收集清单',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 5,
        imgUrl: '/common/images/personPic/5.png',
        text: '第三方信息共享清单',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 6,
        imgUrl: '/common/images/personPic/5.png',
        text: '朋友权限',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 7,
        imgUrl: '/common/images/personPic/5.png',
        text: '通讯录黑名单',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 8,
        imgUrl: '/common/images/personPic/5.png',
        text: '关于鸿萌聊',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 9,
        imgUrl: '/common/images/personPic/5.png',
        text: '帮助与反馈',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
];
const avatar = [
    {
        imgUrl:'/common/images/sessionPic/img_1.png',
        title:'小蔡'
    },
    {
        imgUrl:'/common/images/addChatFriend.png',
        title:''
    }
];
export default {
    data: {
        GtalkList: Gtalk,
        avatarList: avatar,
        checkedId: [],
        checkedUid: [],
        groupName:'',
        groupId:'',
        sendList:[
            {
                'id':'1',
                'title':'退出登录',
            },
            {
                'id':'2',
                'title':'关闭应用',
            }
        ],
        re:{
            code:"",
            msg:"",
            data:""
        },
        testId:''
    },
    onInit() {
        this.avatarList[0].imgUrl = this.$app.$def.chatData[this.$app.$def.idx].imgUrl;
        this.avatarList[0].title = this.$app.$def.chatData[this.$app.$def.idx].title;
//        this.friendsList = this.$app.$def.friendsList
    },
    onShow() {
        this.getFriends()
        this.groupId = this.getRandomSixDigit()
    },
    backToSession(){
        router.back();
    },
    switchUid(e){
        //发送登出消息
        var obj = { uid: this.$app.$def.uid, type:'logout'};
        var json = JSON.stringify(obj);
        this.$app.$def.mySocket.send(json);
        // 跳转到登录页面
        if(e == 1){
            router.push({
                uri: "pages/login/login",
                params:{
                    'uid':this.$app.$def.uid,
                }
            });
        }
        else{
            // 退出app
            console.info("setting=====>close");
            app.terminate();
        }
    },
    change(e){
        this.groupName = e.value;
    },
    getRandomSixDigit(){
        let code = ''
        for(var i=0;i<6;i++){
            code += parseInt(Math.random()*10)
        }
        return code
    },
    accountAndSecurity(text){
        if(text==='通讯录黑名单'){
            router.push({
                uri: "pages/blackList/blackList",
            });
        }
        router.push({
            uri: "pages/myList/setting/accountAndSecurity/accountAndSecurity",
        });
    },
}