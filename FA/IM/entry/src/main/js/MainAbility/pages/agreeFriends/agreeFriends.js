/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@system.router';

export default{
    data:{
        uid:'121232',
        insert:'',
        remark:'',
        message:{
            uid:'',
            touid:'',
            type:''
        }
    },
    onShow(){
        this.socketReceive();
    },
    /*
    * socket实时监听并接收好友请求
    * return参数为touid
    */
    socketReceive() {
        this.uid = this.$app.$def.agreeuid;
        console.info('agreeFriends', this.uid)
    },
    change(e){
        this.remark = e.value;
    },
    clickInsert(){
        this.insert = this.uid;
    },
    buttonClick(){
        let img = '/common/images/sessionPic/img_' + Math.round(Math.random()*7+1) + '.png'
        let newSession = {
            'title': this.remark.length > 0 ? this.remark : this.uid,
            'topMessage': '你们已经成为好友了',
            'imgUrl': img,
            'readTime': new Date().getHours() + ':' + new Date().getMinutes(),
            'unRead': 'say',
            'uid':[this.uid],
            'checkId':[],
            'MsgList': [{isMe: false,
                            sendUid:"",
                            messageType: 0,
                            content:'华为鸿蒙系统是一款全新的面向全场景的分布式操作系统，创造一个超级虚拟终端互联的世界',
                            profPicUrl:'/common/images/bob.jpg',
                            createTime:"",},
                        {isMe: false,
                            sendUid:"",
                            messageType: 1,
                            content:'/common/images/harmonyos.jpg',
                            profPicUrl:'/common/images/bob.jpg',
                            createTime:"",}]
        }
        let newFriend = {
            id: this.$app.$def.friendsList.length + 1,
            imgUrl: img,
            text: this.remark.length > 0 ? this.remark : this.uid,
            url: '',
            style: 'pengyou',
            find_right_icon: '/common/images/findPic/find-right-icon.png'
        }
        this.$app.$def.chatData = [newSession,...this.$app.$def.chatData]
        this.$app.$def.friendsList = [newFriend,...this.$app.$def.friendsList]
        this.socketSend();
        this.$app.$def.agreeuid = ''
        router.push({
            uri:'pages/index/index'
        })
    },
    socketSend() {
        var obj = {uid:this.$app.$def.uid,touid:[this.uid],role:'ho_user',type:'addFriendPass',content:'',stype:0};
        var json = JSON.stringify(obj);
        this.$app.$def.mySocket.send(json);
    },
    backToSession(){
        router.back();
    }
}