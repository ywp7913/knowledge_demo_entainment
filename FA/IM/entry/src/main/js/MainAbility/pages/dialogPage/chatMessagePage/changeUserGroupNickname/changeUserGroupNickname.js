/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@system.router';
import http from '@ohos.net.http';
import prompt from '@system.prompt';

export default {
    data: {
        uid:'',  // 当前用户UID
        nickname: "",
        re:{
            code:"",
            msg:"",
            data:""
        },
        groupId: '',
    },
    onInit() {
        this.uid = this.$app.$def.uid;
        this.groupId = router.getParams().groupId;
    },
    onShow() {
    },
    backToSession(){
        router.back();
    },
    inputChange(e) {
        this.nickname = e.value;
    },
    changeNickname(){
        console.info("changeUserGroupNickname=====this nickname : " + this.nickname);
        let httpRequest= http.createHttp();
        let promise = httpRequest.request("http://" + this.$app.$def.ip +"/group/modifyUserGroupNickname", {
            method: 'POST',
            extraData:JSON.stringify({"uid":this.uid, "userGroupNickname":this.nickname, "groupId":this.groupId}),
            connectTimeout: 60000,
            readTimeout: 60000,
            header: {
                'Content-Type': 'application/json'
            }
        });
        promise.then((value) => {
            let re = JSON.parse(value.result.toString())
            if(re.code == 200){
                prompt.showToast({message: '修改用户群昵称成功'});
                console.info("changeUserGroupNickname=====re.data : " + re.data);
                this.nickname = re.data;
            } else {
                prompt.showToast({message: '修改用户群昵称失败'});
            }
        }).catch((err) => {
            console.error(`changeUserGroupNickname====> errCode:${err.code}, errMessage1:${err.data}`);
        });
        router.back();
    },
}