/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@system.router';
import {ChangeList} from './ListModel.js'
//import {OriginalList} from './ListModel.js'
import {ChangeStrList} from './ListModel.js'

export default {
    data: {
        title: "",
        key: "",
        isShowIcon:true, //是否展示icon
        isShowLoading:false, //是否展示loading加载框
        loadingStyle:'loading-hide',//加载框的样式
        originalList: [{
                           id: 0,
                           isShow: true,
                           imgUrl: 'common/images/icon-user.png',
                           content: '闪闪红心'
                       }, {
                           id: 1,
                           isShow: true,
                           imgUrl: 'common/images/icon-user.png',
                           content: '随风而动'
                       }, {
                           id: 2,
                           isShow: true,
                           imgUrl: 'common/images/icon-user.png',
                           content: '当 当 让我们荡起双桨'
                       }, {
                           id: 3,
                           isShow: true,
                           imgUrl: 'common/images/icon-user.png',
                           content: '东方一条龙'
                       }, {
                           id: 4,
                           isShow: true,
                           imgUrl: 'common/images/icon-user.png',
                           content: '我的中国心'
                       }, {
                           id: 5,
                           isShow: true,
                           imgUrl: 'common/images/icon-user.png',
                           content: '什么诗和远方啥东东'
                       }, {
                           id: 6,
                           isShow: true,
                           imgUrl: 'common/images/icon-menu-release.png',
                           content: 'this love'
                       }, {
                           id: 7,
                           isShow: true,
                           imgUrl: 'common/images/icon-menu-release.png',
                           content: 'black friday'
                       }, {
                           id: 8,
                           isShow: true,
                           imgUrl: 'common/images/icon-menu-release.png',
                           content: 'say you say me'
                       }, {
                           id: 9,
                           isShow: true,
                           imgUrl: 'common/images/icon-menu-release.png',
                           content: 'as long as you love me'
                       }, {
                           id: 10,
                           isShow: true,
                           imgUrl: 'common/images/icon-menu-release.png',
                           content: 'The Sound Of Silence'
                       }, {
                           id: 11,
                           isShow: true,
                           imgUrl: 'common/images/icon-menu-release.png',
                           content: 'Song Sung Blue'
                       }, {
                           id: 12,
                           isShow: true,
                           imgUrl: 'common/images/icon-menu-release.png',
                           content: 'Dream It Possible'
                       }, {
                           id: 13,
                           isShow: true,
                           imgUrl: 'common/images/icon-buy.png',
                           content: '12323232123'
                       }, {
                           id: 14,
                           isShow: true,
                           imgUrl: 'common/images/icon-buy.png',
                           content: '444432232'
                       }, {
                           id: 15,
                           isShow: true,
                           imgUrl: 'common/images/icon-buy.png',
                           content: '222343434'
                       }],
        changeList: new Array(),
        initDataList: new Array(), //保留一份初始化后的数据
    },
    onInit() {
        this.initFriendsListData()
//        this.key = router.getParams().searchContent;
        this.onDataHandle()
    },
    onShow() {

    },
    backToSession(){
        router.back();
    },
    //普通列表搜索框
    showDialog1(e) {
        this.isShowIcon = false
        this.loadingStyle ='loading-hide'
        this.isShowLoading = false
        this.initData()
        this.$element('dialog').show()
    },
    //Logo列表搜索框
    showDialog2(e) {
        this.isShowIcon = true;
        this.loadingStyle ='loading-hide'
        this.isShowLoading = false
        this.initData()
        this.$element('dialog').show()
    },
    //网络加载搜索框
    showDialog3(e) {
        this.isShowIcon = true;
        this.isShowLoading = true
        this.initData()
        this.$element('dialog').show()
    },

    //整个search输入文本改变事件的监听
    onChangeListener(e) {
        this.key = e.value;
        this.onDataHandle()
    },

    //数据处理
    onDataHandle() {
        if (this.key.length == 0) {
            this.updateData()
            this.changeList = this.initDataList
        } else {
            if (this.isShowLoading) {
                this.loadingStyle ='loading-show' //加载框style请求前赋值
                var that = this
                var timeout = setTimeout(function(){
                    that.loadingStyle ='loading-hide' //加载框style请求完成后再赋值
                    that.filterData()
                },3000)
            }else{
                this.filterData()
            }

        }
    },

    //开源项目原来的数据初始化函数
    //拿到originalList数据后，初始化数据
    initData() {
        const changeArr = new Array((this.originalList.length))
        for (var index = 0; index < this.originalList.length; index++) {
            const element = this.originalList[index];
            const bean = new ChangeList()
            bean.id = element.id
            bean.isShow = this.isShowIcon
            bean.imgUrl = element.imgUrl
            bean.content = element.content

            const newArr = new Array((element.content.length))
            for (var j = 0; j < element.content.length; j++) {
                const ele = element.content.charAt(j);
                var strBean = new ChangeStrList()
                strBean.str = ele;
                strBean.isRed = '#222222';
                newArr[j] = strBean;
            }
            bean.list = newArr

            changeArr[index] = bean
        }
        this.changeList = changeArr
        this.initDataList = changeArr //保留一份初始化后的数据
    },
    //根据开源项目写的数据初始化函数
    initFriendsListData(){
        const changeArr = new Array((this.$app.$def.friendsList.length))
        for (var index = 0; index < this.$app.$def.friendsList.length; index++) {
            const element = this.$app.$def.friendsList[index];
            const bean = new ChangeList()
            bean.id = element.id
            bean.isShow = this.isShowIcon
            bean.imgUrl = element.imgUrl
            bean.content = element.text

            const newArr = new Array((element.text.length))
            for (var j = 0; j < element.text.length; j++) {
                const ele = element.text.charAt(j);
                var strBean = new ChangeStrList()
                strBean.str = ele;
                strBean.isRed = '#222222';
                newArr[j] = strBean;
            }
            bean.list = newArr

            changeArr[index] = bean
        }
        this.changeList = changeArr
        this.initDataList = changeArr //保留一份初始化后的数据
    },

    //筛选数据输入key
    filterData() {
        //更新数据
        this.updateData()
        //key中字符能够包含的
        const keyContainsArr = new Array();

        for (var index = 0; index < this.initDataList.length; index++) {
            const element = this.initDataList[index];

            for (var i = 0; i < this.key.length; i++) {
                const keyChar = this.key.charAt(i);

                for (var j = 0; j < element.list.length; j++) {
                    const ele = element.list[j];

                    if (keyChar == ele.str) {
                        ele.isRed = '#ff0000'
                        if (keyContainsArr.length>0) {
                            var pos = -1;
                            for (var t = 0; t < keyContainsArr.length; t++) {
                                var keyTele = keyContainsArr[t];
                                if (keyTele.id==element.id) {
                                    pos = t
                                }
                            }
                            if(pos !=-1){
                                keyContainsArr[pos] = element
                            }else{
                                keyContainsArr.push(element)
                            }
                        }else{
                            keyContainsArr.push(element)
                        }

                        break
                    }
                }
            }
        }
        //去重
//        this.changeList = Array.from(new Set(keyContainsArr))
        this.changeList = keyContainsArr

        //避免搜索延迟导致数据为空
        if (this.key.length == 0) {
            this.changeList = this.initDataList
        }
    },

    updateData() {
        for (var index = 0; index < this.initDataList.length; index++) {
            const element = this.initDataList[index];
            for (var j = 0; j < element.list.length; j++) {
                const ele = element.list[j];
                ele.isRed = '#222222'
            }
        }
    },

    onItemClick: function (data, position) {
//        prompt.showToast({
//            message: '点击了第：' + (position+1) + '个， 标题： ' + data.content
//        })
//        this.$element('dialog').close()
        this.$app.$def.idx = data.id;
        router.push({
            uri: "pages/dialogPage/dialogPage",
        });
    },
}
