/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@system.router';

const Gtalk = [
    {
        id: 1,
        imgUrl: 'common/images/personPic/1.png',
        text: '鸿蒙聊密码',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
];
export default {
    data: {
        GtalkList: Gtalk,
        re:{
            code:"",
            msg:"",
            data:""
        },
        testId:''
    },
    onInit() {
    },
    onShow() {
    },
    backToSession(){
        router.back();
    },
    changePassword(){
        router.push({
            uri: "pages/myList/setting/accountAndSecurity/password/password",
        });
    },

}