/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@system.router';
import http from '@ohos.net.http';
import prompt from '@ohos.prompt'

export default {
    data: {
        blackList: [],
    },
    onInit() {
        this.blackList = this.$app.$def.blackList
        this.getBlackList();
    },
    backToSetting(){
        router.back();
    },
    //获取黑名单
    getBlackList() {
        console.info('blackList=====>getBlackList:' + this.$app.$def.uid)
        let httpRequest1= http.createHttp();
        let promise = httpRequest1.request("http://" + this.$app.$def.ip +"/user/getBlacklist/" + this.$app.$def.uid, {
            connectTimeout: 60000,
            readTimeout: 60000,
            header: {
                'Content-Type': 'application/json'
            }
        });
        promise.then((res) => {
            let re = JSON.parse(res.result.toString())
            console.info('blackList====>getBlackList re: '+JSON.stringify(re))
            if(re.code == 200) {
                this.$app.$def.blackList = []
                for (let i = 0; i < re.data.length; i++) {
                    let blackFriend = {
                        id: re.data[i],
                        imgUrl: '/common/images/sessionPic/img_' + Math.round(Math.random()*7+1) + '.png',
                        text: re.data[i],
                        url: '',
                        style: 'pengyou'
                    }
                    this.$app.$def.blackList.push(blackFriend)
                }
                this.blackList = this.$app.$def.blackList
            }
        })
        httpRequest1.destroy()
    },
    //跳转详情页
    switchToDetail(id){
        console.info('blackList====>switchToDetail: '+id)
        this.$app.$def.mailFriendId = id;
        router.push({
            uri: "pages/dialogPage/chatMessagePage/friendDetails/friendDetails",
            params: {
                sayOrGroup: 'say',
                sayOrGroup_ID: id,
            },
        });
    },
    //将朋友移出黑名单
    removeBlackList(idx){
        console.info('remove ID:' + this.blackList[idx].id)
        let httpRequest2= http.createHttp();
        let promise = httpRequest2.request("http://" + this.$app.$def.ip +"/user/deleteFromBlacklist/", {
            method: 'POST',
            extraData:JSON.stringify({"uid":this.$app.$def.uid,"blockedUid":this.blackList[idx].id}),
            connectTimeout: 60000,
            readTimeout: 60000,
            header: {
                'Content-Type': 'application/json'
            }
        });
        promise.then((res) => {
            let re = JSON.parse(res.result.toString())
            console.info(res.toString())
            if(re.code == 2037) {
                prompt.showToast({
                    message: '已将联系人移出黑名单',
                    duration: 2000,
                });
                this.getBlackList()
            }
            else if(re.code == 2038) {
                prompt.showToast({
                    message: '联系人移出黑名单失败,逻辑',
                    duration: 2000,
                });
            }
        }).catch((err) => {
            prompt.showToast({
                message: '联系人移出黑名单失败,网络',
                duration: 2000,
            });
            console.error(`remove====> errCode:${err.code}, errMessage1:${err.data}`);
        });
        httpRequest2.destroy()
        this.onInit()
    }
}