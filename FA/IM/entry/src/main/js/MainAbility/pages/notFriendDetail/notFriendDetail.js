/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@system.router';

//个人信息假数据
const personData = {
    uid:'',
    name: '小蔡同学',
    nickname:'我叫学习',
    imgUrl: '/common/images/sessionPic/img_7.png',
    area: '英国',
}

export default {
    data: {
        person:personData,
        addFromGroup:true
    },
    onCreate() {
    },
    onInit() {
        this.person.uid=router.getParams().groupMemberId
        //获取群私聊权限，以此决定添加至通讯录是否显示
        this.addFromGroup=router.getParams().addFromGroup
    },
    backToGroupDetail(){
        router.back()
    }
}