/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@system.router';
import prompt from '@system.prompt';
import http from '@ohos.net.http';

const titles = [
    {
        'name': 'All'
    },

];

const chatData = [
    {
        'title': '账号',
        'topMessage': '欢迎来到华为鸿蒙操作系统',
        'imgUrl': '/common/images/sessionPic/img_8.png',
        'readTime': '17:00',
        'unRead': '8',
    },
    {
        'title': '密码',
        'topMessage': '吃了没有?',
        'imgUrl': '我叫学习',
        'readTime': '17:00',
        'unRead': '4',
    },

];
const personData = {
    'name': '我叫学习',
    'personNum': '萌聊号:133213321',
    'imgUrl': '/common/images/sessionPic/img_7.png',
}

export default {
    data: {
        titleList: titles,
        chatList: chatData,
        person:personData,
        // 滑动拼图验证的图片列表
        swipeImgList: [
            '/common/images/swipePuzzle/test1.jpg',
            '/common/images/swipePuzzle/test2.jpg',
            '/common/images/swipePuzzle/test3.jpg',
            '/common/images/swipePuzzle/test4.jpg',
            '/common/images/swipePuzzle/test5.jpg',
            '/common/images/swipePuzzle/test6.jpg',
            '/common/images/swipePuzzle/test7.jpg',
            '/common/images/swipePuzzle/test8.jpg'
        ],
        // 滑动拼图验证的参数
        swipePuzzleTestAttrs: {
            width: 300,
            height: 225,
            blockSize: 50
        },
        title: "",
        username:"",
        password:"",
        result:"111",
        code:"111",
        header:"111",
        user:{
            code:"",
            msg:"",
            data:""
        },
        checkState: false,
        receiveParams:{
            requestType: 2,
            uid: "12344",
        },
        uids:10,
//        receiveParams:"",
    },
    /*"nickname": "dssda",
            "password": 1234,*/
    onInit() {
        this.title = this.$t('strings.title');
    },
    /*onCreate(){
        //修改此处为服务器地址和端口号
        this.$app.$def.mysocket.connect("192.168.1.110",8080);
    },
    onDestroy(){
        this.$app.$def.mysocket.close();
    },*/
    // 事件处理函数，在此处定义验证完成后的操作
    onResReceived(e) {
        prompt.showToast({
            message: "父组件得到结果:" + e.detail.result
        });
    },
    // 监听用户输入框中数据信息的变化
    change(e) {
        this.username = e.value;
    },
    // 监听密码输入框中数据信息的变化
    change2(e) {
        this.password = e.value;
    },
    checkboxOnChange(e) {
        this.checkState = e.checked;
    },
    backToSession(){
        router.push({ // 页面跳转进入登录页面
            uri: "pages/login/login",
            params: this.username + "-" + this.password
        })
    },
    /**
     * 点击输入框操作
     *
     * @param e
     * @return 点击输入框操作
     */
    enterkeyClick(e) {
        prompt.showToast({
            message: "enterkey clicked",
            duration: 3000,
        });
    },
    clickCustomType(str) {
        console.log(str)
    },
    /**
     * 注册点击操作
     */
    loginClick(){
        prompt.showToast({
            message: '发送开始！',
            duration: 2000,
        });
        let httpRequest= http.createHttp();
        let promise = httpRequest.request("http://" + this.$app.$def.ip +"/user/register", {
            method: 'POST',
            extraData:JSON.stringify({"nickname":this.username,"password":this.password}),
            connectTimeout: 60000,
            readTimeout: 60000,
            header: {
                'Content-Type': 'application/json'
            }
        });
        promise.then((value) => {
            prompt.showToast({
                message: '发送成功！',
                duration: 2000,
            });
            console.info('registert111:' + value.result);
            //            this.result = value.result;
            this.user = JSON.parse(value.result.toString())
            this.result = this.user.data;
            this.code = this.user.code;
            prompt.showToast({
                message: '登录成功！',
                duration: 2000,
            });
            router.push({ // 页面跳转进入列表刷新
                uri: "pages/login/login",
                params:{
                    'uid':this.result
                }
            })
            //            this.header =JSON.parse(value.header);
            console.info('code1111:' + value.responseCode);
            console.info('header111:' + value.header);
        }).catch((err) => {
            console.error(`errCode:${err.code}, errMessage1:${err.data}`);
        });
    }

}



