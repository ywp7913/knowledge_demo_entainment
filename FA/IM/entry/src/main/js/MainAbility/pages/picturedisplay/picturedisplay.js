/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@system.router'

export default {
    data: {
        title: '全部照片',
        images: [
            "/common/images/my.jpg",
            "/common/images/address.png",
            "/common/images/back.png",
            "/common/images/file.png",
            "/common/images/harmonyos.jpg",
            "/common/images/fold.png",
            "/common/images/images.png",
            "/common/images/more.png",
            "/common/images/bob.jpg",
            "/common/images/tel.png",
            "/common/images/Wallpaper.png",
            "/common/images/xiangji.png",
            "/common/images/address.png",
            "/common/images/back.png",
            "/common/images/harmonyos.jpg",
            "/common/images/file.png",
            "/common/images/fold.png",
            "/common/images/images.png",
            "/common/images/more.png",
            "/common/images/harmonyos.jpg",
            "/common/images/tel.png",
            "/common/images/Wallpaper.png",
            "/common/images/xiangji.png"
        ],
        url:''

    },
    onInit(){


    },

    //选择图片的点击效果，当点击图片时图片的透明度，大小均发生变化，以此突出我们所选的图片。再次点击，图片效果恢复原始状态。
    //当选中图片时，将选中图片的uri放入app.js中的全局变量uri中储存。当取消选中时，从uri中移除此图片的uri。
    click(index) {

        const ID = "img"+index;
        if(this.$element(ID).style.opacity == 0.2){
            this.$element(ID).setStyle("padding","0px");
            this.$element(ID).setStyle("opacity","1");
            this.$app.$def.uri.splice(this.$app.$def.uri.indexOf(this.$element(ID).attr.src),1);
        }else{
            this.$element(ID).setStyle("padding","20px");
            this.$element(ID).setStyle("opacity","0.2");
            this.$app.$def.uri.push(this.$element(ID).attr.src);
        }
    },

    //左上角返回按钮，取消所有选中图片并返回聊天页面。
    returnDialogPage: function () {
        this.$app.$def.uri = [];
        router.back();

    },

    //发送图片： 返回聊天页面。
    sendPic(){
        router.back();

    },

    //取消按钮，取消所有选中图片，不返回聊天页面。
    cancel(){
        for (var i = 0; i < this.images.length; i++) {
            if(this.$element('img'+i).style.opacity == 0.2){
                this.$element('img'+i).setStyle("padding","0px");
                this.$element('img'+i).setStyle("opacity","1");}
        }
        this.$app.$def.uri = [];
    }

}