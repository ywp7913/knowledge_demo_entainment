/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@system.router';
import featureAbility from '@ohos.ability.featureAbility';
import display from '@ohos.display';

const titles = [
    {
        'name': 'All'
    },
    {
        'name': 'Health'
    },
    {
        'name': 'Finance'
    },
    {
        'name': 'Technology'
    },
    {
        'name': 'Sport'
    },
    {
        'name': 'Internet'
    },
    {
        'name': 'Game'
    }
];

const chatData = [
    {
        'title': '蔡同学',
        'topMessage': '欢迎来到华为鸿蒙操作系统',
        'imgUrl': '/common/images/sessionPic/img_8.png',
        'readTime': '17:00',
        'unRead': '8',
    },
    {
        'title': '朱老师',
        'topMessage': '吃了没有?',
        'imgUrl': '/common/images/sessionPic/img.png',
        'readTime': '17:00',
        'unRead': '4',
    },
    {
        'title': '小王老师',
        'topMessage': '这篇文章很好',
        'imgUrl': '/common/images/sessionPic/img_1.png',
        'readTime': '17:00',
        'unRead': '1',
    },
    {
        'title': '张学姐',
        'topMessage': '不用谢!',
        'imgUrl': '/common/images/sessionPic/img_2.png',
        'readTime': '17:00',
        'unRead': '2',
    },
    {
        'title': '盛同学',
        'topMessage': '我们交流一下......',
        'imgUrl': '/common/images/sessionPic/img_3.png',
        'readTime': '17:00',
        'unRead': '8',
    },
    {
        'title': '朱同学',
        'topMessage': '今天的任务是这些。',
        'imgUrl': '/common/images/sessionPic/img_4.png',
        'readTime': '17:00',
        'unRead': '4',
    },
    {
        'title': '陈老师',
        'topMessage': '好的，没问题！',
        'imgUrl': '/common/images/sessionPic/img_5.png',
        'readTime': '17:00',
        'unRead': '1',
    },
    {
        'title': '郑老师',
        'topMessage': '要注意劳逸结合~',
        'imgUrl': '/common/images/sessionPic/img_6.png',
        'readTime': '17:00',
        'unRead': '9',
    },
    {
        'title': '小吴老师',
        'topMessage': '这个技术目前可以探索...',
        'imgUrl': '/common/images/sessionPic/img_7.png',
        'readTime': '17:00',
        'unRead': '4',
    },
];

const findList = [
    {
        id: 1,
        imgUrl: '/common/images/findPic/pengyouquan.png',
        text: '朋友圈',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 2,
        imgUrl: '/common/images/findPic/saoyisao.png',
        text: '扫一扫',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 3,
        imgUrl: '/common/images/findPic/yaoyiyao.png',
        text: '摇一摇',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 4,
        imgUrl: '/common/images/findPic/kanyikan.png',
        text: '看一看',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 5,
        imgUrl: '/common/images/findPic/souyisou.png',
        text: '搜一搜',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 6,
        imgUrl: '/common/images/findPic/gouwu.png',
        text: '购物',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 7,
        imgUrl: '/common/images/findPic/youxi.png',
        text: '游戏',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    },
    {
        id: 8,
        imgUrl: '/common/images/findPic/xiaochengxu.png',
        text: '小程序',
        url: '',
        find_right_icon: '/common/images/findPic/find-right-icon.png'
    }
]

export default {
    data: {
        titleList: titles,
        chatList: chatData,
        findList: findList
    },
    onInit() {
//        this.title = this.$t('strings.world');
        console.log('宽度:' + display.width + "高度：" + display.height);
        this.grantPermission()
    },
    /*获取部分权限*/
    grantPermission() {
        console.info('grantPermission')
        let context = featureAbility.getContext()
        context.requestPermissionsFromUser(['ohos.permission.DISTRIBUTED_DATASYNC'], 666, function (result) {
            console.info(`grantPermission,requestPermissionsFromUser`)
        })
    },
    // 选择新闻类型
    changeNewsType: function(e) {
        const type = titles[e.index].name;
        this.chatList = [];
        if (type === 'All') {
            // 展示全部新闻
            this.chatList = chatData;
        } else {
            // 分类展示新闻
            const newsArray = [];
            for (const news of chatData) {
                if (news.type === type) {
                    newsArray.push(news);
                }
            }
            this.chatList = newsArray;
        }
    },
    itemClick(chat){
        router.push({
            uri: "pages/dialogPage/dialogPage",
            params: {
                'title': chat.title,
                'type': chat.topMessage,
                'imgUrl': chat.imgUrl,
                'reads': chat.readTime,
                'likes': chat.unRead,
            }
        });
    },
    chat(){
        router.push({
            uri: "pages/sessionList/sessionList",
        });
    },
    message(){
        router.push({
            uri: "pages/mailList/mailList",
        });
    },
    my(){
        router.push({
            uri: "pages/myList/myList",
        });
    },


}



