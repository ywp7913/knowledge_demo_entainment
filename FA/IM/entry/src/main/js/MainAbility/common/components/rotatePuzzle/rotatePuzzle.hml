<!--/*-->
<!-- * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC-->
<!-- * Licensed under the Apache License, Version 2.0 (the "License");-->
<!-- * you may not use this file except in compliance with the License.-->
<!-- * You may obtain a copy of the License at-->
<!-- *-->
<!-- *     http://www.apache.org/licenses/LICENSE-2.0-->
<!-- *-->
<!-- * Unless required by applicable law or agreed to in writing, software-->
<!-- * distributed under the License is distributed on an "AS IS" BASIS,-->
<!-- * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.-->
<!-- * See the License for the specific language governing permissions and-->
<!-- * limitations under the License.-->
<!-- */-->

<!--    旋转拼图-->
<div class="rotate">

<!--验证区域-->
    <stack class="rotateCaptcha">
        <image class="rotateCaptcha_pic" src="{{ rotateCurImgUrl }}"
               style="width : {{ rotateImgWidth }}; height : {{ rotateImgHeight }};">
        </image>

    <!--旋转-->
        <div class="rotateVerifiee"
             style="background-image : {{ rotateCurImgUrl }};
                     background-size : {{ rotateImgWidth + 'px' }} {{ rotateImgHeight + 'px' }};
                     background-position : center;
                     top : {{ rotateImgHeight / 2 - rotateVerifieeRadius + 'px' }};
                     left : {{ rotateImgWidth / 2 - rotateVerifieeRadius + 'px' }};
                     border-radius : {{ rotateVerifieeRadius + 'px' }};
                     width : {{ rotateVerifieeRadius * 2 + 'px' }}; height : {{ rotateVerifieeRadius * 2 + 'px' }};
                     transform : rotate({{ rotateVerifieeAgl + 'deg' }});">
        </div>

    <!--刷新-->
        <image class="rotateRefreshBtn" src='/common/images/refreshicon.png'
               onclick="onClickRotateRfsBtn"
               disabled="{{ rotateFunDisabled }}">

        </image>

    <!--            提示弹窗-->
        <div class="rotateNtfWindow"
             style="bottom : {{ rotateNtfWindowPos + 'px' }}; width : {{ rotateImgWidth }};
                     background-color : {{ rotateNtfColor }};
                     animation-name : rotateEmerge; animation-duration : 500ms;
                     animation-play-state : {{ rotatePlayAnim }};
                     animation-fill-mode : forwards;">
            <text class="rotateNtfWindowTxt">
                {{ rotateNtfText }}
            </text>

        </div>
    </stack>

<!--自定义滑轨-->
    <stack class="rotateSlider">

    <!--滑轨边界-->
        <div class="rotateSliderBorder"
             style="height : 40px; width : {{ rotateImgWidth }};">
        </div>

    <!--滑轨提示文字-->
        <div class="rotateSliderTxtContainer" style="height : 40px; width : {{ rotateImgWidth }};">
            <text class="rotateSliderTxt" style="opacity : {{ rotateTextOpacity }};">
                {{ $t('strings.RotateTxt') }}
            </text>
        </div>

    <!--            已经划过的区域-->
        <div class="rotateProgress"
             style="height : 40px; width : {{ rotateBlockPosLeft + rotateBlockWidth / 2 }};">

        </div>

    <!--滑块-->
        <div class="rotateSliderBlock"
             style="height : 40px; width : {{ rotateBlockWidth }};
                     left : {{ rotateBlockPosLeft }};"
             disabled="{{ rotateFunDisabled }}"
             ontouchstart="onRotateSliderStart"
             ontouchmove="onRotateSliderMove"
             ontouchend="onRotateSliderStop">
            <image class="rotateBlockImg"
                   src="/common/images/swipeicon.png">

            </image>
        </div>

    </stack>
</div>