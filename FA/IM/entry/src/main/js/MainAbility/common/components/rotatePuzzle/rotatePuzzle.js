/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export default {
    props: {
        // 图片列表
        imgList: [],
        // 组件参数
        compAttrs: {},
    },
    data: {
        rotateImageList: [],
        rotateUsedList: [],
        rotateCurImgUrl: '',
        //背景图和组件的尺寸
        rotateImgWidth: 300,
        rotateImgHeight: 225,

        //滑块的动态位置
        rotateBlockWidth: 50, //滑块宽度
        rotateBlockPosLeft: 0,
        rotateStartRealXBias: 0, ////触摸开始的实际位置
        rotateVerifyThreshold: 8, // 偏差阈值 8度以内通过 测试用

        //旋转块参数
        rotateVerifieeRadius: 100, //旋转区域大小
        rotateStartAgl: 0, //记录初始角度
        rotateVerifieeAgl: 0, //验证区域顺时针旋转角度
        rotateMinAngle: 30,
        rotateMaxAngle: 330,

        //验证结果弹窗
        rotateNtfWindowPos: -40,
        rotateNtfText: [],
        rotateNtfColor: '',
        rotatePlayAnim: 'paused',
        rotateFunDisabled: false, //使能页面功能

        //提示文字透明度
        rotateTextOpacity: 1,

        //时间戳信息
        rotateStartTime: 0,
        rotateEndTime: 0,
    },
    onInit() {
        this.rotateImageList = this.imgList;
        this.rotateImgWidth = this.compAttrs.width;
        this.rotateImgHeight = this.compAttrs.height;
        this.rotateVerifieeRadius = this.compAttrs.radius;
        this.newRotateCaptcha();
    },
    //点击刷新按钮
    onClickRotateRfsBtn(e) {
        this.newRotateCaptcha();
    },
    //开始拖动
    onRotateSliderStart(e) {
        //记录开始时间和开始触摸的位置
        this.rotateStartTime = this.getCurTime();
        this.rotateStartRealXBias = e.touches[0].localX;
        // 获取验证区域的旋转角度
        this.rotateStartAgl = this.rotateVerifieeAgl;
    },
    //移动slider
    onRotateSliderMove(e) {
        let curX = e.touches[0].localX;
        // 确保不越界
        if (curX <= this.rotateStartRealXBias || curX >= this.rotateImgWidth - this.rotateBlockWidth + this.rotateStartRealXBias) return;
        // 改变滑块和图像角度
        this.rotateBlockPosLeft = curX - this.rotateStartRealXBias;
        this.rotateVerifieeAgl = this.rotateStartAgl - 360 * (this.rotateBlockPosLeft / (this.rotateImgWidth - this.rotateBlockWidth));
        // 改变文字透明度
        this.rotateTextOpacity = (1 - 3 * (this.rotateBlockPosLeft / this.rotateImgWidth)) >= 0 ? (1 - 3 * (this.rotateBlockPosLeft / this.rotateImgWidth)) : 0;
    },
    //停止拖动
    onRotateSliderStop(e) {
        //记录结束时间
        this.rotateEndTime = this.getCurTime();
        //检查滑块位置
        this.checkRotateCaptcha(this.rotateVerifieeAgl);
    },

    // 创建新的旋转验证
    newRotateCaptcha() {
        //生成随机角度
        this.genRandomAngle();
        // 选择随机图片
        this.selectRotateRdmImg();
    },

    genRandomAngle() {
        // 旋转限制在一定范围内
        let minAgl = this.rotateMinAngle;
        let maxAgl = this.rotateMaxAngle;
        // x-y范围内的随机整数表示为Math.round(Math.random()*(y-x)+x)
        this.rotateVerifieeAgl = Math.round(Math.random() * (maxAgl - minAgl) + minAgl);
    },

    //从列表中随机选择一张
    selectRotateRdmImg() {
        let imgListLength = this.rotateImageList.length;

        if (imgListLength == 0) {
            //图片全部使用过
            this.rotateImageList = this.rotateUsedList;
            this.rotateUsedList = [];
            imgListLength = this.rotateImageList.length;
        }
        //随机生成index
        let randomIdx = Math.round(Math.random() * (imgListLength - 1));
        this.rotateCurImgUrl = this.rotateImageList[randomIdx];
        this.rotateUsedList.push(this.rotateImageList.splice(randomIdx, 1)[0]);
    },
    //检验验证是否成功
    checkRotateCaptcha(movement) {
        let curAngle = movement;
        //显示弹窗
        let isValid = curAngle >= -this.rotateVerifyThreshold
        && curAngle <= this.rotateVerifyThreshold;
        this.showRotateVerifyWindow(isValid);
        // 开启弹窗动画
        this.rotatePlayAnim = 'running';
        // 禁用滑块和刷新
        this.rotateFunDisabled = true;
        // 返回结果给父组件
        this.returnRes(isValid);
        // 暂停页面2秒
        setTimeout(() => {
            //重置页面
            this.rotatePlayAnim = 'paused'; //动画暂停
            this.rotateNtfWindowPos = -40; //隐藏弹窗
            this.rotateTextOpacity = 1; //显示提示文本
            this.rotateBlockPosLeft = 0; //滑块归零
            this.newRotateCaptcha(); //刷新图片
            this.rotateFunDisabled = false; //允许使用页面功能
        }, 2000);
    },
    //弹成功失败窗口
    showRotateVerifyWindow(isVerified) {
        if (isVerified) {
            this.rotateNtfText = this.$t('strings.successNtf') +
            " 用时" + ((this.rotateEndTime - this.rotateStartTime) / 1000).toFixed(1) + "秒";
            this.rotateNtfColor = "#049304";
        } else {
            this.rotateNtfText = this.$t('strings.failNtf');
            this.rotateNtfColor = "#f74a4a";
        }
    },
    //获取当前时间
    getCurTime() {
        return new Date();
    },
    // 返回验证结果
    returnRes(result) {
        this.$emit("handleResult", {
            result: result
        });
    }
}
