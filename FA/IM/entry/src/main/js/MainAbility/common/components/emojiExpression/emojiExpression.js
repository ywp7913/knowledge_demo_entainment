/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// @ts-nocheck
var faceDateBase = [
    { alt: '[微笑]', src: '/common/images/face/Expression_1.png', title: 1 },
    { alt: '[撇嘴]', src: '/common/images/face/Expression_2.png', title: 2 },
    { alt: '[色]', src: '/common/images/face/Expression_3.png', title: 3 },
    { alt: '[发呆]', src: '/common/images/face/Expression_4.png', title: 4 },
    { alt: '[得意]', src: '/common/images/face/Expression_5.png', title: 5 },
    { alt: '[流泪]', src: '/common/images/face/Expression_6.png', title: 6 },
    { alt: '[害羞]', src: '/common/images/face/Expression_7.png', title: 7 },
    { alt: '[闭嘴]', src: '/common/images/face/Expression_8.png', title: 8 },
    { alt: '[睡]', src: '/common/images/face/Expression_9.png', title: 9 },
    { alt: '[大哭]', src: '/common/images/face/Expression_10.png', title: 10 },
    { alt: '[尴尬]', src: '/common/images/face/Expression_11.png', title: 11 },
    { alt: '[发怒]', src: '/common/images/face/Expression_12.png', title: 12 },
    { alt: '[调皮]', src: '/common/images/face/Expression_13.png', title: 13 },
    { alt: '[呲牙]', src: '/common/images/face/Expression_14.png', title: 14 },
    { alt: '[惊讶]', src: '/common/images/face/Expression_15.png', title: 15 },
    { alt: '[难过]', src: '/common/images/face/Expression_16.png', title: 16 },
    { alt: '[囧]', src: '/common/images/face/Expression_17.png', title: 17 },
    { alt: '[抓狂]', src: '/common/images/face/Expression_18.png', title: 18 },
    { alt: '[吐]', src: '/common/images/face/Expression_19.png', title: 19 },
    { alt: '[偷笑]', src: '/common/images/face/Expression_20.png', title: 20 },
    { alt: '[愉快]', src: '/common/images/face/Expression_21.png', title: 21 },
    { alt: '[白眼]', src: '/common/images/face/Expression_22.png', title: 22 },
    { alt: '[傲慢]', src: '/common/images/face/Expression_23.png', title: 23 },
    { alt: '[困]', src: '/common/images/face/Expression_24.png', title: 24 },
    { alt: '[惊恐]', src: '/common/images/face/Expression_25.png', title: 25 },
    { alt: '[流汗]', src: '/common/images/face/Expression_26.png', title: 26 },
    { alt: '[憨笑]', src: '/common/images/face/Expression_27.png', title: 27 },
    { alt: '[悠闲]', src: '/common/images/face/Expression_28.png', title: 28 },
    { alt: '[奋斗]', src: '/common/images/face/Expression_29.png', title: 29 },
    { alt: '[咒骂]', src: '/common/images/face/Expression_30.png', title: 30 },
    { alt: '[疑问]', src: '/common/images/face/Expression_31.png', title: 31 },
    { alt: '[嘘]', src: '/common/images/face/Expression_32.png', title: 32 },
    { alt: '[晕]', src: '/common/images/face/Expression_33.png', title: 33 },
    { alt: '[衰]', src: '/common/images/face/Expression_34.png', title: 34 },
    { alt: '[骷髅]', src: '/common/images/face/Expression_35.png', title: 35 },
    { alt: '[敲打]', src: '/common/images/face/Expression_36.png', title: 36 },
]

export default {
    data:{
        faceDateBase:faceDateBase,
        selectFaceContent:'',
        inputMsg:'',
        idx:0,
        uid:'1111',
        groupId:'',
        //创建一个变量，用来保存当前的索引
        index: 0,
        //显示当前是删除还是撤回
        deleteOrRecall:'删除',
        //创建一个数组来保存撤回、删除
        imgArr: ["撤回", "删除"],
    },
    onCreate(){
        //该登录用户的uid
        this.uid =  this.$app.$def.uid;
        //如果是群聊，为该群聊的用户id
        this.groupId = this.$app.$def.chatData[this.idx].uid[0]
    },
    /*
	 * 开启一个定时器，来自动变换文字
	 */
    changeState(){
        //定义一个变量，用来保存定时器的标识
        var timer;
        //在开启定时器之前，需要将当前元素上的其他定时器关闭
        clearTimeout(timer);
        var that = this
        timer = setTimeout(function() {
            //修改img1的src属性
            //使索引自增
            that.changeState();
        }, 120000);
        //判断索引是否超过最大索引
        if(this.index < 2){
            this.$app.$def.deleteOrRecall = this.imgArr[this.index];
            this.index++;
        }
        else{
            clearTimeout(timer);
        }
    },
    //选择表情发送
    selectFace(fId){
        //该登录用户的uid
        this.uid =  this.$app.$def.uid;
        //如果是群聊，为该群聊的用户id
        this.idx = this.$app.$def.idx;
        this.groupId = this.$app.$def.chatData[this.idx].uid[0]
        this.selectFaceContent = this.faceDateBase[fId-1].src;
        //定时器，撤回转删除
        this.index = 0;
        this.changeState();
        let timestamp = new Date().getTime().toString() - 28800000;
        this.$app.$def.chatData[this.idx].MsgList.push({isMe: true,
            sendUid:"",
            messageType: 2,
            content: this.selectFaceContent,
            profPicUrl:'/common/images/my.jpg',
            createTime:timestamp,});
        this.sendImage(fId, timestamp);
//        this.inputMsg = this.inputMsg + this.selectFaceContent;
//        console.info("dialogPages=====inputMsg:" + this.inputMsg);
    },
    //发送表情的socket通讯
    sendImage(FId, timestamp) {
//        let timestamp = new Date().getTime().toString() - 28800000;
        let obj = {}
        let json = {}
        if(this.$app.$def.chatData[this.idx].unRead == 'say'){
            console.info("emoji====>sendImage:type == say " );
            obj = { uid: this.uid, touid: this.$app.$def.chatData[this.idx].uid , role:'ho_user' , type:'say', content:FId, stype:4,timestamp:timestamp};
            json = JSON.stringify(obj);
            console.info("emoji====>sendImage:json == " + json);
        }
        else if(this.$app.$def.chatData[this.idx].unRead == 'group'){
            console.info("emoji====>sendImage:type == group " );
            obj = { uid: this.uid, touid: [this.groupId] , role:'ho_user' , type:'group', content:FId, stype:4,timestamp:timestamp};
            json = JSON.stringify(obj);
            console.info("emoji====>sendImage:json == " + json);
        }
        this.$app.$def.mySocket.send(json)
    },
}