/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@system.router';
export default {
    props: {
        title: {
            default: 'title',
        }
    },
    data() {
        return {
            message: 'message',
        };
    },
    message(){
        router.push({
            uri: "pages/mailList/mailList",
        });
    },
    chat(){
        router.push({
            uri: "pages/sessionList/sessionList",
        });
    },
    find(){
        router.push({
            uri: "pages/findList/findList",
        });
    },
    my(){
        router.push({
            uri: "pages/myList/myList",
        });
    },
}
