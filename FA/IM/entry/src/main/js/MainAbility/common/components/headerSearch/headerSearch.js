/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import router from '@system.router';
import prompt from '@system.prompt';

export default {
    props: {
        title: {
            default: 'title',
        },
        selector: [
            {
                'function': '发起群聊',
                'imgUrl': 'common/images/sessionPic/add.png',
            },
            {
                'function': '添加朋友',
                'imgUrl': 'common/images/sessionPic/add.png',
            },
            {
                'function': '扫一扫',
                'imgUrl': 'common/images/sessionPic/add.png',
            },
            {
                'function': '收付款',
                'imgUrl': 'common/images/sessionPic/add.png',
            },
        ],
    },
    data() {
        return {
            message: 'message',
            searchContent:''
        };
    },
    change(e){
        this.searchContent = e.value
    },
    search () {
        prompt.showToast({
            message: '点击按钮成功！',
            duration: 2000,
        });
        console.info('mysearch' + this.searchContent)
        this.$emit('eventType1', {text: '收到输入框组件参数'});
        router.push({
            uri: "pages/searchPage/searchPage",
            params: {
                searchContent: this.searchContent,
            },
        });
    },
    showpopup() {
        console.info("headerSearch====> popup");
        this.$element("popup").show();
        console.info("headerSearch====> popup show");
    },
    hidepopup() {
        this.$element("popup").hide();
        console.info("headerSearch====> popup hide");
    },
    addFriend(){
        router.push({
            uri: "pages/addFriends/addFriends",
        });
    },
    createGroup() {
        router.push({
            uri: "pages/createGroup/createGroup",
            params: {
                operation: 'create',
            },
        });
    }
}
