/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
const Bus = {
    // 发布的事件集合
    $events:{ },

    /**
     * 发布事件方法
     * type: string 字符串
     * fun: function 绑定的方法
     * */
    $on(type,fun){
        let res = "";
        if(type && typeof type == "string" && fun &&　typeof fun　=="function"){
            console.info('[eventBus] $on:' + type)
            let events = this.$events[type];
            if(events){
                let index = -1
                for (let i = 0; i < events.length; i++) {
                    if(events[i] === null){
                        index = i;
                        break;
                    }
                }
//                let index = events.findIndex(null);
                if(index > -1){
                    res = `${String(index)}${type}`;
                    events[index] = fun;
                }else{
                    events.push(fun);
                }
//                events.push(fun);
            }else{
                this.$events[type] = [fun];
                res = `0${type}`;
            }

        }
        return res;
    },

    /**
     * 触发事件方法
     * type: string 发布事件的字符串
     * args: 传参
     * */
    $emit(type,...args){
        console.info('[eventBus] $emit:' + type)
        if(type && typeof type == "string"){
            let events = this.$events[type];
            if(events){
                events.forEach(fun => {
                    if(fun &&　typeof fun　=="function"){
                        fun(...args);
                    }
                });
            }
        }
    },

    /**
     * 注销事件方法
     * type: string 字符串
     * fun: string|function 发布事件时返回的值或者发布的原function
     * */
    $off(type,fun){
        console.info('[eventBus] $off:' + type)
        if(type && typeof type == "string" && fun){
            console.info('[eventBus] $off:' + type)
            let events = this.$events[type];
            if(events){
                if(typeof fun == "string"){
                    let indexStr = fun.replace(type,'');
                    let index = parseInt(indexStr);
                    events[index] = null;
                }
                if(typeof fun == "function"){
                    events.forEach(item => {
                        if(item == fun){
                            item = null;
                        }
                    });
                }
            }
        }
    }
}

export default Bus;