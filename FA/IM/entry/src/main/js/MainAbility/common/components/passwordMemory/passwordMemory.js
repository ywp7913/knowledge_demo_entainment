/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import prompt from '@system.prompt';
import router from '@system.router';
export default{
    props:{
        // 注册账号参数
        compAttrs: '',
    },
    data:{
        GtalkList: '',
        username:'',
        infuse: 0,
    },
    onInit(){
        this.GtalkList = this.$app.$def.memory;
        console.info("passwordMemory====>onInit:" + this.compAttrs)
    },
    /**
     * 弹出记忆账号操作
     *
     * @param null
     * @return 弹出记忆账号操作
     */
    showPopup(){
        this.$element("popup").show();
        console.info("passwordMemory====>showPopup")

    },
    /**
     * 点击输入框操作
     *
     * @param e
     * @return 点击输入框操作
     */
    enterkeyClick(e) {
        prompt.showToast({
            message: "enterkey clicked",
            duration: 3000,
        });
    },
    // 监听用户输入框中数据信息的变化
    change(e) {
        this.username = e.value;
        this.$app.$def.uid = this.username;
        console.info("passwordMemory====>this.username: " + this.$app.$def.uid)
    },
    /**
     * 记忆账号注入操作
     *
     * @param id
     * @return 记忆账号注入操作
     */
    accountAndSecurity(id){
        this.compAttrs = this.GtalkList[id].text;
        this.$app.$def.uid = this.compAttrs;
        this.infuse = 1;
        console.info("passwordMemory====>this.$app.$def.uid: " + this.$app.$def.uid)
    },
    //当组件获得焦点且value不为初值，表示是账号注入->刷新一下
    focus(e){
        console.info("passwordMemory====>focus.infuse: " + this.infuse)
        if(this.infuse == 1 || this.compAttrs != undefined){
            console.info("passwordMemory====>focus.compAttrs: " + this.compAttrs)
            router.push({
                uri: "pages/login/login",
            })
        }
    },
    //刷新现有状态
//    refreshState(){
//        var that = this;
//        setTimeout(function(){
//            that.refreshState();
//        },2000)
//    },
    hidepopup() {
        this.$element("popup").hide();
        console.info("passwordMemory====>hidepopup")

    },
}