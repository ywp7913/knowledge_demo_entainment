/*
 * Copyright 2022 Institute of Software Chinese Academy of Sciences, ISRC
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var tabbarData = [
    {
        i: 0,
        title: '主页',
        color: '#515151',
        selectedColor: '#EEB174',
        img: '/common/images/sessionPic/home.png',
        selectedImg: '/common/images/sessionPic/selected_home.png',
        show: true,
        hasDot: 0
    },
    {
        i: 1,
        title: '通讯录',
        color: '#515151',
        selectedColor: '#EEB174',
        img: '/common/images/sessionPic/mail.png',
        selectedImg: '/common/images/sessionPic/selected_mail.png',
        show: false,
        hasDot: 'none'
    },
    {
        i: 1,
        title: '发现',
        color: '#515151',
        selectedColor: '#EEB174',
        img: '/common/images/sessionPic/find.png',
        selectedImg: '/common/images/sessionPic/selected_find.png',
        show: false,
        hasDot: 'none'
    },
    {
        i: 3,
        title: '我的',
        color: '#515151',
        selectedColor: '#EEB174',
        img: '/common/images/sessionPic/user.png',
        selectedImg: '/common/images/sessionPic/selected_user.png',
        show: false,
        hasDot: 'none'
    }
]

export default tabbarData;