2022.06.10

socket的使用方法。

供js调用的接口：
1. connect(address,port);
2. send(message);
3. close();
4. receive();

使用实例：
1. 在app.js中 import socket from 'libsocket.so'
2. 在app.js里创建全局变量mySocket。
3. 在onCreate()中连接服务器 this.mysocket.connect("192.168.1.110",8090);
4. 发送时：
   1. var obj = {requestType:1,uid:this.username,password:this.password};
   2. //通过mySocket连接发送数据。使用callback方式作为异步方法。
   3. this.$app.$def.mysocket.send(JSON.stringify(obj))
5. 接收时：
   1. this.$app.$def.mysocket.receive()

![输入图片说明](../resources/base/media/%E6%88%AA%E5%B1%8F2022-06-10%20%E4%B8%8B%E5%8D%886.40.02%20(2).png)

![输入图片说明](../resources/base/media/%E6%88%AA%E5%B1%8F2022-06-10%20%E4%B8%8B%E5%8D%886.52.34%20(2).png)

存在的问题：
1. 目前所有变量，方法都采用静态标识static修饰，使用便利但存在安全问题，建议在后续迭代中修改为非静态。
2. 共享内存：此版本的共享内存仅被定义成了一个char数组，采用覆盖式更改数据，同一时间内存仅容纳一个消息，当下一个消息到来后，将前一消息覆盖掉，若js未及时访问，共享内存中数据将丢失。建议将共享内存由简单的一个char数组改为一个由循环链表构成的消息队列。每次放入消息时放入队列末尾，每次读取时从队列开头读取，并将读到的数据从消息队列中移除。
3. 共享内存的读取未加同步锁，建议加上锁机制。