/*
 * Copyright (c) 2021 Cocollria
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export function Chess(name, type, owner, health, max_health, attack, move, defence, position, img_x, img_y, x, y){
    this.name = name;                   //名字
    this.type = type;                   //类型
    this.owner = owner;                 //从属关系

    this.health = health;               //生命值
    this.max_health = max_health;       //最大生命值

    this.attack = attack;               //攻击
    this.move = move;                   //行动力
    this.defence = defence;             //防御

    this.position = position;           //方向    0为向下 1为向左 2为向右 3为向上
    this.state = 0;                     //状态    0为正常状态 -1为激昂 1为低落 2为混乱

    this.have_moved = false;            //是否已移动
    this.have_attacked = false;         //是否已攻击

//    this.choose = false;                //是否被选择

    this.img_x = img_x;                 //棋子的图块的正常x坐标
    this.img_y = img_y;                 //棋子的图块的正常y坐标
    this.add_offX = 1;                  //下一次图块的x要偏移的位置(仅限敌方棋子使用)
    this.img_offX = 1;                  //棋子的图块的x偏移量
    this.img_offY = 0;                  //棋子的图块的y偏移量

    this.x = x;                         //棋子的x坐标
    this.y = y;                         //棋子的y坐标
}

Chess.prototype = {
    constructor : Chess,
    setLocation : function(x,y){
        this.x = x;
        this.y = y;
    },
    IsLocation : function(x,y){
        if(x == this.x && y == this.y){
            return true;
        }
        else{
            return false;
        }
    }
}