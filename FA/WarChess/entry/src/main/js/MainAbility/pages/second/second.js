/*
 * Copyright (c) 2021 Cocollria
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//攻击时没有对着攻击方向
//被攻击时没有对着被攻击的方向
//没有阵亡时的动画

import {Chess} from "../../common/js/Chess.js"
import {find_Search_Map_1,find_Attack_Map_1,find_Search_Map_2,find_Attack_Map_2,get_move_route,get_Search_Map,get_Attack_Map,clearMap} from "../../common/js/Backstate_Map.js"
import {fight_against,change_position} from "../../common/js/Attack_Time.js"

//可调的参数
const IMAGE_LENGTH = 48;                //雪碧图各图块的原本大小
var GRAPH_WIDTH = 48;                   //在屏幕上显示的图块大小
var Max_Round = 8;                      //允许的最大回合数
var fps = 6;                            //帧数

var CANVAS_HEIGHT = 0                   //画布的高度

var MAP_COL = 0;                        //地图的列数
var MAP_ROW = 0;                        //地图的行数

var MAP_WIDTH = 0;                      //地图的宽度
var MAP_HEIGHT = 0;                     //地图的高度

var MAP_DISPLAY_X = 0;                  //地图显示的x轴偏移量
var MAP_DISPLAY_Y = 0;                  //地图显示的y轴偏移量

var OFF_SET_X = 0;                      //地图在屏幕上的位置的x轴偏移量
var OFF_SET_Y = 0;                      //地图在屏幕上的位置的y轴偏移量

var map = [
    [1,1,1,1,1,1,1,1,1,1,1,1,1],
    [1,0,0,0,0,0,0,0,0,0,0,0,1],
    [1,0,0,0,0,0,101,0,0,0,0,0,1],
    [1,0,0,0,0,0,0,0,0,0,0,0,1],
    [1,0,0,0,0,0,0,0,1,1,0,0,1],
    [1,0,0,1,1,0,1,1,1,0,204,0,1],
    [1,0,1,0,0,0,0,203,0,0,0,0,1],
    [1,0,0,0,201,1,0,1,1,1,1,0,1],
    [1,0,1,1,1,1,0,1,1,0,0,0,1],
    [1,204,0,0,0,0,0,0,0,0,0,202,1],
    [1,0,0,1,1,1,1,1,1,1,1,0,1],
    [1,0,0,0,0,0,204,0,0,0,0,0,1],
    [1,0,0,0,0,0,0,0,0,0,0,0,1],
    [1,0,0,0,0,0,0,0,0,0,0,0,1],
    [1,0,0,0,0,0,0,0,0,0,0,0,1],
    [1,1,1,1,1,1,1,1,1,1,1,1,1]
];

//循环绘图线程有关的
var func_flag = 0;                      //选择当前动画线程中的功能
var now = 0;                            //现在时间
var then = Date.now();                  //上一次的时间
var interval = 1000/fps;                //定时器
var delta = 0;                          //时间差

//图片有关的
var MapImg = null;                      //地图的地面图块
var MaterialImg = null;                 //地图的地面上的素材图块(可以是障碍物之类的)
var CharacterImg = null;                //角色的图块
var AImg = null;                        //敌人的图块
var WindowImg = null;                   //小窗口图块和色块
var Attack_Img = null;                  //攻击时需要的图块

//记录当前棋子数量的
var myList = null;                      //记录己方棋子
var enemyList = null;                   //记录敌方棋子

//记录地图当前的状态
var allow_click = true;                 //允许点击触发事件
var Round = 1;                          //当前的回合数
var Player_Round = true;                //判断是否为玩家回合
var Win_Flag = false;                   //判断是否获胜
var Lose_Flag = false;                  //判断是否失败
var myFlag = -1;                        //棋子被选择的列表位置
var enemyFlag = -1;                     //当前攻击的敌方的列表位置

//记录地图上次的数据的以供下一次绘画进行清理
var lastSearchMap = null;               //上一次的搜寻图
var lastAttackMap = null;               //上一次的攻击图

//一些长动画需要的数据
var move_num = -1;                      //暂存要移动的人物的参数
var move_route = null;                  //移动路线图
var move_flag = 0;                      //移动的步伐
var step = 4;                           //移动一格需要的次数
var move_fps = 0;                       //移动所需要的图片帧数
var move_col = -1;                      //当前所在的移动的列
var move_row = -1;                      //当前所在的移动的行
var move_off_x = 0;                     //当前所在的移动的列的偏移量
var move_off_y = 0;                     //当前所在的移动的行的偏移量
var attack_fps = 0;                     //攻击所需要的图片帧数
var attack_img_offX = 0;                //当前攻击动画的列的偏移量
var attack_img_offY = 0;                //当前攻击动画的行的偏移量
var attack_round = 0;                   //记录当前的攻击回合(一次攻击里面最多进行攻击3次攻击，0标志结束,1标志进攻方攻击,2标志防守方反击)

export default{
    data:{
        ctx:null,                       //画布的上下文
        offscreen:null,                 //离屏画布
        bitmap:null,                    //离屏画布生成的地图图片

        local_x:0,                      //当前点击的x轴位置
        local_y:0,                      //当前点击的y轴位置

        //需要显示的数据
        round_show:false,               //显示回合属性
        my_show:"hidden",               //显示己方棋子属性
        detail_my_show:"hidden",        //显示己方棋子详细属性
        enemy_show:"hidden",            //显示敌方棋子属性
        detail_enemy_show:"hidden",     //显示敌方棋子详细属性
        game_over_show:false,
        game_over_result:null,

        my_margintop:0,                 //己方棋子属性的上空隙
        enemy_margintop:0,              //敌方棋子属性的上空隙

        show_round:1,                   //显示回合
        show_max_round:1,               //显示最大回合

        mychessName:null,               //显示己方名字
        mychessHealth:0,                //显示己方生命
        mychessMaxhealth:0,             //显示己方最大生命上限
        mychessAttack:0,                //显示己方攻击
        mychessDefence:0,               //显示己方防御
        mychessMove:0,                  //显示己方行动力点数
        myhealthPercent:0,              //己方生命占比

        enemychessName:null,            //显示敌方名字
        enemychessHealth:0,             //显示敌方生命
        enemychessMaxhealth:0,          //显示敌方最大生命上限
        enemychessAttack:0,             //显示敌方攻击
        enemychessDefence:0,            //显示敌方防御
        enemychessMove:0,               //显示敌方行动力点数
        enemyhealthPercent:0,           //敌方生命占比
    },
    onInit(){
        MAP_COL = 13;
        MAP_ROW = 16;

        MapImg = new Image();
        MaterialImg = new Image();
        CharacterImg = new Image();
        AImg = new Image();
        WindowImg = new Image();
        Attack_Img = new Image();
        MapImg.src = 'common/picture/outside.png';
        MaterialImg.src = 'common/picture/material.png';
        CharacterImg.src = 'common/picture/character.png';
        AImg.src = 'common/picture/enemy.png';
        WindowImg.src = 'common/picture/window.png';
        Attack_Img.src = 'common/picture/attack.png';

        myList = new Array();
        enemyList =new Array();
    },

    onReady(){
        //获取地图宽度和高度,若小于屏幕宽度或高度就进行偏移
        MAP_WIDTH = GRAPH_WIDTH * MAP_COL;
        if(MAP_WIDTH < 720){
            //x轴的偏移量
            OFF_SET_X = (720 - MAP_WIDTH) / 2;
        }
        MAP_HEIGHT = GRAPH_WIDTH * MAP_ROW;
        if(MAP_HEIGHT < 300){
            //y轴偏移量
            OFF_SET_Y = (720 - MAP_HEIGHT) / 2;
        }
    },

    onShow(){
        this.show_round = Round;
        this.show_max_round = Max_Round;
        //获取和设置布局长度
        CANVAS_HEIGHT = this.$element('draw_Image').getBoundingClientRect().height;
//        var ROUND_ATTR = this.$element('round_Attr').getBoundingClientRect();
        var MY_ATTR = this.$element('my_Attr').getBoundingClientRect();
        var ENEMY_ATTR = this.$element('enemy_Attr').getBoundingClientRect();

//        this.round_marginleft = 720 - ROUND_ATTR.width;
        this.my_margintop = CANVAS_HEIGHT - MY_ATTR.height;
        this.enemy_margintop = CANVAS_HEIGHT - ENEMY_ATTR.height;
        //开始画地图
        this.ctx = this.$element('draw_Image').getContext('2d');
        // @ts-ignore
        this.offscreen = new OffscreenCanvas(MAP_WIDTH, MAP_HEIGHT);
//        console.log(MAP_WIDTH + " " + MAP_HEIGHT);
        var offscreenCtx = this.offscreen.getContext("2d");
        for(var i = 0; i < MAP_ROW; i++){
            for(var j = 0; j < MAP_COL; j++){
                offscreenCtx.drawImage(MapImg,
                                        0,
                                        96,
                                        IMAGE_LENGTH, IMAGE_LENGTH,
                                        j * GRAPH_WIDTH,
                                        i * GRAPH_WIDTH,
                                        GRAPH_WIDTH, GRAPH_WIDTH);
                if(map[i][j] == 1){
                    offscreenCtx.drawImage(MaterialImg,
                                            480,
                                            192,
                                            IMAGE_LENGTH, IMAGE_LENGTH,
                                            j * GRAPH_WIDTH,
                                            i * GRAPH_WIDTH,
                                            GRAPH_WIDTH, GRAPH_WIDTH);
                }
                else if(map[i][j] == 101){
                    var chess = new Chess("Martha", 0, 0, 6, 6, 6, 6, 2, 0, 0, 0, j, i);
                    offscreenCtx.drawImage(CharacterImg,
                                            chess.img_x + (IMAGE_LENGTH * chess.img_offX),
                                            chess.img_y + (IMAGE_LENGTH * chess.img_offY),
                                            IMAGE_LENGTH, IMAGE_LENGTH,
                                            j * GRAPH_WIDTH,
                                            i * GRAPH_WIDTH,
                                            GRAPH_WIDTH, GRAPH_WIDTH);
                    myList.push(chess);
                    //Chess(name, type, owner, health, max_health, attack, move, defence, position, img_x, img_y, x, y)
                    //方向:0为向下 1为向左 2为向右 3为向上
                }
                else if(map[i][j] == 201){
                    var chess = new Chess("Andy", 0, 1, 6, 6, 6, 1, 0, 3, 0, 0, j, i);
                    chess.img_offY = 3;
                    offscreenCtx.drawImage(AImg,
                                            chess.img_x + (IMAGE_LENGTH * chess.img_offX),
                                            chess.img_y + (IMAGE_LENGTH * chess.img_offY),
                                            IMAGE_LENGTH, IMAGE_LENGTH,
                                            j * GRAPH_WIDTH,
                                            i * GRAPH_WIDTH,
                                            GRAPH_WIDTH, GRAPH_WIDTH);
                    enemyList.push(chess);
                }
                else if(map[i][j] == 202){
                    var chess = new Chess("Andy", 0, 1, 6, 6, 6, 1, 0, 0, 0, 0, j, i);
                    chess.img_offY = 0;
                    offscreenCtx.drawImage(AImg,
                                            chess.img_x + (IMAGE_LENGTH * chess.img_offX),
                                            chess.img_y + (IMAGE_LENGTH * chess.img_offY),
                                            IMAGE_LENGTH, IMAGE_LENGTH,
                                            j * GRAPH_WIDTH,
                                            i * GRAPH_WIDTH,
                                            GRAPH_WIDTH, GRAPH_WIDTH);
                    enemyList.push(chess);
                }
                else if(map[i][j] == 203){
                    var chess = new Chess("Andy", 0, 1, 6, 6, 6, 1, 0, 1, 0, 0, j, i);
                    chess.img_offY = 1;
                    offscreenCtx.drawImage(AImg,
                                            chess.img_x + (IMAGE_LENGTH * chess.img_offX),
                                            chess.img_y + (IMAGE_LENGTH * chess.img_offY),
                                            IMAGE_LENGTH, IMAGE_LENGTH,
                                            j * GRAPH_WIDTH,
                                            i * GRAPH_WIDTH,
                                            GRAPH_WIDTH, GRAPH_WIDTH);
                    enemyList.push(chess);
                }
                else if(map[i][j] == 204){
                    var chess = new Chess("Andy", 0, 1, 6, 6, 6, 1, 0, 2, 0, 0, j, i);
                    chess.img_offY = 2;
                    offscreenCtx.drawImage(AImg,
                                            chess.img_x + (IMAGE_LENGTH * chess.img_offX),
                                            chess.img_y + (IMAGE_LENGTH * chess.img_offY),
                                            IMAGE_LENGTH, IMAGE_LENGTH,
                                            j * GRAPH_WIDTH,
                                            i * GRAPH_WIDTH,
                                            GRAPH_WIDTH, GRAPH_WIDTH);
                    enemyList.push(chess);
                }
            }
        }
        this.bitmap = this.offscreen.transferToImageBitmap();
        this.ctx.transform(1, 0, 0, 1, OFF_SET_X, OFF_SET_Y);
        this.ctx.transferFromImageBitmap(this.bitmap);

        this.round_show = true;

        this.myLoop();
    },

    //触屏事件
    maptouchstart:function(msg){
        //        this.move_x = msg.touches[0].globalX;
        //        this.move_y = msg.touches[0].globalY;
        this.local_x = msg.touches[0].localX;
        this.local_y = msg.touches[0].localY;
    },

    //触屏移动事件
    maptouchmove:function(msg){
        var move_x = msg.changedTouches[0].localX - this.local_x;
        var move_y = msg.changedTouches[0].localY - this.local_y;
        this.local_x = msg.changedTouches[0].localX;
        this.local_y = msg.changedTouches[0].localY;
        //刷新地图
        this.ctx.clearRect(0,0,720,CANVAS_HEIGHT)
        //判断横向移动界线
        if(OFF_SET_X > 0){
            move_x = 0;
            MAP_DISPLAY_X += move_x;
        }
        else if(MAP_DISPLAY_X + move_x > 0){
            move_x = -(MAP_DISPLAY_X);
            MAP_DISPLAY_X = 0;
        }
        else if(MAP_DISPLAY_X + move_x <= 720 - MAP_WIDTH){
            move_x = 720 - MAP_WIDTH - MAP_DISPLAY_X;
            MAP_DISPLAY_X += move_x;
        }
        else{
            MAP_DISPLAY_X += move_x;
        }
        //判断竖向移动界线
        if(OFF_SET_Y > 0){
            move_y = 0;
            MAP_DISPLAY_Y += move_y;
        }
        else if(MAP_DISPLAY_Y + move_y > 0){
            move_y = -(MAP_DISPLAY_Y);
            MAP_DISPLAY_Y = 0;
        }
        else if(MAP_DISPLAY_Y + move_y <= CANVAS_HEIGHT - MAP_HEIGHT){
            move_y = CANVAS_HEIGHT - MAP_HEIGHT - MAP_DISPLAY_Y;
            MAP_DISPLAY_Y += move_y;
        }
        else{
            MAP_DISPLAY_Y += move_y;
        }
        this.ctx.transform(1, 0, 0, 1, move_x, move_y);
        this.ctx.transferFromImageBitmap(this.bitmap);
    },

    //长按触发事件
    maplongpress:function(){
        if(allow_click && this.local_x >= OFF_SET_X && this.local_x <= 720 - OFF_SET_X && this.local_y >= OFF_SET_Y && this.local_y <= CANVAS_HEIGHT - OFF_SET_Y) {
            var press_y;
            var press_x;
            press_y = parseInt((this.local_y - MAP_DISPLAY_Y - OFF_SET_Y) / GRAPH_WIDTH);
            press_x = parseInt((this.local_x - MAP_DISPLAY_X - OFF_SET_X) / GRAPH_WIDTH);
            var flag = true;
            for (var i = 0; i < myList.length; i++) {
                if (myList[i].IsLocation(press_x, press_y)) {
                    var buffer = myFlag;
                    myFlag = i;
                    this.Show_Mychess_Mes(true);
                    ///////////////////////////////////////////??????
                    myFlag = buffer;
                    flag = false;
                    break;
                }
            }
            if(flag){
                for (var i = 0; i < enemyList.length; i++) {
                    if (enemyList[i].IsLocation(press_x, press_y)) {
                        enemyFlag = i;
                        this.Show_Enemychess_Mes(true);
                        enemyFlag = -1;
                        flag = false;
                        break;
                    }
                }
            }
        }
    },

    //点击事件
    mapclick:function(){
        if(allow_click && this.local_x >= OFF_SET_X && this.local_x <= 720 - OFF_SET_X && this.local_y >= OFF_SET_Y && this.local_y <= CANVAS_HEIGHT - OFF_SET_Y) {
            var click_y;
            var click_x;
            click_y = parseInt((this.local_y - MAP_DISPLAY_Y - OFF_SET_Y) / GRAPH_WIDTH);
            click_x = parseInt((this.local_x - MAP_DISPLAY_X - OFF_SET_X) / GRAPH_WIDTH);

            //如果之前没有选择己方棋子，这时就进行轮询
            if(myFlag == -1) {
                var flag = true;
                //循环比较每个己方棋子，如果有检测到点击的位置为己方的棋子的位置，则表示该棋子被选择了
                for (var i = 0; i < myList.length; i++) {
                    if (myList[i].IsLocation(click_x, click_y)) {
                        myFlag = i;
                        if(myList[i].have_moved == false) {
                            //如果可以移动说明这回合开始之后没有操作该棋子
                            find_Search_Map_1(map, MAP_ROW, MAP_COL, myList[i].x, myList[i].y, myList[i].move);
                            //允许绘画
                            func_flag = 5;
                            this.Show_Mychess_Mes(false);
                        }
                        else{
                            this.Show_Mychess_Mes(false);
                            myFlag = -1;
                        }
                        flag = false;
                        break;
                    }
                }

                if(flag){
                    this.enemy_show = "hidden";
                    this.detail_enemy_show = "hidden";
                    //判断是否点击到敌方棋子
                    for(var i = 0; i < enemyList.length; i++){
                        if(enemyList[i].IsLocation(click_x,click_y)){
                            enemyFlag = i;
                            this.Show_Enemychess_Mes(false);
                            //显示完数据后,及时将myFlag空置
                            enemyFlag = -1;
                            flag = false;
                            break;
                        }
                    }
                }

                if(flag){
                    this.my_show = "hidden";
                    this.detail_my_show = "hidden";
                }
            }
            else{
                /*******************************************************************
                有选择己方可动的棋子的话,就判断是否在移动范围之内,
                范围之外的话就直接取消,该棋子的被选择状态,同时还要再次判断是否有其他棋子在该位置
                *******************************************************************/
                var search_map = get_Search_Map();
                var attack_map = get_Attack_Map();
                lastSearchMap = new Array();
                lastAttackMap = new Array();
                lastSearchMap = get_Search_Map();
                lastAttackMap = get_Attack_Map();
                if(myList[myFlag].have_moved == false && search_map[click_y][click_x] > 0 && map[click_y][click_x] <= 100){
                    //在移动范围之内
                    move_route = get_move_route(click_x, click_y, myList[myFlag].x, myList[myFlag].y);
                    move_row = myList[myFlag].y;
                    move_col = myList[myFlag].x;
                    //保存该点的人物参数
                    move_num = map[move_row][move_col];
                    //清空该点的人物参数
                    map[move_row][move_col] = 0;
                    myList[myFlag].x = -1;
                    myList[myFlag].y = -1;
                    move_fps = move_route.length * step;
                    myList[myFlag].have_moved = true;
                    //在移动动画过程中将不允许触发点击事件
                    allow_click = false;
                    func_flag = 6;
                    //myFlag保持不变,因为还可以攻击
                }
                else if(attack_map[click_y][click_x] == 1){
                    //在攻击范围之内
                    //要使用clearMap，一定要确保该地图是否要进行低频率刷新,否则就会出错
                    clearMap();
                    //判断是否点击到其他敌方棋子
                    for (var i = 0; i < enemyList.length; i++) {
                        if (enemyList[i].IsLocation(click_x, click_y)) {
                            //myFlag更改相应的棋子的下标
                            enemyFlag = i;
                            this.my_show = "hidden";
                            this.detail_my_show = "hidden";
                            break;
                        }
                    }
                    attack_fps = 5;
                    myList[myFlag].have_moved = true;
                    myList[myFlag].have_attacked = true;
                    //在攻击动画过程中将不允许触发点击事件
                    allow_click = false;
                    func_flag = 7;
                }
                else if(click_x == myList[myFlag].x && click_y == myList[myFlag].y){
                    //刚好点击的位置就是被激活的棋子
                    this.Show_Mychess_Mes(false);
                    //myFlag不做处理
                }
                else{
                    //范围之外
                    //先清理缓存的搜寻地图和攻击地图
                    clearMap();
                    var flag = true;
                    //判断是否点击到其他己方棋子
                    for (var i = 0; i < myList.length; i++) {
                        if (myList[i].IsLocation(click_x, click_y)) {
                            //myFlag更改相应的棋子的下标
                            myFlag = i;
                            if( myList[i].have_moved == false){
                                find_Search_Map_1(map, MAP_ROW, MAP_COL, myList[i].x, myList[i].y, myList[i].move);
                                this.Show_Mychess_Mes(false);
                                func_flag = 8;
                            }
                            else{
                                this.Show_Mychess_Mes(false);
                                //显示完数据后,及时将myFlag空置
                                myFlag = -1;
                                func_flag = 9;
                            }
                            flag = false;
                            break;
                        }
                    }
                    //点击位置绝对在范围外的无人区或敌人的棋子
                    if(flag == true){
                        this.my_show = "hidden";
                        this.detail_my_show = "hidden";
                        //判断是否点击到范围外的敌方棋子
                        for(var i = 0; i < enemyList.length; i++){
                            if(enemyList[i].IsLocation(click_x,click_y)){
                                enemyFlag = i;
                                this.Show_Enemychess_Mes(false);
                                //显示完数据后,及时将myFlag空置
                                enemyFlag = -1;
                                break;
                            }
                        }
                        myFlag = -1;
                        func_flag = 9;
                    }
                }
            }

//            console.log("当前位置:" + click_y + "||" + click_x);
        }
        else{
            //超过边界不做处理
        }
    },

    //循环进行动画绘制
    myLoop:function(){
// @ts-ignore
        requestAnimationFrame(this.myLoop);
        now = Date.now();
        delta = now - then;
        if(delta > interval){
            /******************************************
            这里不能简单then = now,否则还会出现细微的时间差问题.
            例如fps = 10,每帧100ms,而现在每16ms(60fps)执行一次draw. 16*7 = 112 > 100,需要7次才实际绘制一次。
            这个情况下需要 112*10 = 1120ms > 1000ms 才绘制完成
            ******************************************/
            then = now - (delta % interval);
            this.myDrawing();
        }
    },

    //动画绘制
    myDrawing:function(){
        if(func_flag == -2){
            if(Lose_Flag == true){
                this.game_over_show = true;
                this.game_over_result = "你输了!"
//                console.log("你输了!!!");
            }
            else if(Win_Flag == true){
                this.game_over_show = true;
                this.game_over_result = "你赢了!"
//                console.log("你赢了!");
            }
            else{
//                console.log("未知动向.");
            }
            func_flag = -1;
        }
        //绘制移动的动画
        if(func_flag == 1){
            //循环播放动画
            this.draw_Move_Chess(true);
            var bitmap = this.offscreen.transferToImageBitmap();
            this.ctx.transferFromImageBitmap(bitmap);
        }
        //绘制攻击的动画
        else if(func_flag == 2){
            if(attack_round == 1){
                this.draw_Attack_Chess(myList[myFlag], enemyList[enemyFlag]);
            }
            else if(attack_round == 2){
                this.draw_Attack_Chess(enemyList[enemyFlag], myList[myFlag]);
            }
            else if(attack_round == 3){
                //未来扩展
                attack_round = 0;
            }

            if(attack_round == 0){
                //myFlag置为-1,因为该棋子行动和攻击能力丧失
                myFlag = -1;
                //enemyFlag置为-1
                enemyFlag = -1;
                //判断是否双方棋子是否为0
                if(myList.length == 0){
                    Lose_Flag = true;
                    func_flag = -2;
                }
                else if(enemyList.length == 0){
                    Win_Flag = true;
                    func_flag = -2;
                }
                else{
                    Player_Round = false;
                    for (var i = 0; i < myList.length; i++) {
                        if (myList[i].have_moved == false) {
                            Player_Round = true;
                            break;
                        }
                    }
                    if (Player_Round == false) {
                        func_flag = 10;
                    }
                    else {
                        //重新允许触发点击事件
                        allow_click = true;
                        func_flag = -1;
                    }
                }
            }

            var bitmap = this.offscreen.transferToImageBitmap();
            this.ctx.transferFromImageBitmap(bitmap);
        }
        //绘制棋子被点击激活时的自身动画
        else if(func_flag == 3){
            //循环播放动画
            this.draw_Active_Chess();
            var bitmap = this.offscreen.transferToImageBitmap();
            this.ctx.transferFromImageBitmap(bitmap);
        }
        //绘制棋子已经移动后的攻击图
        else if(func_flag == 4) {
            this.draw_Attack_Map();
            var bitmap = this.offscreen.transferToImageBitmap();
            this.ctx.transferFromImageBitmap(bitmap);
            func_flag = 3;
        }
        //绘制棋子被点击激活时的寻路图和攻击图
        else if(func_flag == 5){
            this.draw_Search_Map();
            this.draw_Attack_Map();
            var bitmap = this.offscreen.transferToImageBitmap();
            this.ctx.transferFromImageBitmap(bitmap);
            func_flag = 3;
        }
        //快速刷新地图
        else if(func_flag == 6 || func_flag == 7 || func_flag == 8 || func_flag == 9){
            this.draw_Low_Refresh_Map();
            if(func_flag == 6){
                this.my_show = "hidden";
                this.detail_my_show = "hidden";
                func_flag = 1;
            }
            else if(func_flag == 7){
                change_position(myList[myFlag], enemyList[enemyFlag]);
                attack_round = 1;
                func_flag = 2;
            }
            else if(func_flag == 8){
                var bitmap = this.offscreen.transferToImageBitmap();
                this.ctx.transferFromImageBitmap(bitmap);
                func_flag = 5;
            }
            else if(func_flag == 9){
                var bitmap = this.offscreen.transferToImageBitmap();
                this.ctx.transferFromImageBitmap(bitmap);
                func_flag = -1;
                //判断该回合是否结束
                if(Player_Round == true) {
                    Player_Round = false;
                    for (var i = 0; i < myList.length; i++) {
                        if (myList[i].have_moved == false) {
                            Player_Round = true;
                            break;
                        }
                    }
                    if(Player_Round == false){
                        func_flag = 10;
                    }
                }
            }
        }
        //结束该动画线程，转为敌方回合的线程
        else if(func_flag == 10){
            func_flag = 23;
            //重新赋予每个敌方棋子行动能力和攻击能力
            for(var i = 0; i < enemyList.length; i++){
                enemyList[i].have_moved = false;
                enemyList[i].have_attacked = false;
            }
            allow_click = false;
        }

        /***********************************************
        ***************以下是敌方回合的动画绘制**************
        ***********************************************/
        //敌方移动
        if(func_flag == 21){
            //循环播放动画
            this.draw_Move_Chess(false);
            //未来将创建一个线程以及s锁和x锁来控制输出获取图片
            var bitmap = this.offscreen.transferToImageBitmap();
            this.ctx.transferFromImageBitmap(bitmap);
        }
        //敌方攻击
        else if(func_flag == 22){
            if(attack_round == 1){
                this.draw_Attack_Chess(enemyList[enemyFlag], myList[myFlag]);
            }
            else if(attack_round == 2){
                this.draw_Attack_Chess(myList[myFlag], enemyList[enemyFlag]);
            }
            else if(attack_round == 3){
                //未来扩展
                attack_round = 0;
            }

            if(attack_round == 0){
                //myFlag置为-1表示空置
                myFlag = -1;
                //enemyFlag置为-1表示空置
                enemyFlag = -1;
                //清除缓存的地图
                clearMap();
                //判断是否双方棋子是否为0
                if(myList.length == 0){
                    Lose_Flag = true;
                    func_flag = -2;
                }
                else if(enemyList.length == 0){
                    Win_Flag = true;
                    func_flag = -2;
                }
                else{
                    func_flag = 23;
                }
            }
            var bitmap = this.offscreen.transferToImageBitmap();
            this.ctx.transferFromImageBitmap(bitmap);
        }
        //寻找敌方棋子列表中第一个可被激活的棋子
        else if(func_flag == 23){
            for(var i = 0; i < enemyList.length; i++){
                if(enemyList[i].have_moved == false){
                    find_Search_Map_2(map, MAP_ROW, MAP_COL, enemyList[i].x, enemyList[i].y, enemyList[i].move);
                    enemyFlag = i;
                    func_flag = 25;
                    break;
                }
                if(i == enemyList.length - 1){
                    func_flag = 30;
                    break;
                }
            }
        }
        //绘制棋子已经移动后的攻击图
        else if(func_flag == 24){
            this.draw_Attack_Map();
            var bitmap = this.offscreen.transferToImageBitmap();
            this.ctx.transferFromImageBitmap(bitmap);
            //判断攻击最少血的棋子
            this.Select_Attack();
            change_position(enemyList[enemyFlag], myList[myFlag]);
            attack_round = 1;
            func_flag = 22;
        }
        //绘制棋子被选择时的寻路图和攻击图
        else if(func_flag == 25){
            this.draw_Search_Map();
            this.draw_Attack_Map();
            var bitmap = this.offscreen.transferToImageBitmap();
            this.ctx.transferFromImageBitmap(bitmap);
            //敌人判断走哪里,不能走就直接攻击
            this.Move_Or_Attack();
        }
        //快速刷新地图
        else if(func_flag == 26 || func_flag == 27 || func_flag == 28){
            this.draw_Low_Refresh_Map();
            if (func_flag == 26) {
                func_flag = 21;
            }
            else if (func_flag == 27) {
                change_position(enemyList[enemyFlag], myList[myFlag]);
                attack_round = 1;
                func_flag = 22;
            }
            else if (func_flag == 28) {
                func_flag = 24;
            }
            var bitmap = this.offscreen.transferToImageBitmap();
            this.ctx.transferFromImageBitmap(bitmap);
        }
        //结束该动画线程，转为敌方回合的线程
        else if(func_flag == 30){
            func_flag = -1;
            //重新赋予每个己方棋子行动能力和攻击能力
            for(var i = 0; i < myList.length; i++){
                myList[i].have_moved = false;
                myList[i].have_attacked = false;
            }
            if(Round < Max_Round){
                Round += 1;
                this.show_round = Round;
                Player_Round = true;
                allow_click = true;
            }
            else{
                Lose_Flag = true;
                func_flag = -2;
            }
        }
//        else if(func_flag == 11){
//            //添加个动画？
//        }
    },

    //绘制移动的动画
    draw_Move_Chess:function(flag){
        //flag标记是己方移动还是敌方移动,true:己方 flase:敌方
        var offscreenCtx = this.offscreen.getContext("2d");
        //清除当前棋子的位置
        offscreenCtx.clearRect(move_col * GRAPH_WIDTH, move_row * GRAPH_WIDTH, GRAPH_WIDTH, GRAPH_WIDTH);
        offscreenCtx.drawImage(MapImg,
                                0,
                                96,
                                IMAGE_LENGTH, IMAGE_LENGTH,
                                move_col * GRAPH_WIDTH,
                                move_row * GRAPH_WIDTH,
                                GRAPH_WIDTH, GRAPH_WIDTH);
        if(flag) {
            if (map[move_row][move_col] > 100 && map[move_row][move_col] <= 200) {
                for (var r = 0; r < myList.length; r++) {
                    if (myList[r].IsLocation(move_col, move_row)) {
                        offscreenCtx.drawImage(CharacterImg,
                                                myList[r].img_x + (IMAGE_LENGTH * myList[r].img_offX),
                                                myList[r].img_y + (IMAGE_LENGTH * myList[r].img_offY),
                                                IMAGE_LENGTH, IMAGE_LENGTH,
                                                move_col * GRAPH_WIDTH,
                                                move_row * GRAPH_WIDTH,
                                                GRAPH_WIDTH, GRAPH_WIDTH);
                        break;
                    }
                }
            }
        }
        else{
            if (map[move_row][move_col] > 200 && map[move_row][move_col] <= 300) {
                for (var r = 0; r < enemyList.length; r++) {
                    if (enemyList[r].IsLocation(move_col, move_row)) {
                        offscreenCtx.drawImage(AImg,
                                                enemyList[r].img_x + (IMAGE_LENGTH * enemyList[r].img_offX),
                                                enemyList[r].img_y + (IMAGE_LENGTH * enemyList[r].img_offY),
                                                IMAGE_LENGTH, IMAGE_LENGTH,
                                                move_col * GRAPH_WIDTH,
                                                move_row * GRAPH_WIDTH,
                                                GRAPH_WIDTH, GRAPH_WIDTH);
                        break;
                    }
                }
            }
        }

        //根据不同的移动路线对相应的方向的格子进行清除
        var i = move_route.length - 1;
        var x = move_col;
        var y = move_row;
        if(move_route[i] == 1){
            //1代表向下走
            if(flag){
                myList[myFlag].img_offY = 0;
            }
            else{
                enemyList[enemyFlag].img_offY = 0;
            }
            move_off_y += GRAPH_WIDTH / step;
            y += 1;
        }
        else if(move_route[i] == 2){
            //2代表向右走
            if(flag){
                myList[myFlag].img_offY = 2;
            }
            else{
                enemyList[enemyFlag].img_offY = 2;
            }
            move_off_x += GRAPH_WIDTH / step;
            x += 1;
        }
        else if(move_route[i] == 3){
            //3代表向左走
            if(flag){
                myList[myFlag].img_offY = 1;
            }
            else{
                enemyList[enemyFlag].img_offY = 1;
            }
            move_off_x -= GRAPH_WIDTH / step;
            x -= 1;
        }
        else if(move_route[i] == 4){
            //4代表向上走
            if(flag){
                myList[myFlag].img_offY = 3;
            }
            else{
                enemyList[enemyFlag].img_offY = 3;
            }
            move_off_y -= GRAPH_WIDTH / step;
            y -= 1;
        }
        offscreenCtx.clearRect(x * GRAPH_WIDTH, y * GRAPH_WIDTH, GRAPH_WIDTH, GRAPH_WIDTH);
        offscreenCtx.drawImage(MapImg,
                                0,
                                96,
                                IMAGE_LENGTH, IMAGE_LENGTH,
                                x * GRAPH_WIDTH,
                                y * GRAPH_WIDTH,
                                GRAPH_WIDTH, GRAPH_WIDTH);
        if(flag) {
            if (map[y][x] > 100 && map[y][x] <= 200) {
                for (var r = 0; r < myList.length; r++) {
                    if (myList[r].IsLocation(x, y)) {
                        offscreenCtx.drawImage(CharacterImg,
                                                myList[r].img_x + (IMAGE_LENGTH * myList[r].img_offX),
                                                myList[r].img_y + (IMAGE_LENGTH * myList[r].img_offY),
                                                IMAGE_LENGTH, IMAGE_LENGTH,
                                                x * GRAPH_WIDTH,
                                                y * GRAPH_WIDTH,
                                                GRAPH_WIDTH, GRAPH_WIDTH);
                        break;
                    }
                }
            }
            //绘制移动的人物
            //若刚好走完4帧，则已经完全移动到下一格子了
            if(myList[myFlag].img_offX != 1){
                myList[myFlag].add_offX = -myList[myFlag].add_offX;
            }
            myList[myFlag].img_offX += myList[myFlag].add_offX;
            move_flag = (move_flag + 1) % step;
            if (move_flag == 0) {
                move_col = x;
                move_row = y;
                move_off_x = 0;
                move_off_y = 0;
                move_route.pop();
            }
            offscreenCtx.drawImage(CharacterImg,
                myList[myFlag].img_x + (IMAGE_LENGTH * myList[myFlag].img_offX),
                myList[myFlag].img_y + (IMAGE_LENGTH * myList[myFlag].img_offY),
                IMAGE_LENGTH, IMAGE_LENGTH,
                move_col * GRAPH_WIDTH + move_off_x,
                move_row * GRAPH_WIDTH + move_off_y,
                GRAPH_WIDTH, GRAPH_WIDTH);

            move_fps -= 1;
            if (move_fps == 0) {
                myList[myFlag].x = move_col;
                myList[myFlag].y = move_row;
                map[move_row][move_col] = move_num;
                move_route = null;
                //重新计算周围是否可以攻击
                if (!find_Attack_Map_1(map, move_col, move_row)) {
                    myList[myFlag].have_attacked = true;
                    myFlag = -1;
                    func_flag = -1;
                    //判断该回合是否结束
                    if (Player_Round == true) {
                        Player_Round = false;
                        for (var i = 0; i < myList.length; i++) {
                            if (myList[i].have_moved == false) {
                                Player_Round = true;
                                break;
                            }
                        }
                        if (Player_Round == false) {
                            func_flag = 10;
                        }
                    }
                }
                else {
                    //只显示攻击图
                    func_flag = 4;
                }
                //重置缓存的数据
                move_col = -1;
                move_row = -1;
                move_num = -1;
                //重新允许触发点击事件
                allow_click = true;
            }
        }
        else{
            if (map[y][x] > 200 && map[y][x] <= 300) {
                for (var r = 0; r < enemyList.length; r++) {
                    if (enemyList[r].IsLocation(x, y)) {
                        offscreenCtx.drawImage(AImg,
                                                enemyList[r].img_x + (IMAGE_LENGTH * enemyList[r].img_offX),
                                                enemyList[r].img_y + (IMAGE_LENGTH * enemyList[r].img_offY),
                                                IMAGE_LENGTH, IMAGE_LENGTH,
                                                x * GRAPH_WIDTH,
                                                y * GRAPH_WIDTH,
                                                GRAPH_WIDTH, GRAPH_WIDTH);
                        break;
                    }
                }
            }
            //绘制移动的敌方单位
            //若刚好走完4帧，则已经完全移动到下一格子了
            if(enemyList[enemyFlag].img_offX != 1){
                enemyList[enemyFlag].add_offX = -enemyList[enemyFlag].add_offX;
            }
            enemyList[enemyFlag].img_offX += enemyList[enemyFlag].add_offX;
            move_flag = (move_flag + 1) % step;
            if (move_flag == 0) {
                move_col = x;
                move_row = y;
                move_off_x = 0;
                move_off_y = 0;
                move_route.pop();
            }
            offscreenCtx.drawImage(AImg,
                                    enemyList[enemyFlag].img_x + (IMAGE_LENGTH * enemyList[enemyFlag].img_offX),
                                    enemyList[enemyFlag].img_y + (IMAGE_LENGTH * enemyList[enemyFlag].img_offY),
                                    IMAGE_LENGTH, IMAGE_LENGTH,
                                    move_col * GRAPH_WIDTH + move_off_x,
                                    move_row * GRAPH_WIDTH + move_off_y,
                                    GRAPH_WIDTH, GRAPH_WIDTH);

            move_fps -= 1;
            if (move_fps == 0) {
                enemyList[enemyFlag].x = move_col;
                enemyList[enemyFlag].y = move_row;
                map[move_row][move_col] = move_num;
                move_route = null;
                //重新计算周围是否可以攻击
                if (!find_Attack_Map_2(map, move_col, move_row)) {
                    //清除缓存的地图
                    clearMap();
                    enemyList[enemyFlag].have_attacked = true;
                    enemyFlag = -1;
                    func_flag = 23;
                }
                else {
                    //只显示攻击图
                    func_flag = 24;
                }
                //重置缓存的数据
                move_col = -1;
                move_row = -1;
                move_num = -1;
            }
        }
    },

    //绘制攻击的动画
    draw_Attack_Chess:function(chess_one, chess_two){
        var flag;
        if(chess_one.owner == 0){
            flag = 1;
        }
        else if(chess_one.owner == 1){
            flag = 2;
        }
        var offscreenCtx = this.offscreen.getContext("2d");
        //清除要攻击的棋子的样子
        offscreenCtx.clearRect(chess_one.x * GRAPH_WIDTH,
                                chess_one.y * GRAPH_WIDTH,
                                GRAPH_WIDTH, GRAPH_WIDTH);
        offscreenCtx.drawImage(MapImg,
                                0,
                                96,
                                IMAGE_LENGTH, IMAGE_LENGTH,
                                chess_one.x * GRAPH_WIDTH,
                                chess_one.y * GRAPH_WIDTH,
                                GRAPH_WIDTH, GRAPH_WIDTH);

        //清除要攻击的棋子位置
        offscreenCtx.clearRect(chess_two.x * GRAPH_WIDTH,
                                chess_two.y * GRAPH_WIDTH,
                                GRAPH_WIDTH, GRAPH_WIDTH);
        offscreenCtx.drawImage(MapImg,
                                0,
                                96,
                                IMAGE_LENGTH, IMAGE_LENGTH,
                                chess_two.x * GRAPH_WIDTH,
                                chess_two.y * GRAPH_WIDTH,
                                GRAPH_WIDTH, GRAPH_WIDTH);

        //重新绘制棋子的样子,同时更改双方的方向，要面朝对方
        if(flag == 1){
            offscreenCtx.drawImage(CharacterImg,
                                    chess_one.img_x + (IMAGE_LENGTH * chess_one.img_offX),
                                    chess_one.img_y + (IMAGE_LENGTH * chess_one.img_offY),
                                    IMAGE_LENGTH, IMAGE_LENGTH,
                                    chess_one.x * GRAPH_WIDTH,
                                    chess_one.y * GRAPH_WIDTH,
                                    GRAPH_WIDTH, GRAPH_WIDTH);
            offscreenCtx.drawImage(AImg,
                                    chess_two.img_x + (IMAGE_LENGTH * chess_two.img_offX),
                                    chess_two.img_y + (IMAGE_LENGTH * chess_two.img_offY),
                                    IMAGE_LENGTH, IMAGE_LENGTH,
                                    chess_two.x * GRAPH_WIDTH,
                                    chess_two.y * GRAPH_WIDTH,
                                    GRAPH_WIDTH, GRAPH_WIDTH);
        }
        else if(flag == 2){
            offscreenCtx.drawImage(AImg,
                                    chess_one.img_x + (IMAGE_LENGTH * chess_one.img_offX),
                                    chess_one.img_y + (IMAGE_LENGTH * chess_one.img_offY),
                                    IMAGE_LENGTH, IMAGE_LENGTH,
                                    chess_one.x * GRAPH_WIDTH,
                                    chess_one.y * GRAPH_WIDTH,
                                    GRAPH_WIDTH, GRAPH_WIDTH);
            offscreenCtx.drawImage(CharacterImg,
                                    chess_two.img_x + (IMAGE_LENGTH * chess_two.img_offX),
                                    chess_two.img_y + (IMAGE_LENGTH * chess_two.img_offY),
                                    IMAGE_LENGTH, IMAGE_LENGTH,
                                    chess_two.x * GRAPH_WIDTH,
                                    chess_two.y * GRAPH_WIDTH,
                                    GRAPH_WIDTH, GRAPH_WIDTH);
        }

        attack_fps -= 1;
        if(attack_fps != 0){
            //增加攻击的动画
            offscreenCtx.drawImage(Attack_Img,
                                    attack_img_offX * 192,
                                    attack_img_offY * 192,
                                    192, 192,
                                    chess_two.x * GRAPH_WIDTH,
                                    chess_two.y * GRAPH_WIDTH,
                                    GRAPH_WIDTH, GRAPH_WIDTH);
            attack_img_offX += 1;
        }
        else{
            attack_img_offX = 0;
            attack_img_offY = 0;
            //攻击逻辑
            var ret = fight_against(chess_one, chess_two);
            if(ret){
                //低于生命值的话该对象就从列表中移除,同时地图也是
                //清除要攻击的棋子位置
                offscreenCtx.clearRect(chess_two.x * GRAPH_WIDTH,
                                        chess_two.y * GRAPH_WIDTH,
                                        GRAPH_WIDTH, GRAPH_WIDTH);
                offscreenCtx.drawImage(MapImg,
                                        0,
                                        96,
                                        IMAGE_LENGTH, IMAGE_LENGTH,
                                        chess_two.x * GRAPH_WIDTH,
                                        chess_two.y * GRAPH_WIDTH,
                                        GRAPH_WIDTH, GRAPH_WIDTH);
                map[chess_two.y][chess_two.x] = 0;
                attack_round = 0;
                if(flag == 1){
                    enemyList.splice(enemyFlag, 1);
                }
                else if(flag == 2){
                    myList.splice(myFlag, 1);
                }
            }
            else{
                if(attack_round == 1 || attack_round == 2){
                    attack_fps = 5;
                    attack_round += 1;
                }
                else{
                    attack_round = 0;
                }
            }
        }
    },

    //绘制人物被激活时的动画
    draw_Active_Chess:function(){
        var offscreenCtx = this.offscreen.getContext("2d");
        //当被选择的时候才能播放动画
        if(myFlag >= 0){
            //改变被选择人物的动作
            offscreenCtx.clearRect(myList[myFlag].x * GRAPH_WIDTH,
                                    myList[myFlag].y * GRAPH_WIDTH,
                                    GRAPH_WIDTH, GRAPH_WIDTH);
            offscreenCtx.drawImage(MapImg,
                                    0,
                                    96,
                                    IMAGE_LENGTH, IMAGE_LENGTH,
                                    myList[myFlag].x * GRAPH_WIDTH,
                                    myList[myFlag].y * GRAPH_WIDTH,
                                    GRAPH_WIDTH, GRAPH_WIDTH);

            if(myList[myFlag].img_offX != 1){
                myList[myFlag].add_offX = -myList[myFlag].add_offX;
            }
            myList[myFlag].img_offX += myList[myFlag].add_offX;

            offscreenCtx.drawImage(CharacterImg,
                                    myList[myFlag].img_x + (IMAGE_LENGTH * myList[myFlag].img_offX),
                                    myList[myFlag].img_y + (IMAGE_LENGTH * myList[myFlag].img_offY),
                                    IMAGE_LENGTH, IMAGE_LENGTH,
                                    myList[myFlag].x * GRAPH_WIDTH,
                                    myList[myFlag].y * GRAPH_WIDTH,
                                    GRAPH_WIDTH, GRAPH_WIDTH);
        }
    },

    //显示可移动路径的图
    draw_Search_Map:function() {
        var offscreenCtx = this.offscreen.getContext("2d");
        offscreenCtx.filter = 'opacity(40%)';
        var search_map = get_Search_Map();
        for (var m = 0; m < MAP_ROW; m++) {
            for (var n = 0; n < MAP_COL; n++) {
                if (search_map[m][n] > 0 && map[m][n] <= 100) {
                    offscreenCtx.drawImage(WindowImg,
                                            96,
                                            144,
                                            12, 12,
                                            n * GRAPH_WIDTH,
                                            m * GRAPH_WIDTH,
                                            GRAPH_WIDTH, GRAPH_WIDTH);
                }
            }
        }
        offscreenCtx.filter = 'opacity(100%)';
    },

    //显示周围可攻击的人
    draw_Attack_Map:function(){
        var offscreenCtx = this.offscreen.getContext("2d");
        offscreenCtx.filter = 'opacity(40%)';
        var attack_map = get_Attack_Map();
        for (var m = 0; m < MAP_ROW; m++) {
            for (var n = 0; n < MAP_COL; n++) {
                if (attack_map[m][n] > 0) {
                    offscreenCtx.drawImage(WindowImg,
                                            120,
                                            144,
                                            12, 12,
                                            n * GRAPH_WIDTH,
                                            m * GRAPH_WIDTH,
                                            GRAPH_WIDTH, GRAPH_WIDTH);
                }
            }
        }
        offscreenCtx.filter = 'opacity(100%)';
    },

    //低刷新地图
    draw_Low_Refresh_Map:function(){
        var offscreenCtx = this.offscreen.getContext("2d");
        for(var m = 0; m < MAP_ROW; m++){
            for(var n = 0; n < MAP_COL; n++){
                if(lastSearchMap[m][n] > 0){
                    //可移动的地方都是无人区
                    if(map[m][n] == 0) {
                        offscreenCtx.drawImage(MapImg,
                                                0,
                                                96,
                                                IMAGE_LENGTH, IMAGE_LENGTH,
                                                n * GRAPH_WIDTH,
                                                m * GRAPH_WIDTH,
                                                GRAPH_WIDTH, GRAPH_WIDTH);
                    }
                }
                else if(lastAttackMap[m][n] > 0) {
                    //可攻击的地方都是敌人的位置
                    offscreenCtx.drawImage(MapImg,
                                            0,
                                            96,
                                            IMAGE_LENGTH, IMAGE_LENGTH,
                                            n * GRAPH_WIDTH,
                                            m * GRAPH_WIDTH,
                                            GRAPH_WIDTH, GRAPH_WIDTH);
                    /////////////////////////////////////
                    if (map[m][n] > 100 && map[m][n] <= 200) {
                        for (var r = 0; r < myList.length; r++) {
                            if (myList[r].IsLocation(n, m)) {
                                offscreenCtx.drawImage(CharacterImg,
                                                        myList[r].img_x + (IMAGE_LENGTH * myList[r].img_offX),
                                                        myList[r].img_y + (IMAGE_LENGTH * myList[r].img_offY),
                                                        IMAGE_LENGTH, IMAGE_LENGTH,
                                                        n * GRAPH_WIDTH,
                                                        m * GRAPH_WIDTH,
                                                        GRAPH_WIDTH, GRAPH_WIDTH);
                                break;
                            }
                        }
                        //方向:0为向下 1为向左 2为向右 3为向上
                    }
                    else if (map[m][n] > 200 && map[m][n] <= 300) {
                        for (var r = 0; r < enemyList.length; r++) {
                            if (enemyList[r].IsLocation(n, m)) {
                                offscreenCtx.drawImage(AImg,
                                                        enemyList[r].img_x + (IMAGE_LENGTH * enemyList[r].img_offX),
                                                        enemyList[r].img_y + (IMAGE_LENGTH * enemyList[r].img_offY),
                                                        IMAGE_LENGTH, IMAGE_LENGTH,
                                                        n * GRAPH_WIDTH,
                                                        m * GRAPH_WIDTH,
                                                        GRAPH_WIDTH, GRAPH_WIDTH);
                                break;
                            }
                        }
                    }
                }
            }
        }
        lastSearchMap = null;
        lastAttackMap = null;
    },

    //敌方棋子判断走哪里,不能走就直接攻击
    Move_Or_Attack:function(){
        var search_map = get_Search_Map();
        var attack_map = get_Attack_Map();
        lastSearchMap = new Array();
        lastAttackMap = new Array();
        lastSearchMap = get_Search_Map();
        lastAttackMap = get_Attack_Map();
        //方向    0为向下 1为向左 2为向右 3为向上
        var chess = enemyList[enemyFlag];
        var offX = 0;
        var offY = 0;
        if(chess.position == 0){
            chess.position = 3;
            offY += 1;
            if(chess.y + 1 < MAP_ROW && search_map[chess.y + 1][chess.x] > 0){
                func_flag = 26;
            }
            else if(chess.y + 1 < MAP_ROW && attack_map[chess.y + 1][chess.x] == 1){
                func_flag = 27;
            }
            else{
                func_flag = 28;
            }
        }
        else if(chess.position == 1) {
            offX -= 1;
            chess.position = 2;
            if (chess.x - 1 >= 0 && search_map[chess.y][chess.x - 1] > 0) {
                func_flag = 26;
            }
            else if(chess.x - 1 >= 0 && attack_map[chess.y][chess.x - 1] == 1){
                func_flag = 27;
            }
            else{
                func_flag = 28;
            }
        }
        else if(chess.position == 2) {
            chess.position = 1;
            offX += 1;
            if (chess.x + 1 < MAP_COL && search_map[chess.y][chess.x + 1] > 0) {
                func_flag = 26;
            }
            else if(chess.x + 1 < MAP_COL && attack_map[chess.y][chess.x + 1] == 1){
                func_flag = 27;
            }
            else{
                func_flag = 28;
            }
        }
        else if(chess.position == 3) {
            chess.position = 0;
            offY -= 1;
            if (chess.y - 1 >= 0 && search_map[chess.y - 1][chess.x] > 0) {
                func_flag = 26;
            }
            else if(chess.y - 1 >= 0 && attack_map[chess.y - 1][chess.x] == 1){
                func_flag = 27;
            }
            else{
                func_flag = 28;
            }
        }
        if(func_flag == 26) {
            //在移动范围之内,且没有点击到其他己方单位
            move_route = get_move_route(chess.x + offX, chess.y + offY, chess.x, chess.y);
            move_row = chess.y;
            move_col = chess.x;
            //保存该点的人物参数
            move_num = map[move_row][move_col];
            //清空该点的人物参数
            map[move_row][move_col] = 0;
            chess.x = -1;
            chess.y = -1;
            move_fps = move_route.length * step;
            chess.have_moved = true;
        }
        else if(func_flag == 27){
            for(var i = 0; i < myList.length; i++){
                if(chess.x + offX == myList[i].x && chess.y + offY == myList[i].y){
                    myFlag = i;
                    break;
                }
            }
            chess.have_moved = true;
            chess.have_attacked = true;
            attack_fps = 3;
        }
        else if(func_flag == 28){
            chess.have_moved = true;
        }
    },

    //敌方棋子选择健康值最少的进行攻击
    Select_Attack:function(){
        var attack_map = get_Attack_Map();
        var least_health = 0;
        for(var i = 0; i < MAP_ROW; i++){
            for(var j = 0; j < MAP_COL; j++){
                if(attack_map[i][j] == 1){
                    //找到可攻击的棋子
                    for(var n = 0; n < myList.length; n++){
                        if(myList[n].x == j && myList[n].y == i){
                            if(myList[n].health < least_health || least_health == 0){
                                least_health = myList[n].health;
                                myFlag = n;
                                attack_fps = 3;
                            }
                            break;
                        }
                    }
                }
            }
        }
    },

    //显示己方棋子信息
    Show_Mychess_Mes:function(show_detail){
        if(show_detail == true){
            this.my_show = "hidden";
            this.detail_my_show = "visible";
            this.enemy_show = "hidden";
            this.detail_enemy_show = "hidden";
        }
        else{
            this.my_show = "visible";
            this.detail_my_show = "hidden";
            this.enemy_show = "hidden";
            this.detail_enemy_show = "hidden";
        }
        this.mychessName = myList[myFlag].name;
        this.mychessHealth = myList[myFlag].health;
        this.mychessMaxhealth = myList[myFlag].max_health;
        this.mychessAttack = myList[myFlag].attack;
        this.mychessDefence = myList[myFlag].defence;
        this.mychessMove = myList[myFlag].move;
        this.myhealthPercent = (this.mychessHealth / this.mychessMaxhealth) * 100;
    },

    //显示敌方棋子信息
    Show_Enemychess_Mes:function(show_detail){
        if(show_detail == true){
            this.my_show = "hidden";
            this.detail_my_show = "hidden";
            this.enemy_show = "hidden";
            this.detail_enemy_show = "visible";
        }
        else{
            this.my_show = "hidden";
            this.detail_my_show = "hidden";
            this.enemy_show = "visible";
            this.detail_enemy_show = "hidden";
        }
        this.enemychessName = enemyList[enemyFlag].name;
        this.enemychessHealth = enemyList[enemyFlag].health;
        this.enemychessMaxhealth = enemyList[enemyFlag].max_health;
        this.enemychessAttack = enemyList[enemyFlag].attack;
        this.enemychessDefence = enemyList[enemyFlag].defence;
        this.enemychessMove = enemyList[enemyFlag].move;
        this.enemyhealthPercent = (this.enemychessHealth / this.enemychessMaxhealth) * 100;
    },
}