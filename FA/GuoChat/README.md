# 即时通讯《果聊》

## 果聊简介

- 即时通讯软件

今天打造的这一款即时通讯软件，可以应用在OpenHarmony系统。

### 代码结构

### 安装部署

##### 1.代码编译运行步骤

1）下载此项目，[链接](https://gitee.com/openharmony-sig/knowledge_demo_entainment/tree/master/FA/GuoChat)。

2）开发环境搭建，开发工具：DevEco Studio 3.0 Beta4，SDK 请配置请参考[配置OpenHarmony SDK](https://gitee.com/openharmony/docs/blob/update_master_0323/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md) 。

3）导入OpenHarmony工程：OpenHarmony应用开发，只能通过导入Sample工程的方式来创建一个新工程，具体可参考[导入Sample工程创建一个新工程](https://gitee.com/openharmony/docs/blob/update_master_0323/zh-cn/application-dev/quick-start/import-sample-to-create-project.md)

4）OpenHarmony应用运行在真机设备上，需要对应用进行签名，请参考[OpenHarmony 应用签名](https://gitee.com/openharmony/docs/blob/update_master_0323/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md)

## 参考资料

- [OpenHarmony 基于TS扩展的声明式开发范式](https://gitee.com/openharmony/docs/blob/update_master_0323/zh-cn/application-dev/reference/arkui-ts/Readme-CN.md)
