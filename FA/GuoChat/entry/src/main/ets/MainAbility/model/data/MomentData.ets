/**
 * 朋友圈页面动态数据条目结构
 */

/**
 * 动态的类型
 * 主要有：文字/链接/图片/视频
 */
export enum momentType {
  //文字
  Text ,
  //链接
  Link ,
  //图片
  Pic ,
  //视频
  Video
}

/**
 * 动态下评论的数据结构
 * @param wid 用户wid
 * @param name  用户名
 * @param context 评论内容
 * @param io  发送/接收
 */
export class CommentData{
  wid: string       //用户wid
  name: string      //用户名
  context: string   //评论内容

  constructor(wid: string , name: string , context: string ){
    this.wid = wid
    this.name = name
    this.context = context
  }
}

/**
 * 动态内容数据结构
 * @param wid       用户wid
 * @param name      用户名
 * @param headImg   用户头像
 * @param type      动态类型
 * @param io        发送/接收
 * @param param1    参数1（一般存放文字内容）
 * @param param2    参数2（一般存放图片、视频、链接等内容）
 * @param comments  评论内容结构数组
 */
export class MomentData{
  wid: string                  //用户wid
  name: string                 //用户昵称
  headImg: string            //用户头像
  type: momentType             //动态类型
  param1: any                  //参数1（一般存放文字内容）
  param2: any                  //参数2（一般存放图片、视频、链接等内容）
  comments: Array<CommentData> //评论内容
  time: string                 //时间

  constructor(wid: string , name: string , headImg: string , type: momentType , param1: string , param2: string , time: string , comments : Array<CommentData>){
    this.wid = wid
    this.name = name
    this.headImg = headImg
    this.type = type
    this.param1 = param1
    this.param2 = param2
    this.time = time
    this.comments = comments
  }
}