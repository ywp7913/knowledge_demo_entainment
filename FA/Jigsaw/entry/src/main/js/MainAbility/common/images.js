/*
 * Copyright 2022 LookerSong
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

export let images =  [
    {
        index: 0,
        src: "common/0.png",
    },
    {
        index: 1,
        src: "common/1.png",
    },
    {
        index: 2,
        src: "common/2.png",
    },
    {
        index: 3,
        src: "common/3.png",
    },
    {
        index: 4,
        src: "common/4.png",
    },
    {
        index: 5,
        src: "common/5.png",
    },
    {
        index: 6,
        src: "common/6.png",
    },
    {
        index: 7,
        src: "common/7.png",
    },
    {
        index: 8,
        src: "common/8.png",
    },
    {
        index: 9,
        src: "common/9.png",
    },
    {
        index: 10,
        src: "common/10.png",
    },
    {
        index: 11,
        src: "common/11.png",
    },
    {
        index: 12,
        src: "common/12.png",
    },
    {
        index: 13,
        src: "common/13.png",
    },
    {
        index: 14,
        src: "common/14.png",
    },
]

export default images;