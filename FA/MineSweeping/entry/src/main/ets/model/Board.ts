/*
 * Copyright (c) 2022 Tetcl Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @Description 棋盘
 * @date 2022/7/19 10:16
 * @Author Bai XiaoMing
 * @Version 1.0
 */
export default class Board {
    x: number; // 棋盘行坐标
    y: number; // 棋盘列坐标
    content: string; // 周边雷数
    isCover: boolean;   // 默认图
    isMine: boolean;    // 是否雷
    isClick: boolean;   // 是否点击

    constructor(x: number, y: number, content: string, isCover: boolean, isMine: boolean, isClick: boolean) {
        this.x = x;
        this.y = y;
        this.content = content;
        this.isCover = isCover;
        this.isMine = isMine;
        this.isClick = isClick;
    }
}