/*
 * Copyright (c) 2022 Tetcl Co., Ltd.
 * Licensed under the Apache License,Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import prompt from '@ohos.prompt';
import Board from '../model/Board';
import BoardModel from '../model/BoardModel';


@Entry
@Component
struct Index {
  @State boards: Array<Board> = new Array<Board>();

  // 棋盘行/列数
  @State boardRowsNum: number = 4;
  @State boardColsNum: number = 4;
  // 最大雷数量
  @State maxMineNum: number = 4;

  @State images: Array<Resource> = [$r("app.media.app_icon"), $r('app.media.loading_icon')]
  @State gridColsFr: string = "";
  @State clickCount: number = 0;

  // 初始化棋盘列显示数
  initGrid = () => {
    for (let i = 0; i < this.boardColsNum; i++) {
      this.gridColsFr += (i === 0 ? '1fr' : ' 1fr');
    }
  }

  init = () => {
    this.boards = new Array<Board>();
    this.gridColsFr = "";
    this.clickCount = this.boardRowsNum * this.boardColsNum;
    this.initGrid();
    this.boards = new BoardModel(this.boardRowsNum, this.boardColsNum, this.maxMineNum).init();
  }



  build() {
    Column({space: 20}) {
      Row() {
        Stack({alignContent: Alignment.Center}) {
          Image($r('app.media.start_game'))
            .width(240)
            .height(120)
          Text('开始游戏')
            .fontSize(18)
            .fontColor(Color.White)
            .fontWeight(FontWeight.Bold)
        }
        .onClick(() => {
          this.init();
        })
      }


      Grid() {
        ForEach(this.boards, (item: Board) => {
          GridItem() {
            Stack({alignContent: Alignment.Center}) {
              Image(item.isCover ? $r('app.media.loading_icon') : (item.isMine ? $r('app.media.app_icon') : $r('app.media.click_bg')))
                .width((!item.isCover && item.isMine) ? 80 : '100%')
              Text(item.isClick ? ((item.content === '9' || item.content === '0') ? '' : item.content) : '')
                .fontSize(26).fontWeight(FontWeight.Bold)
            }
            .width('100%').height(100)
          }
          .border({
            width: 1,
            style: BorderStyle.Solid,
            color: Color.White
          })
          .onClick(() => {
            if ((this.clickCount - 1) === this.maxMineNum) {
              prompt.showToast({
                message: '恭喜你，成功排雷！',
                duration: 2000
              })
              this.boards = [];
              return false;
            }
            let tempBoards = this.boards;
            this.boards = new Array<Board>();
            tempBoards.forEach(temp => {
              if (temp.x === item.x && temp.y === item.y) {
                temp.isClick = true;
                temp.isCover = false
                if (temp.isMine) {
                  AlertDialog.show({
                    message: '您踩雷了，游戏结束~',
                    autoCancel: false,
                    primaryButton: {
                      value: '重新开始',
                      action: () => {
                        this.init();
                      }
                    },
                    secondaryButton: {
                      value: '不玩了~',
                      action: () => {
                        this.boards = [];
                      }
                    },
                    alignment: DialogAlignment.Center
                  })
                } else {
                  this.clickCount--;
                }
              }
            })
            this.boards = tempBoards;
          })
        }, (item: Board) => (item.x + ',' + item.y).toString())
      }
      .width('95%')
      .columnsTemplate(this.gridColsFr)
      .columnsGap(0)
      .rowsGap(0)
      .height(500)

      if (this.boards.length > 0) {
        Row() {
          Text(`还剩多少方格${this.clickCount}`).fontSize(16).fontWeight(FontWeight.Bold)
          Text(`总共有${this.maxMineNum}个雷需要排`).fontSize(16).fontWeight(FontWeight.Bold)
        }
        .width('100%')
        .justifyContent(FlexAlign.SpaceAround)
      }
    }
    .width('100%')
    .height('100%')
  }
}